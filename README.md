# Statistics

Dll for statistics and data analysis. Provides methods for one-, two- and multidimensional analysis. Also includes PCA, Clustering algorithms and scouting factor analysis.