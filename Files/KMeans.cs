﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;

namespace Statistics
{
    public static class KMeans
    {
        private const double EPS = 0.0001;

        public static Matrix[] PerformClusteringByBollHowl(Matrix M, int K, int TotalIterations, Func<MultiDimensionalVector, MultiDimensionalVector, double> DistanceMetric)
        {
            Random R = new Random();
            int Width = M.Width;
            int TotalCount = M.Height;

            Matrix[] Clusters = new Matrix[K];
            List<MultiDimensionalVector>[] DynamiClusters = new List<MultiDimensionalVector>[K];

            MultiDimensionalVector[] Centroids = new MultiDimensionalVector[K];

            for (int i = 0, j; i < K; i++)
            {
                Centroids[i] = M.GetRow(R.Next(TotalCount));
                for (j = 0; j < i; j++)
                    if (Centroids[j].Equals(Centroids[i]))
                    {
                        i--;
                        break;
                    }
                DynamiClusters[i] = new List<MultiDimensionalVector>();
            }

            double MinDistance = Double.MaxValue;
            double TempDistance;
            int MinIndex = 0;
            MultiDimensionalVector TempVector;
            for (int i = 0, j, k; i < TotalIterations; i++)
            {
                for (j = 0; j < K; j++)
                    DynamiClusters[j].Clear();

                for (j = 0; j < M.Height; j++)
                {
                    TempVector = M.GetRow(j);
                    MinDistance = DistanceMetric(TempVector, Centroids[0]);
                    MinIndex = 0;
                    for (k = 1; k < K; k++)
                    {
                        TempDistance = DistanceMetric(TempVector, Centroids[k]);
                        if (MinDistance > TempDistance)
                        {
                            MinDistance = TempDistance;
                            MinIndex = k;
                        }
                    }
                    DynamiClusters[MinIndex].Add(TempVector);
                }

                for (k = 0; k < K; k++)
                {
                    Clusters[k] = ListToMatrix(DynamiClusters[k]);
                    Centroids[k] = Clusters[k].GetAverageInColumns();
                }
            }
            return Clusters;
        }
        public static Matrix[] PerformClusteringByMcQuinn(Matrix M, int K, int TotalIterations, Func<MultiDimensionalVector, MultiDimensionalVector, double> DistanceMetric)
        {
            Random R = new Random();
            int Width = M.Width;
            int TotalCount = M.Height;

            Matrix[] Clusters = new Matrix[K];
            List<MultiDimensionalVector>[] DynamiClusters = new List<MultiDimensionalVector>[K];

            MultiDimensionalVector[] Centroids = new MultiDimensionalVector[K];

            for (int i = 0, j; i < K; i++)
            {
                Centroids[i] = M.GetRow(R.Next(TotalCount));
                for (j = 0; j < i; j++)
                    if (Centroids[j].Equals(Centroids[i]))
                    {
                        i--;
                        break;
                    }
                DynamiClusters[i] = new List<MultiDimensionalVector>();
            }

            double MinDistance = Double.MaxValue;
            double TempDistance;
            int MinIndex = 0;
            MultiDimensionalVector TempVector;
            for (int i = 0, j, k; i < TotalIterations; i++)
            {
                for (j = 0; j < K; j++)
                    DynamiClusters[j].Clear();

                for (j = 0; j < M.Height; j++)
                {
                    TempVector = M.GetRow(j);
                    MinDistance = DistanceMetric(TempVector, Centroids[0]);
                    MinIndex = 0;
                    for (k = 1; k < K; k++)
                    {
                        TempDistance = DistanceMetric(TempVector, Centroids[k]);
                        if (MinDistance > TempDistance)
                        {
                            MinDistance = TempDistance;
                            MinIndex = k;
                        }
                    }
                    DynamiClusters[MinIndex].Add(TempVector);
                    //Clusters[MinIndex] = ListToMatrix(DynamiClusters[MinIndex]);
                    //Centroids[MinIndex] = Clusters[MinIndex].GetAverageInColumns();
                    Centroids[MinIndex] = (Centroids[MinIndex] * (DynamiClusters[MinIndex].Count - 1) + TempVector)
                        / DynamiClusters[MinIndex].Count;
                }
            }
            for (int j = 0; j < K; j++)
                Clusters[j] = ListToMatrix(DynamiClusters[j]);
            return Clusters;
        }
        static Matrix ListToMatrix(List<MultiDimensionalVector> VectorList)
        {
            double[,] M = new double[VectorList.Count, VectorList[0].Dimensionality];

            for (int i = 0, j; i < VectorList.Count; i++)
                for (j = 0; j < VectorList[i].Dimensionality; j++)
                    M[i, j] = VectorList[i][j];
            return new Matrix(M);
        }
    }
}
