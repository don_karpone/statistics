﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public static class Tests
    {
        public static bool hi2Test(VariationGrid V, TwoDimensionalStatistics S, double alpha)
        {
            double Sum = 0;
            for (int i = 0; i < V.XClasses; i++)
                for (int j = 0; j < V.YClasses; j++)
                {
                    double p1 = V.Grid[i, j].RelativeFrequency;
                    double p2 = Distribution.TwoDimNormalDistribution.f(V.XClassBorders[i], V.YClassBorders[j], S)/* * V.hX * V.hY*/;

                    Sum += Math.Pow(p1 - p2, 2) / p2;
                }
            if (Sum <= Quantile.Hi2(alpha, V.XClasses * V.XClasses - 2))
                return false;
            else return true;
        }

        public static bool tTest(double Value, int N, double alpha)
        {
            double t = (Value * Math.Sqrt(N - 2)) / Math.Sqrt(1 - Math.Pow(Value, 2));
            if (Math.Abs(t) <= Quantile.Student(alpha, N - 2))
                return true;
            else return false;
        }

        public static bool fTest(double Value, int v1, int v2, double alpha)
        {
            double f = Value / (1 - Value) * (double)(v1 - v2) / (v1 - 1);
            if (f > Quantile.Fisher(1 - alpha, v2 - 1, v1 - v2))
                return true;
            else return false;
        }

        public static bool uTest(double Value, int N, double alpha)
        {
            double u = (3 * Value) * Math.Sqrt(N * (N - 1)) / Math.Sqrt(2 * (2 * N + 5));
            if (Math.Abs(u) >= Quantile.Normal(alpha))
                return true;
            return false;
        }

    }
}
