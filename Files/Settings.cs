﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public class Settings
    {
        public string SampleName;
        protected int size;
        public int Size
        {
            get => size;
        }
        public double Alpha;
        public DistributionType Distribution;
        public int[] ClassesCount;
        public int XClassesCount
        {
            get => ClassesCount[0];
            set => ClassesCount[0] = value;
        }

        public Settings() => ToDefault();
        public Settings(string SampleName)
            : this() => this.SampleName = SampleName;
        public Settings(int SizeofArray) : this()
        {
            size = SizeofArray;
            ClassesCount[0] = CountClasses();
        }
        public Settings(string SampleName, int SizeofArray) : this(SizeofArray)
        {
            this.SampleName = SampleName;
        }
        public Settings(Settings Source)
        {
            this.Alpha = Source.Alpha;
            this.Distribution = Source.Distribution;
            this.SampleName = Source.SampleName;
            this.size = Source.Size;
            this.ClassesCount = Source.ClassesCount;
        }
        public virtual void ToDefault()
        {
            ClassesCount = new int[1];
            Alpha = 0.05;
            Distribution = DistributionType.None;
        }

        protected int CountClasses()
        {
            int S = (int)Math.Pow(Size, 1d / ((Size <= 100)? 2 : 3));
            return (S % 2 == 0) ? S + 1 : S;
        }
    }
    public class AdvancedSettings : Settings
    {
        public int Dimensionality
        {
            get => ClassesCount.Length;
        }
        public int YClassesCount
        {
            get => ClassesCount[1];
            set => ClassesCount[1] = value;
        }
        public string RegressionModel;
        public string AdditionalRegression;

        public AdvancedSettings() : base() => ToDefault();

        public AdvancedSettings(int SizeofArray)
            : base(SizeofArray) => YClassesCount = XClassesCount;

        public AdvancedSettings(string SampleName)
        {
            this.SampleName = SampleName;
            ToDefault();
        }

        public AdvancedSettings(string SampleName, int SizeofArray)
            : this(SizeofArray) => this.SampleName = SampleName;

        public AdvancedSettings(string SampleName, int XClassesCount, int YClassesCount)
            :this(SampleName)
        {
            ClassesCount[0] = XClassesCount;
            ClassesCount[1] = YClassesCount;
        }
        public AdvancedSettings(AdvancedSettings Source): base(Source)
        {         
            this.RegressionModel = Source.RegressionModel;
            this.AdditionalRegression = Source.AdditionalRegression;
        }
        public override void ToDefault()
        {
            base.ToDefault();
            ClassesCount = new int[2];
            RegressionModel = null;
        }
    }
    public class SuperAdvancedSettings : AdvancedSettings
    {
        public int DependentVector;
        public int[] RegressionVectors;
        public VisualizationType VizualizationType;
        public int VisualizationCount;
        public int ZClassesCount
        {
            get => ClassesCount[2];
            set => ClassesCount[2] = value;
        }

        public SuperAdvancedSettings() : base() => ToDefault();

        public SuperAdvancedSettings(int Dim, int SizeofArray)
        {
            ToDefault();
            size = SizeofArray;
            ClassesCount = new int[Dim];
            for (int i = 0; i < Dim; i++)
                ClassesCount[i] = CountClasses();
        }

        public SuperAdvancedSettings(string SampleName)
        {
            this.SampleName = SampleName;
            ToDefault();
        }

        public SuperAdvancedSettings(string SampleName, int Dimensionality, int SizeofArray)
            : this(Dimensionality, SizeofArray) => this.SampleName = SampleName;

        public SuperAdvancedSettings(SuperAdvancedSettings Source) : base(Source)
        {
            ClassesCount = new int[Source.Dimensionality];
            for (int i = 0; i < Source.Dimensionality; i++)
                ClassesCount[i] = Source.ClassesCount[i];
        }
        public SuperAdvancedSettings(string Name, params int[] ClassesCount)
        {
            SampleName = Name;
            this.ClassesCount = ClassesCount;
        }

        public override void ToDefault()
        {
            base.ToDefault();
            ClassesCount = new int[Dimensionality];
            VizualizationType = VisualizationType.DispersionDiagram;
            VisualizationCount = 9;
            RegressionModel = "";// "Лінійна (багатовимірна з вільним чл.)";
            DependentVector = Dimensionality - 1;
            RegressionVectors = new int[Dimensionality - 1];
            for (int i = 0; i < Dimensionality - 1; i++)
                RegressionVectors[i] = i;
        }
    }
}
