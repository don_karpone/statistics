﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;

namespace Statistics
{
    class PartialCorrelation
    {
        struct Key
        {
            public int I;
            public int J;
            public string D;
            public Key(int i, int j, int[] d)
            {
                I = i;
                J = j;
                D = "";
                if (d != null)
                    for (int k = 0; k < d.Length; k++)
                        D += d[k].ToString() + ";";
            }
        }

        Dictionary<Key, double> Coefficients;
        PartialCorrelation()
        {
            Coefficients = new Dictionary<Key, double>();
        }
        public PartialCorrelation(Matrix Correlation): this()
        {
            for (int i = 0, j; i < Correlation.Height; i++)
                for (j = 0; j < Correlation.Height; j++)
                    Coefficients.Add(new Key (i, j, null), Correlation[i, j]);
        }
        public double GetValue(int i, int j, int[] Set)
        {
            Key TempK = new Key (i, j, Set);

            if (!Coefficients.ContainsKey(TempK))
            {
                int[] PreviousSet = GetArrayWithOutLast(Set);
                double TempR = GetValue(i, j, PreviousSet);
                double Left = GetValue(i, Set.Last(), PreviousSet);
                double Right = GetValue(j, Set.Last(), PreviousSet);

                Coefficients.Add(TempK,
                    (TempR - Left * Right) / Math.Sqrt((1 - Left * Left) * (1 - Right * Right)));
            }
            return Coefficients[TempK];
        }
        int[] GetArrayWithOutLast(int[] Input)
        {
            if (Input.Length > 1)
            {
                int[] Output = new int[Input.Length - 1];
                Array.Copy(Input, Output, Output.Length);
                return Output;
            }
            return null;
        }
    }
    public class MultiDimensionalStatistic: Statistics
    {
        Matrix Correlation;
        PartialCorrelation PartialCorrelationCoefficients;
        public MultiDimensionalStatistic(MultiDimensionalSample Sample)
        {
            Correlation = new Matrix(Sample.Dimensionality);
            Alpha = 0.05;
            SetMathExpectations(Sample.OneDimensionalVectors);
            SetSigmas(Sample.OneDimensionalVectors);
            SetLinearCorrelation(Sample.Dimensionality, Sample.Values.ToVectorArray());
            SetPartialCorrelation(Sample.Dimensionality, Sample.Values.Height);
        }

        protected void SetMathExpectations(Sample[] OneDimensionalSamples)
        {
            string Name = "";
            AdvancedEvaluation E;
            for (int i = 0; i < OneDimensionalSamples.Length; i++)
            {
                E = OneDimensionalSamples[i].Statistic["MathExpectation"] as AdvancedEvaluation;
                Name = String.Format("MathExpectation(x{0})", i);
                Values.Add(Name, new AdvancedEvaluation("Мат. сподівання (" + i + ")", E.Data, E.BottomBorder, E.TopBorder, E.Sigma));
            }
        }

        protected void SetSigmas(Sample[] OneDimensionalSamples)
        {
            string Name = "";
            AdvancedEvaluation E;
            for (int i = 0; i < OneDimensionalSamples.Length; i++)
            {
                Name = String.Format("Sigma(x{0})", i);
                E = OneDimensionalSamples[i].Statistic["Sigma"] as AdvancedEvaluation;
                Values.Add(Name, new AdvancedEvaluation("Середньоквадратичне ("+ i + ")", E.Data, E.BottomBorder, E.TopBorder, E.Sigma));
            }
        }
        protected void SetLinearCorrelation(int Dimensionality, MultiDimensionalVector[] InputValues)
        {
            for (int i = 0, j; i < Dimensionality; i++)
                for (j = i + 1; j < Dimensionality; j++)
                {
                    SetLinearCorrelation(i, j, InputValues);
                }
        }
        protected void SetLinearCorrelation(int IndexX, int IndexY, MultiDimensionalVector[] InputValues)
        {
            int N = InputValues.Length;
            string Name1 = String.Format("Correlation({0}, {1})", IndexX, IndexY);
            string Name2 = String.Format("Correlation({0}, {1})", IndexY, IndexX);
            double r = N
                * (AverageXY(IndexX, IndexY, InputValues) - Values[String.Format("MathExpectation(x{0})", IndexX)].Data * Values[String.Format("MathExpectation(x{0})", IndexY)].Data) 
                / (Values[String.Format("Sigma(x{0})", IndexX)].Data * Values[String.Format("Sigma(x{0})", IndexY)].Data) / (N - 1);
            Values.Add(Name1, new Evaluation("Парна кореляція (" + IndexX + "; " + IndexY + ")", r));
            Values.Add(Name2, new Evaluation("Парна кореляція (" + IndexY + "; " + IndexX + ")", r));
            Correlation.SetValue(IndexX, IndexX, 1);
            Correlation.SetValue(IndexY, IndexY, 1);
            Correlation.SetValue(IndexX, IndexY, r);
            Correlation.SetValue(IndexY, IndexX, r);
        }
        protected void SetPartialCorrelation(int Dimensionality, int N)
        {
            PartialCorrelationCoefficients = new PartialCorrelation(Correlation);

            double R;
            double V;
            double V1;
            double V2;
            double U = Quantile.Normal(Alpha / 2) / (N - Dimensionality - 5);
            List<int> Arr = new List<int>();
            for (int i = 0, j, k; i < Dimensionality; i++)
                for (j = i + 1; j < Dimensionality; j++)
                {
                    Arr = new List<int>();
                    for (k = 0; k < Dimensionality; k++)
                        if (k != i && k != j)
                            Arr.Add(k);

                    R = PartialCorrelationCoefficients.GetValue(i, j, Arr.ToArray());//GetPartialCorrelation(i, j, Arr.ToArray());
                    V = 0.5 * Math.Log((1 + R) / (1 - R));
                    V1 = V - U;
                    V2 = V + U;
                    V1 = (Math.Exp(2 * V1) - 1) / (Math.Exp(2 * V1) + 1);
                    V2 = (Math.Exp(2 * V2) - 1) / (Math.Exp(2 * V2) + 1);

                    Values.Add(String.Format("PartCorrelation({0}, {1})", i, j), 
                        new AdvancedEvaluation(String.Format("Часткова кореляція({0}, {1})", i, j),R, V1, V2));
                }
        }
        public void SetMultipleCorrelation(Matrix CorrelationMatrix)
        {
            double R = 0;
            double Det = CorrelationMatrix.GetDeterminant();
            for (int i = 0; i < CorrelationMatrix.Height; i++)
            {
                R = Math.Sqrt(1 - Math.Abs(Det / CorrelationMatrix.Minor(i, i).GetDeterminant()));
                if (R != R)
                    R = 0;
                Values.Add("MultipleCorrelation(" + i.ToString() + ")", new Evaluation("Множинна кореляція(" + i.ToString() + " | інш.)", R));
            }
        }

        public static double AverageXY(int IndexX, int IndexY, MultiDimensionalVector[] Values)
        {
            if (IndexX > Values[0].Dimensionality || IndexY > Values[0].Dimensionality)
                throw new IndexOutOfRangeException();
            else
            {
                double Result = 0;
                for (int i = 0; i < Values.Length; i++)
                    Result += Values[i][IndexX] * Values[i][IndexY];
                return Result / Values.Length;
            }
        }
        public static double AverageX(int Index, MultiDimensionalVector[] Values)
        {
            if (Index > Values[0].Dimensionality)
                throw new IndexOutOfRangeException();
            else
            {
                double Result = 0;
                for (int i = 0; i < Values.Length; i++)
                    Result += Values[i][Index];
                return Result / Values.Length;
            }
        }
        public static double SigmaX(int Index, double AverageValue, MultiDimensionalVector[] Values)
        {
            if (Index > Values[0].Dimensionality)
                throw new IndexOutOfRangeException();
            else
            {
                double Result = 0;
                for (int i = 0; i < Values.Length; i++)
                    Result += Math.Pow(Values[i][Index] - AverageValue, 2);
                return Math.Sqrt(Result / (Values.Length - 2));
            }
        }
        public static double SigmaX(int Index, MultiDimensionalVector[] Values)
        {
            if (Index > Values[0].Dimensionality)
                throw new IndexOutOfRangeException();
            else
            {
                double MiddleValue = AverageX(Index, Values);
                return SigmaX(Index, MiddleValue, Values);
            }
        }
    }
}
