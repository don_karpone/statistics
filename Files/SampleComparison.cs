﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;

namespace Statistics
{
    public static class SampleComparison
    {
        const int Accuracy = 4;
        static double Round(double D) => Math.Round(D, Accuracy);


        public static string DCMatrixesConvergence(double Alpha, params MultiDimensionalSample[] Samples)
        {
            string Output = "Збіг DC-матриць:" + Environment.NewLine;

            int K = Samples.Length;
            Output += "K = " + K.ToString() + Environment.NewLine; 
            int D = Samples[0].Dimensionality;
            Output += "n = " + D.ToString() + Environment.NewLine;
            int N = 0;
            double S = 0;
            double Sum = 0;

            foreach (Sample Sp in Samples)
                if (Sp.Dimensionality != D)
                    throw new Exception("Dimensions are not equal.");
                else
                    D = Sp.Dimensionality;

            double[] Sd = new double[K];

            for (int i = 0, j; i < K; i++)
            {
                Sum = 0;
                for (j = 0; j < Samples[i].Values.Height; j++)
                    Sum += MultiDimensionalVector.ScalarMultiplying(Samples[i].Values.GetRow(j) - Samples[i].MathExpectation, Samples[i].Values.GetRow(j) - Samples[i].MathExpectation);
                Sd[i] = Sum / (Samples[i].Values.Height - 1);
                Output += "S[" + i.ToString() + "] = " + Round(Sd[i]).ToString() + Environment.NewLine;

                S += Sum;
                N += Samples[i].Values.Height;
            }
            S /= (N - K);
            Output += "S = " + Round(S).ToString() + Environment.NewLine;

            double V = 0;
            for (int i = 0; i < K; i++)
                V += (Samples[i].Values.Height - 1) / 2 * Math.Log(Math.Abs(S) / Math.Abs(Sd[i]));
            Output += "V = " + Round(V).ToString() + Environment.NewLine;

            if (V <= Quantile.Hi2(1 - Alpha, D * (D + 1) * (K - 1) / 2))
                Output += "DC-матриці збігаються";
            else
                Output += "DC-матриці не збігаються";
            return Output;
        }
        public static string DCMatrixesAreNotEqual(double Alpha, params MultiDimensionalSample[] Samples)
        {
            string Output = "Збіг к-середніх за умови розбіжності DC-матриць:" + Environment.NewLine;
            int K = Samples.Length;
            Output += "K = " + K.ToString() + Environment.NewLine;
            int D = Samples[0].Dimensionality;
            Output += "n = " + D.ToString() + Environment.NewLine;
            MultiDimensionalVector Average = new MultiDimensionalVector(D);
            double Sum = 0;

            foreach (Sample S in Samples)
                if (S.Dimensionality != D)
                    throw new Exception("Dimensions are not equal.");
                else
                    D = S.Dimensionality;

            double[] Sd = new double[K];

            for(int i = 0, j; i < K; i++)
            {
                Sum = 0;
                for (j = 0; j < Samples[i].Values.Height; j++)
                    Sum += MultiDimensionalVector.ScalarMultiplying(Samples[i].Values.GetRow(j) - Samples[i].MathExpectation, Samples[i].Values.GetRow(j) - Samples[i].MathExpectation);
                Sd[i] = Sum / (Samples[i].Values.Height - 1);
                Output += "S[" + i.ToString() + "] = " + Round(Sd[i]).ToString() + Environment.NewLine;
            }

            Sum = 0;
            for (int i = 0; i < K; i++)
            {
                Sum += Samples[i].Values.Height / Sd[i];
                Average += (Samples[i].Values.Height / Sd[i]) * Samples[i].MathExpectation;
            }
            Average = (1d / Sum) * Average;
            Output += "x(ceр. заг.) = " + MultiDimensionalVector.Round(Average, Accuracy).ToString() + Environment.NewLine;

            double V = 0;
            for (int i = 0; i < K; i++)
                V += Samples[i].Values.Height * MultiDimensionalVector.ScalarMultiplying((Samples[i].MathExpectation - Average) / Sd[i], Samples[i].MathExpectation - Average);
            Output += "V = " + Round(V).ToString() + Environment.NewLine;

            if (V <= Quantile.Hi2(1 - Alpha, D * (K - 1)))
                Output += "Cередні збігаються";
            else
                Output += "Cередні не збігаються";
            return Output;
        }
        public static string MathExpectationsConvergence(double Alpha, MultiDimensionalSample Sample1, MultiDimensionalSample Sample2)
        {
            string Output = "Збіг cередніх за умови збігу DC-матриць:" + Environment.NewLine;

            if (Sample1.Dimensionality != Sample2.Dimensionality)
                return "Вибірки мають різну розмірність.";

            int Dim = Sample1.Dimensionality;
            int N1 = Sample1.Values.Height;
            int N2 = Sample2.Values.Height;

            Matrix S0 = new Matrix(Dim);
            Matrix S1 = new Matrix(Dim);
            double Sum1;
            double Sum2;
            for (int i = 0, j; i < Dim; i++)
                for (j = 0; j < Dim; j++)
                {
                    Sum1 = Sum(Sample1.Values, i, j) + Sum(Sample2.Values, i, j);
                    Sum2 = (Sample1.Values.GetColumn(i).ToArray().Sum() + Sample2.Values.GetColumn(i).ToArray().Sum())
                        * (Sample1.Values.GetColumn(j).ToArray().Sum() + Sample2.Values.GetColumn(j).ToArray().Sum()) / (N1 + N2);

                    S0.SetValue(i, j,(Sum1 - Sum2) / (N1 + N2 - 2));

                    Sum2 = Sample1.Values.GetColumn(i).ToArray().Sum() * Sample1.Values.GetColumn(j).ToArray().Sum() / N1 
                        + Sample2.Values.GetColumn(i).ToArray().Sum() * Sample2.Values.GetColumn(j).ToArray().Sum() / N2;
                    S1.SetValue(i, j, (Sum1 - Sum2) / (N1 + N2 - 2));
                }

            double V = -(N1 + N2 - 2 - (double)Dim / 2) * Math.Log(S1.GetDeterminant() / S0.GetDeterminant());
            Output += "V = " + Round(V).ToString() + Environment.NewLine;

            if (V <= Quantile.Hi2(Alpha, Dim))
                Output += "Середні збігаються";
            else
                Output += "Середні не збігаються";
            return Output;
        }
        static double Sum(Matrix Values, int Vector1, int Vector2)
        {
            double Output = 0;
            for (int l = 0; l < Values.Height; l++)
                Output += Values[l, Vector1] * Values[l, Vector2];
            return Output;
        }
        static double Sum(MultiDimensionalVector[] Values, int Vector)
        {
            double Output = 0;
            for (int l = 0; l < Values.Length; l++)
                Output += Values[l][Vector];
            return Output;
        }
        public static string RegressionComparison(TwoDimensionalSample First, TwoDimensionalSample Secound)
        {
            string Output = "Збіг регресій:" + Environment.NewLine + Environment.NewLine;
            if (First == Secound)
                Output += "Вибірки ідентичні." + Environment.NewLine;
            else
            {
                double S1 = First.Regression["ResidualDispersion"].Data;
                double S2 = Secound.Regression["ResidualDispersion"].Data;
                double f = (S1 >= S2) ?
                    S1 / S2 :
                    S2 / S1;
                int N1 = First.Values.Length;
                int N2 = Secound.Values.Length;
                if (f <= Quantile.Fisher(1 - First.Alpha, N1 - 2, N2 - 2))
                {
                    double S = ((N1 - 2) * S1 + (N2 - 2) * S2) / (N1 + N2 - 4);

                    double Sigma1 = First.Statistic["Sigma(x)"].Data;
                    double Sigma2 = Secound.Statistic["Sigma(x)"].Data;

                    double b1 = First.Regression["b"].Data;
                    double b2 = Secound.Regression["b"].Data;

                    double t = (b1 - b2)
                        / (S * Math.Sqrt(1d / (Sigma1 * Sigma1 * (N1 - 1)) + 1d / (Sigma2 * Sigma2 * (N2 - 1))));
                    if (Tests.tTest(t, N1 + N2 - 4, 1 - First.Alpha / 2))
                    {
                        double b = ((N1 - 1) * Sigma1 * Sigma1 * b1 + (N2 - 1) * Sigma2 * Sigma2 * b2) / ((N1 - 1) * Sigma1 * Sigma1 + (N2 - 1) * Sigma2 * Sigma2);

                        double b0 = (First.Statistic["MathExpectation(y)"].Data - Secound.Statistic["MathExpectation(y)"].Data)
                            / (First.Statistic["MathExpectation(x)"].Data - Secound.Statistic["MathExpectation(x)"].Data);

                        double S0 = S * S * (1d / ((N1 - 1) * Sigma1 * Sigma1 + (N2 - 1) * Sigma2 * Sigma2) + 1d / Math.Pow(First.Statistic["MathExpectation(x)"].Data - Secound.Statistic["MathExpectation(x)"].Data, 2) * (1d / N1 + 1d / N2));

                        if (Tests.tTest((b - b0) / S0, N1 + N2 - 4, 1 - First.Alpha / 2))
                            Output += "Регресії співпадають." + Environment.NewLine;
                        else
                            Output += "Регресії не збіжні." + Environment.NewLine;
                    }
                    else
                        Output += "Регресії не збіжні. Кути нахилу прямих відрізняються." + Environment.NewLine;
                }
                else
                    Output += "Регресії не збіжні. Дисперсії не збігаються." + Environment.NewLine;
            }
            return Output;
        }

        //////////////////////////////////////////КАРАНТИН//////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////ОПАСНО////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////НЕ ВЛЕЗАЙ, УБЬЕТ///////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string Bartlett(params Sample[] Samples)
        {
            string Output = "Критерій Бартлетта:" + Environment.NewLine + Environment.NewLine;
            int K = Samples.Length;
            if (K >= 2)
            {
                Output += "k = " + K.ToString() + Environment.NewLine;
                Output += "ν = " + (K - 1).ToString() + Environment.NewLine;
                Output += "α = " + Samples[0].Alpha.ToString() + Environment.NewLine;
                double[] SI = new double[K];
                int[] NI = new int[K];

                double SumS = 0;
                double SumN = 0;
                for (int i = 0; i < K; i++)
                {
                    SI[i] = Samples[i].Statistic["Dispersion"].Data;
                    NI[i] = Samples[i].VariationList.Size;
                    SumS += (NI[i] - 1) * SI[i];
                    SumN += NI[i] - 1;
                }
                double S = SumS / SumN;
                Output += "S^2 = " + Round(S).ToString() + Environment.NewLine;

                double B = Criterion.Bartlett.B(SI, NI, S, K);
                Output+= "B = " + Round(B).ToString() + Environment.NewLine;

                double C = Criterion.Bartlett.C(NI, K);
                Output += "C = " + Round(C).ToString() + Environment.NewLine;

                double Hi = B / C;
                Output += "χ^2 = " + Round(Hi).ToString() + Environment.NewLine;

                double Hi2 = Quantile.Hi2(1 - Samples[0].Alpha, K - 1);
                Output += "χ^2(1 - α ,ν) = " + Round(Hi2).ToString() + '\n';

                if (Hi <= Hi2)
                    Output += "Згідно критерію Бартлетта, дисперсії вибірок збігаються;" + Environment.NewLine;
                else
                    Output += "Згідно критерію Бартлетта, дисперсії вибірок не збігаються;" + Environment.NewLine;
            }
            else
            {
                Output = "Кількість обраних вибірок менше 2-х. Оберіть достатню кількість вибірок та повторіть спробу.";
            }
            return Output;
        }

        public static string FTest(Sample Sample1, Sample Sample2)
        {
            string Output = "F-тест:" + Environment.NewLine + Environment.NewLine;

            Output += "α = " + Sample1.Alpha.ToString() + Environment.NewLine;

            double v1 = Sample1.VariationList.Size - 1;
            Output += "v1 = " + v1.ToString() + Environment.NewLine;

            double v2 = Sample2.VariationList.Size - 1;
            Output += "v2 = " + v2.ToString() + Environment.NewLine;

            double S1 = Sample1.Statistic["Dispersion"].Data;
            Output += "S^2 (1)= " + Round(S1).ToString() + Environment.NewLine;

            double S2 = Sample2.Statistic["Dispersion"].Data;
            Output += "S^2 (2)= " + Round(S2).ToString() + Environment.NewLine;

            double f = 0;
                if (S1 >= S2)
                    f = S1 / S2;
                else
                    f = S2 / S1;
            Output += "f = " + Round(f).ToString() + Environment.NewLine;

            double fA = Quantile.Fisher(Sample1.Alpha, v1, v2);
            Output += "f(1 - α ,ν1, ν2) = " + Round(fA).ToString() + Environment.NewLine;

            if (f <= fA)
                Output += "Згідно F-тесту, дисперсії вибірок збігаються;" + Environment.NewLine;
            else
                    Output += "Згідно F-тесту, дисперсії вибірок не збігаються;" + Environment.NewLine;

            return Output;
        }

        public static string OneFactorDispersion(params Sample[] Samples)
        {
            string Output = "Однофакторний дисперсійний аналіз:" + Environment.NewLine + Environment.NewLine;
            int K = Samples.Length;
            if (K >= 2)
            {
                double X = 0;
                int N = 0;
                int[] Ni = new int[K];
                double[] x = new double[K];
                double[] Si = new double[K];
                for (int i = 0; i < K; i++)
                {
                    x[i] = Samples[i].Statistic["MathExpectation"].Data;
                    Ni[i] = Samples[i].VariationList.Size;
                    X += x[i] * Ni[i];
                    N += Ni[i];

                    Si[i] = Samples[i].Statistic["Dispersion"].Data;
                }
                X /= N;

                Output += "α = " + Samples[0].Alpha.ToString() + Environment.NewLine;
                int v1 = K - 1;
                Output += "\tv1 = " + v1.ToString() + Environment.NewLine;
                int v2 = N - K;
                Output += "\tv2 = " + v2.ToString() + Environment.NewLine;


                double SM = 0;
                double SB = 0;
                for (int i = 0; i < K; i++)
                {
                    SM += Ni[i] * Math.Pow(x[i] - X, 2);
                    SB += (Ni[i] - 1) * Si[i];
                }
                SM /= K - 1;
                SB /= N - K;
                Output += "SM = " + Round(SM).ToString() + Environment.NewLine;
                Output += "SB = " + Round(SB).ToString() + Environment.NewLine;

                double F = SM / SB;
                Output += "F = " + Round(F).ToString() + Environment.NewLine;

                double f = Quantile.Fisher(Samples[0].Alpha, v1, v2);
                Output += "f(1 - α ,ν1, ν2) = " + Round(f).ToString() + Environment.NewLine;

                if (F <= f)
                    Output += "Згідно аналізу, різниця між середніми вибірок є несуттєвою;";
                else
                    Output += "Згідно аналізу, різниця між середніми вибірок є суттєвою;";

            }
            else
            {
                Output = "Кількість обраних вибірок менше 2-х. Оберіть достатню кількість вибірок та повторіть спробу.";
            }
            return Output;
        }

        public static string AverageConvergenceForIndependent(Sample Sample1, Sample Sample2)
        {
            string Output = "Збіг середніх для незалежних виб-к:" + Environment.NewLine + Environment.NewLine;
            Output += "α = " + Sample1.Alpha.ToString() + Environment.NewLine;

            int N1 = Sample1.VariationList.Size;
            int N2 = Sample2.VariationList.Size;
            int v = N1 + N2 - 2;
            Output += "v = " + v.ToString() + Environment.NewLine;

            double x = Sample1.Statistic["MathExpectation"].Data;
            Output += "x(сер.) = " + Round(x).ToString() + Environment.NewLine;
            double Sx = Sample1.Statistic["Sigma"].Data;
            Output += "Sx = " + Round(Sx).ToString() + Environment.NewLine;
            double y = Sample2.Statistic["MathExpectation"].Data;
            Output += "y(сер.) = " + Round(y).ToString() + Environment.NewLine;
            double Sy = Sample2.Statistic["Sigma"].Data;
            Output += "Sy = " + Round(Sy).ToString() + Environment.NewLine;

            if (x == y)
                Output += "Згідно аналізу, середні двох вибірок однакові;";
            else
            {
                double t = (x - y) / Math.Sqrt(Sx / N1 + Sy / N2);
                Output += "t = " + Round(t).ToString() + Environment.NewLine;

                double tQ = Quantile.Student(1 - Sample1.Alpha / 2, v);
                Output += "t(1 - α/2,ν1) = " + Round(tQ).ToString() + Environment.NewLine;

                if (t <= tQ)
                    Output += "Згідно аналізу, середні двох вибірок збігаються;";
                else
                    Output += "Згідно аналізу, середні двох вибірок не збігаються;";
            }
            return Output;
        }

        public static string AverageConvergence(Sample Sample1, Sample Sample2)
        {
            string Output = "Збіг середніх для залежних виб-к" + Environment.NewLine + Environment.NewLine;
            Output += "α = " + Sample1.Alpha.ToString() + Environment.NewLine;

            int N = Sample1.VariationList.Size;
            double[] Z = new double[N];
            double Sum = 0;
            for (int i = 0; i < N; i++)
            {
                Z[i] = Sample1.Values[i] - Sample2.Values[i];
                Sum += Z[i];
            }
            double z = Sum / N;
            if (z == 0)
                Output += "Згідно аналізу, середні двох вибірок однакові;" + Environment.NewLine;
            else
            {
                Output += "z(сер.) = " + Round(z).ToString() + Environment.NewLine;

                Sum = 0;
                for (int i = 0; i < N; i++)
                    Sum += Math.Pow(Z[i] - z, 2);

                double S = Sum / (N - 1);
                Output += "S^2 = " + Round(S).ToString() + Environment.NewLine;

                double t = z * Math.Sqrt(N) / Math.Sqrt(S);
                Output += "t = " + Round(t).ToString() + Environment.NewLine;

                int v = N - 2;
                double tQ = Quantile.Student(1 - Sample1.Alpha / 2, v);
                Output += "t(1 - α/2,ν) = " + Round(tQ).ToString()  +Environment.NewLine;

                if (Math.Abs(t) <= tQ)
                    Output += "Згідно аналізу, середні двох вибірок збігаються;" + Environment.NewLine;
                else
                    Output += "Згідно аналізу, середні двох вибірок не збігаються;" + Environment.NewLine;
            }
            return Output;
        }

        public static string Wilcoxon(Sample Sample1, Sample Sample2)
        {
            string Output = "Збіг середніх для залежних виб-к:" + Environment.NewLine + Environment.NewLine;
            Output += "α = " + Sample1.Alpha.ToString() + Environment.NewLine;

            int N1 = Sample1.VariationList.Size;
            int N2 = Sample2.VariationList.Size;
            int N = N1 + N2;

            double W = 0;
            double[] z = SetArray(Sample1.Values, Sample2.Values);
            double[] r = SetRang(z);
            for (int i = 0, j = 0; i < N1; i++)
            {
                for (; j < N; j++)
                    if (z[j] == Sample1.Values[i])
                    {
                        W += r[j];
                        j++;
                        break;
                    }
            }
            Output += "W = " + W.ToString() + Environment.NewLine;

            double E = Criterion.Wilcoxon.E(N1, N);
            Output += "E = " + Round(E).ToString() + Environment.NewLine;

            double D = Criterion.Wilcoxon.D(N1, N2, N);
            Output += "D = " + Round(D).ToString() + Environment.NewLine;

            double w = Criterion.Wilcoxon.w(W, E, D);
            Output += "w = " + Round(w).ToString() + Environment.NewLine;

            double u = Quantile.Normal(Sample1.Alpha / 2);
            Output += "u(1 - α/2) = " + Round(u).ToString() + Environment.NewLine;

            if (w <= u)
                Output += "Згідно аналізу, вибірки однорідні;" + Environment.NewLine;
            else
                Output += "Згідно аналізу, вибірки не однорідні;" + Environment.NewLine;
            return Output;
        }

        static public string MannUitney(Sample Sample1, Sample Sample2)
        {
            string Output = "Збіг середніх для залежних виб-к:" + Environment.NewLine;
            Output += "α = " + Sample1.Alpha.ToString() + Environment.NewLine;

            int N1 = Sample1.VariationList.Size;
            int N2 = Sample2.VariationList.Size;
            int N = N1 + N2;

            double W = 0;
            double r = 1;
            for (int i = 0, j = 0; i < N1 && j < N1; i++, j++)
            {
                if (Sample1.Values[i] == Sample2.Values[j])
                {
                    r += r;
                    r /= 2;
                    W += r;
                    r += 0.5;
                }
                else if (Sample1.Values[i] < Sample2.Values[j])
                {
                    W += r;
                    r++;
                    i++;
                }
                else if (Sample1.Values[i] > Sample2.Values[j])
                {
                    j++;
                    r++;
                }

            }
            double[] z = new double[N];
            double U = 0;
            for (int i = 0, j = 0; i < N1 && j < N1; i++, j++)
            {
                for (int k = 0; k < N2; k++)
                    if (Sample1.Values[i] > Sample2.Values[k])
                        U++;
            }
            Output += "U = " + Round(U).ToString() + Environment.NewLine;

            double E = Criterion.Witney.E(N1, N2);
            Output += "E = " + Round(E).ToString() + Environment.NewLine;

            double D = Criterion.Witney.D(N1, N2, N);
            Output += "D = " + Round(D).ToString() + Environment.NewLine;

            double u = Criterion.Witney.u(U, E, D);
            Output += "u = " + Round(u).ToString() + Environment.NewLine;

            double uQ = Quantile.Normal(Sample1.Alpha / 2);
            Output += "u(1 - α/2) = " + Round(uQ).ToString() + Environment.NewLine;

            if (u <= uQ)
                Output += "Згідно аналізу, вибірки однорідні;" + Environment.NewLine;
            else
                Output += "Згідно аналізу, вибірки не однорідні;" + Environment.NewLine;
            return Output;
        }

        static public string Symbol(Sample Sample1, Sample Sample2)
        {
            string Output = "Критерій знаків:" + Environment.NewLine + Environment.NewLine;

            int N = Sample1.VariationList.Size;

            double[] z = new double[N];
            List<int> U = new List<int>();
            int Sum = 0;
            for (int i = 0, j = 0; i < N; i++, j++)
            {
                z[i] = Sample1.Values[i] - Sample2.Values[i];
                if (z[i] > 0)
                    U.Add(1);
                else if (z[i] < 0)
                    U.Add(0);
                Sum += U[i];
            }
            Output += "U = " + Round(Sum).ToString() + Environment.NewLine;

            N = U.Count;
            Output += "N = " + Round(N).ToString() + Environment.NewLine;

            double Ss = 0;
            if (N > 15)
            {
                Ss = (2 * Ss - 1 - N) / Math.Sqrt(N);
                Output += "S = " + Round(Ss).ToString() + Environment.NewLine;

                double uQ = Quantile.Normal(Sample1.Alpha);
                Output += "u(1 - α/2) = " + Round(uQ).ToString() + Environment.NewLine;

                if (Ss <= uQ)
                    Output += "Згідно аналізу, вибірки однорідні;" + Environment.NewLine;
                else
                    Output += "Згідно аналізу, вибірки не однорідні;" + Environment.NewLine;
            }
            else
            {
                double a = Criterion.Symbol.Alpha(N, Sum);
                Output += "α0 = " + Round(a).ToString() + Environment.NewLine;

                if (a >= Sample1.Alpha)
                    Output += "Згідно аналізу, вибірки однорідні;" + Environment.NewLine;
                else
                    Output += "Згідно аналізу, вибірки не однорідні;" + Environment.NewLine;
            }
            return Output;
        }

        public static string Cruscal(params Sample[] Samples)
        {
            string Output = "Критерій Крускала-Уолліса:" + Environment.NewLine + Environment.NewLine;
            int K = Samples.Length;
            if (K > 2)
            {
                double[] z = new double[Samples[0].Values.Length];
                Array.Copy(Samples[0].Values, z, z.Length);

                for (int cn = 1; cn < K; cn++)
                {
                    z = SetArray(z, Samples[cn].Values);
                }
                double[] r = SetRang(z);

                double[] Wm = new double[K];
                int[] N = new int[K];
                int Nn = 0;
                for (int cn = 0; cn < K; cn++)
                {
                    Wm[cn] = 0;
                    N[cn] = Samples[cn].VariationList.Size;
                    Nn += N[cn];
                    for (int i = 0, j = 0; i < N[cn]; i++)
                    {
                        for (; j < N[cn]; j++)
                            if (z[j] == Samples[cn].Values[i])
                            {
                                Wm[cn] += r[j];
                                j++;
                                break;
                            }
                    }
                    Wm[cn] /= N[cn];
                }

                double H = Criterion.Kruskal.H(Wm, N, Nn);
                Output += "χ^2 = " + Round(H).ToString() + Environment.NewLine;

                double Hi2 = Quantile.Hi2(1 - Samples[0].Alpha, K - 1);
                Output += "χ^2(1 - α ,ν) = " + Round(Hi2).ToString() + Environment.NewLine;

                if (H <= Hi2)
                    Output += "Згідно аналізу, вибірки однорідні;" + Environment.NewLine;
                else
                    Output += "Згідно аналізу, вибірки не однорідні;" + Environment.NewLine;
            }
            return Output;
        }

        public static string Minus(Sample Sample1, Sample Sample2)
        {
            string Output = "Критерій різниці середніх рангів:" + Environment.NewLine + Environment.NewLine;

            int N1 = Sample1.VariationList.Size;
            int N2 = Sample2.VariationList.Size;
            double[] Rx = SetRang(Sample1.Values);
            double rX = 0;
            for (int i = 0; i < N1; i++)
            {
                rX += Rx[i];
            }
            rX /= N1;
            Output += "Rx = " + Round(rX).ToString() + Environment.NewLine;

            double[] Ry = SetRang(Sample2.Values);
            double rY = 0;
            for (int i = 0; i < N2; i++)
            {
                rY += Ry[i];
            }
            rY /= N2;
            Output += "Ry = " + Round(rY).ToString() + Environment.NewLine;

            double v = 0;
            if (rX != rY)
            {
                v = Criterion.MinusMed.v(rX, rY, N1, N2);
            }
            Output += "v = " + Round(v).ToString() + Environment.NewLine;

            double u = Quantile.Normal(Sample1.Alpha / 2);
            Output += "u(1 - α/2) = " + Round(u).ToString() + Environment.NewLine;

            if (v <= u)
                Output += "Згідно аналізу, вибірки однорідні;" + Environment.NewLine;
            else
                Output += "Згідно аналізу, вибірки не однорідні;" + Environment.NewLine;

            return Output;
        }

        private static double[] SetRang(double[] x)
        {
            int N = x.Length;
            double[] r = new double[N];

            int Repeat;
            int r2;
            int Sum = 0;
            for (int r1 = 0; r1 < N; r1++)
            {
                Repeat = 1;
                Sum = r1 + 1;
                for (r2 = r1 + 1; r2 < N; r2++)
                {
                    if (x[r2] == x[r1])
                    {
                        Repeat++;
                        Sum += r2 + 1;
                    }
                    else
                        break;
                }
                for (int res = r1; res < r2; res++)
                    r[res] = (double)Sum / Repeat;
                r1 = r2 - 1;
            }
            return r;
        }
        private static double[] SetArray(double[] x, double[] y)
        {
            int Nx = x.Length;
            int Ny = y.Length;
            int N = Nx + Ny;
            List<double> r = new List<double>();
            for (int i = 0, j = 0; i + j < N;)
            {
                if (i < Nx && j < Ny)
                {
                    if (x[i] <= y[j])
                    {
                        r.Add(x[i]);
                        i++;
                    }
                    else
                    {
                        r.Add(x[i]);
                        j++;
                    }
                }
                else if (j < Ny)
                {
                    r.Add(y[j]);
                    j++;
                }
                else if (i < Nx)
                {
                    r.Add(x[i]);
                    i++;
                }
                else
                    break;
            }

            double[] z = r.ToArray();

            return z;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
