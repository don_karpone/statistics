﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace Statistics
{
    public enum VisualizationType { HeatMap, DispersionDiagram, Bubble, ParallelCoordinates, DiagnosticDiagram}

    public class VisualizationChart: Chart
    {
        ChartArea MainArea;
        Series MainSeries;
        public VisualizationChart()
        {
            this.Dock = System.Windows.Forms.DockStyle.Fill;
            MainArea = new ChartArea
            {
                Name = "MainArea",
                BackColor = System.Drawing.Color.Black
            };
            MainArea.AxisX.IsInterlaced = true;
            MainArea.AxisY.IsInterlaced = true;
            MainArea.AxisX.LineWidth = 1;
            MainArea.AxisY.LineWidth = 1;
            MainArea.AxisX.LineColor = System.Drawing.Color.White;
            MainArea.AxisY.LineColor = System.Drawing.Color.White;
            MainArea.AxisX.IsMarginVisible = false;
            MainArea.AxisY.IsMarginVisible = false;

            MainSeries = new Series();
            MainSeries.Color = System.Drawing.Color.Red;
            MainSeries.IsVisibleInLegend = false;

            ChartAreas.Add(MainArea);
            Series.Add(MainSeries);
        }
        public VisualizationChart(MultiLinearRegression Regres, double[] Y, Algebra.MultiDimensionalVector[] Values): this()
        {
            SetDiagnosticDiagramm(Y, Values, Regres);
        }
        public VisualizationChart(Sample S) : this() => SetHistogram(S.VariationList);
        //public VisualizationChart(Algebra.MultiDimensionalVector[] Values, int X, int Y) : this() => SetCorrelation(Values, X, Y);
        public VisualizationChart(double[] X, double[] Y) : this() => SetCorrelation(X, Y);
        public VisualizationChart(Algebra.MultiDimensionalVector[] Values, VisualizationType Type) : this()
        {
            switch (Type)
            {
                case VisualizationType.Bubble:
                    SetBubble(Values);
                    break;
                case VisualizationType.ParallelCoordinates:
                    SetParallelCoordinates(Values);
                    break;
                default:
                    throw new Exception("Invalid parameter.");
            }
        }
        private void SetHistogram(VariationLine L)
        {
            MainArea.AxisX.Interval = Math.Round(L.h, 4);
            MainArea.AxisX.Minimum = Math.Round(L.VariationList[0].Min, 4);
            MainArea.AxisX.Maximum = Math.Round(L.VariationList[L.ClassNumbers - 1].Max, 4) + 0.001;

            MainSeries.ChartType = SeriesChartType.Column;
            MainSeries["PointWidth"] = "1";
            MainSeries.BorderColor = System.Drawing.Color.Black;
            for(int i = 0; i < L.ClassNumbers; i++)
                MainSeries.Points.AddXY(L.VariationList[i].ClassValue, L.VariationList[i].RelativeFrequency);
        }
        private void SetCorrelation(double[] X, double[] Y)
        {
            MainArea.AxisX.Minimum = Math.Round(X.Min(), 4);
            MainArea.AxisY.Minimum = Math.Round(Y.Min(), 4);
            MainArea.AxisX.Maximum = Math.Round(X.Max(), 4);
            MainArea.AxisY.Maximum = Math.Round(Y.Max(), 4);

            MainSeries.ChartType = SeriesChartType.Point;
            for (int i = 0; i < X.Length; i++)
                MainSeries.Points.AddXY(X[i], Y[i]);
        }
        //private void SetCorrelation(Algebra.MultiDimensionalVector[] Values, int X, int Y)
        //{
        //    MainArea.AxisX.Minimum = Math.Round(Algebra.MultiDimensionalVector.Min(Values, X), 4);
        //    MainArea.AxisY.Minimum = Math.Round(Algebra.MultiDimensionalVector.Min(Values, Y), 4);
        //    MainArea.AxisX.Maximum = Math.Round(Algebra.MultiDimensionalVector.Max(Values, X), 4);
        //    MainArea.AxisY.Maximum = Math.Round(Algebra.MultiDimensionalVector.Max(Values, Y), 4);

        //    MainSeries.ChartType = SeriesChartType.Point;
        //    for(int i = 0; i < Values.Length; i++)
        //        MainSeries.Points.AddXY(Values[i][X], Values[i][Y]);
        //}
        private void SetBubble(Algebra.MultiDimensionalVector[] Values)
        {
            MainArea.AxisX.Title = "X";
            MainArea.AxisY.Title = "Y";

            MainSeries.ChartType = SeriesChartType.Bubble;

            MainSeries.BorderColor = System.Drawing.Color.Black;
            MainSeries.MarkerStyle = MarkerStyle.Circle;
            MainSeries["BubbleScaleMin"] = "0";
            MainSeries.BorderWidth = 1;
            MainArea.AxisX.Minimum = Math.Round(Algebra.MultiDimensionalVector.Min(Values, 0), 4);
            MainArea.AxisY.Minimum = Math.Round(Algebra.MultiDimensionalVector.Min(Values, 1), 4);
            MainArea.AxisX.Interval = Math.Round((Algebra.MultiDimensionalVector.Max(Values, 0) - MainArea.AxisX.Minimum) / 5, 4);
            MainArea.AxisY.Interval = Math.Round((Algebra.MultiDimensionalVector.Max(Values, 1) - MainArea.AxisY.Minimum) / 5, 4);

            for (int i = 0; i < Values.Length; i++)
                MainSeries.Points.AddXY(Values[i][0], Values[i][1], Values[i][2]);
        }
        private void SetParallelCoordinates(Algebra.MultiDimensionalVector[] Values)
        {
            MainArea.AxisX.Title = "Вектори";
            MainArea.AxisY.Title = "Зн-ня";
            MainArea.AxisY.Interval = 1;
            MainArea.AxisY.Minimum = 0;
            this.Series.Clear();
            Series S;
            Algebra.MultiDimensionalVector Minimums = new Algebra.MultiDimensionalVector(Values[0].Dimensionality);
            Algebra.MultiDimensionalVector Maximums = new Algebra.MultiDimensionalVector(Values[0].Dimensionality);
            for (int i = 0; i < Maximums.Dimensionality; i++)
            {
                Minimums[i] = Algebra.MultiDimensionalVector.Min(Values, i);
                Maximums[i] = Algebra.MultiDimensionalVector.Max(Values, i) - Minimums[i];
            }

            for (int i = 0, j; i < Values.Length; i++)
            {
                S = new Series
                {
                    Color = System.Drawing.Color.Red,
                    ChartArea = "MainArea",
                    ChartType = SeriesChartType.Line,
                    IsVisibleInLegend = false
                };
                for (j = 0; j < Values[i].Dimensionality; j++)
                    S.Points.AddXY(j, (Values[i][j] - Minimums[j]) / Maximums[j]);
                Series.Add(S);
            }
        }
        private void SetDiagnosticDiagramm(double[] YVector, Algebra.MultiDimensionalVector[] Values, MultiLinearRegression multiLinearRegression)
        {
            MainArea.AxisX.Title = "Y";
            MainArea.AxisY.Title = "eps";
            MainArea.AxisX.Interval = Math.Round((YVector.Max() - YVector.Min()) / 6, 4);
            MainArea.AxisX.Minimum = Math.Round(YVector.Min(), 4);


            MainSeries.ChartType = SeriesChartType.Point;
            MainSeries.Color = System.Drawing.Color.Red;
            for (int i = 0; i < Values.Length; i++)
                MainSeries.Points.AddXY(YVector[i], /*Math.Abs(*/Math.Round(YVector[i] - multiLinearRegression.Function(Values[i]), 4)/*)*/);

            Series ZeroSeries = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                ChartType = SeriesChartType.Line,
                Color = System.Drawing.Color.White,
                ChartArea = "MainArea"
            };
            ZeroSeries.Points.AddXY(YVector.Min(), 0);
            ZeroSeries.Points.AddXY(YVector.Max(), 0);
            Series.Add(ZeroSeries);
        }
    }
}
