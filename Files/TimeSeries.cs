﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;

namespace Statistics
{
    public class TimeSeries
    {
        double[] _Values;
        double[] Values;
        public double[] GetValues() => Values;
        public int Length
        {
            get => Values.Length;
        }
        public double this[int Index]
        {
            get => Values[Index];
        }

        public double Average
        {
            get => average;
        }
        double average;
        public double Dispersion
        {
            get => dispersion;
        }
        double dispersion;

        public double Trend(double t) => GetTrend(t);
        Func<double, double> GetTrend;

        MultiDimensionalVector PredictionVector;
        Matrix A;

        public TimeSeries(double[] Values)
        {
            this.Values = new double[Values.Length];
            Array.Copy(Values, this.Values, Values.Length);
            this._Values = new double[Values.Length];
            Array.Copy(Values, this._Values, Values.Length);

            average = Values.Average();
            dispersion = GetDispersion();

            SetTrend();
        }
        public void SetNewValues(double[] NewValues)
        {
            this.Values = new double[NewValues.Length];
            Array.Copy(NewValues, this.Values, Values.Length);

            average = Values.Average();
            dispersion = GetDispersion();

            SetTrend();
        }
        double GetDispersion()
        {
            double D = 0;
            for (int i = 0; i < Values.Length; i++)
                D += (Values[i] - Average) * (Values[i] - Average);
            return D / (Values.Length - 1);
        }

        public double Autocovariation(int Tau)
        {
            double D = 0;
            for (int i = 0; i < Values.Length - Tau; i++)
                D += (Values[i] - Average) * (Values[i + Tau] - Average);
            return D / (Values.Length - Tau - 1);
        }

        public double Autocorellation(int Tau) => Autocovariation(Tau) / Autocovariation(0);

        public void SetSmoothed(int WindowSize, Func<double[], int, double[]> Smoothing)
        {
            Values = Smoothing(Values, WindowSize);
            average = Values.Average();
            dispersion = GetDispersion();

            SetTrend();
        }

        public double[] SMA(double[] Values, int WindowSize)
        {
            double[] SmoothedValues = new double[Values.Length - WindowSize];

            for(int i = 0; i < WindowSize; i++)
                SmoothedValues[0] += Values[WindowSize - i];
            SmoothedValues[0] /= WindowSize;

            for (int i = 1; i < Values.Length - WindowSize; i++)
                SmoothedValues[i] = SmoothedValues[i - 1] + (Values[i + WindowSize] - Values[i]) / WindowSize;

            return SmoothedValues;
        }

        public double[] WMA(double[] Values, int WindowSize)
        {
            double[] SmoothedValues = new double[Values.Length - WindowSize];
            double Addition = 2d / WindowSize / (WindowSize + 1);

            for (int i = 0, j; i < Values.Length - WindowSize; i++)
            {
                for (j = 0; j < WindowSize; j++)
                    SmoothedValues[i] += (WindowSize - j) * Values[i + WindowSize - j];
                SmoothedValues[i] *= Addition;
            }
            return SmoothedValues;
        }

        public double[] EMA(double[] Values, int WindowSize)
        {
            double[] SmoothedValues = new double[Values.Length - WindowSize];
            double Addition = 2d / (WindowSize + 1);

            for (int i = 0; i < WindowSize; i++)
                SmoothedValues[0] += Values[WindowSize - i];
            SmoothedValues[0] /= WindowSize;

            for (int i = 1; i < SmoothedValues.Length; i++)
                SmoothedValues[i] = (1 - Addition) * SmoothedValues[i - 1] + Addition * Values[i];

            return SmoothedValues;
        }

        void SetTrend()
        {
            SmallestSquaresLinearRegression Reg;

            Data[] TwoDimValues = new Data[Values.Length];
            for (int i = 0; i < Values.Length; i++)
            {
                TwoDimValues[i].XValue = i;
                TwoDimValues[i].YValue = Values[i];
            }
            TwoDimensionalStatistics St = new TwoDimensionalStatistics(TwoDimValues, 0.05);

            Reg = new SmallestSquaresLinearRegression(TwoDimValues, St);

            GetTrend = Reg.Function;
        }
        public void RemoveLinearTrend()
        {
            double[] _Values = new double[Values.Length];
            for (int i = 0; i < Values.Length; i++)
                _Values[i] = Values[i] - GetTrend(i);

            GetTrend = (t) => average;

            Values = _Values;
            average = Values.Average();
            dispersion = GetDispersion();
        }

        void SetStatistics()
        {
            PredictionVector = null;

            average = Values.Average();
            dispersion = GetDispersion();

            SetTrend();
        }

        public int ShowTrendDirection()
        {
            double c = 0;
            for (int i = 0; i < Values.Length - 1; i++)
                c += (Values[i] < Values[i + 1]) ? 1 : 0;

            c = 12 * (c - 0.5 * (Values.Length - 1)) / (Values.Length + 1);

            if (c > 1.96)
                return 1;
            else if (c < -1.96)
                return -1;
            else
                return 0;
        }

        public void Return()
        {
            Values = new double[_Values.Length];
            Array.Copy(_Values, Values, _Values.Length);
            SetStatistics();
        }
        public void DeleteAnomalies()
        {
            double S = 3 * Math.Sqrt(dispersion);
            List<double> _Values = new List<double>();
            for (int i = 0; i < Values.Length; i++)
                if (Values[i] <= average + S && Values[i] >= average - S)
                    _Values.Add(Values[i]);

            Values = _Values.ToArray();

            SetStatistics();
        }

        public double Predict(int WindowSize)
        {
            Tuple<Matrix, Matrix, MultiDimensionalVector> Decomposed = DecomposeBySSA(WindowSize, Values);

            if (PredictionVector == null)
            {
                A = Matrix.RemoveColumn(Matrix.RemoveColumn(Decomposed.Item2, Decomposed.Item2.Width - 1).GetTransponed(), Decomposed.Item2.Width - 1);
                PredictionVector = new MultiDimensionalVector(WindowSize - 1);
                for (int i = 0; i < WindowSize - 1; i++)
                    PredictionVector[i] = A[i, A.Width - 1];
            }
                MultiDimensionalVector B = new MultiDimensionalVector(WindowSize - 1);
            
                for (int i = 0; i < WindowSize - 1; i++)
                    B[i] = Values[Values.Length - WindowSize + i + 1];

                MultiDimensionalVector Ys = SystemOfLinearEquations.SolveByGauss(A, B);

            double sum = 0;
            for (int i = 0; i < WindowSize - 1; i++)
                sum += Ys[i] * PredictionVector[i];
            return sum;
        }

        public static Tuple<Matrix, Matrix, MultiDimensionalVector> DecomposeBySSA(int M, double[] Values)
        {
            double[,] DecomposedValues = new double[M, Values.Length - M];

            for (int i = 0, j; i < Values.Length - M; i++)
                for (j = 0; j < M; j++)
                    DecomposedValues[j, i] = Values[j + i];

            Matrix ValuesMatrix = new Matrix(DecomposedValues);
            ValuesMatrix -= ValuesMatrix.GetAverageInRows();
            //ValuesMatrix -= ValuesMatrix.GetAverageInRows();

            PCA Components = new PCA(ValuesMatrix * ValuesMatrix.GetTransponed() / ValuesMatrix.Width);
            Matrix EigenVectors = Components.EigenVectors;

            return new Tuple<Matrix, Matrix, MultiDimensionalVector>(Components.Translate(ValuesMatrix.GetTransponed(), M), EigenVectors, Components.EigenValues);
        }

        public static double[] Reconstruct(Tuple<Matrix, Matrix, MultiDimensionalVector> DecomposedValues, int[] Components)
        {
            Matrix Values = DecomposedValues.Item1.Copy().GetTransponed();
            Matrix Translation = DecomposedValues.Item2.Copy();
            //bool Remove = true;
            //for (int i = Translation.Width - 1; i >= 0; i--)
            //{
            //    Remove = true;
            //    foreach (int n in Components)
            //        if (n == i)
            //        {
            //            Remove = false;
            //            break;
            //        }
            //    if (Remove)
            //        Translation = Matrix.RemoveColumn(Translation, i);
            //}

            //Values = (Values * Translation).GetTransponed();
            double[,] Temp = new double[Values.Height, Values.Width];
            for (int i = 0, j; i < Values.Height; i++)
                for (j = 0; j < Values.Width; j++)
                    foreach (int k in Components)
                        Temp[i, j] += Translation[i, k] * Values[k, j];

            Values = new Matrix(Temp);
            int N = Values.Width + Values.Height;

            double[] TimeS = new double[N];

            for (int i = 0, j; i < N; i++)
            {
                if (i < Values.Height)
                    for (j = 0; j <= i; j++)
                        TimeS[i] += Values[j, i - j] / (i + 1);
                else if(i < N - Values.Height)
                    for (j = 0; j < Values.Height; j++)
                        TimeS[i] += Values[j, i - j] / (Values.Height);

                if (i > 0 && i < Values.Height)
                    for (j = i; j < Values.Height; j++)
                        TimeS[N - Values.Height + i] = Values[j, i - j + N - Values.Height - 1];
            }
            return TimeS;
        }
    }
}
