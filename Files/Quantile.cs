﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Statistics
{
    public static class Quantile
    {
        public static double Normal(double alpha)
        {
            double U = 0;
            double c0 = 2.515517,
                    c1 = 0.802853,
                    c2 = 0.010328,
                    d1 = 1.432788,
                    d2 = 0.1892659,
                    d3 = 0.001308;
            double t = Math.Sqrt(Math.Log(1 / Math.Pow(alpha, 2)));
            U = t - (c0 + c1 * t + c2 * Math.Pow(t, 2)) / (1 + d1 * t + d2 * Math.Pow(t, 2) + d3 * Math.Pow(t, 3)); 
            return U;
        }
        public static double FromSize(int N)
        {
            double v = N - 1;
            if(v < 10)
            {
                return 0.75;
            }
            else if (v < 20)
            {
                return 0.691;
            }
            else if (v < 30)
            {
                return 0.684;
            }
            else if (v < 60)
            {
                return 0.681;
            }
            else if (v < 120)
            {
                return 0.678;
            }
            else
            {
                return 0.674;
            }
        }

        public static double Hi2(double Alpha, double v)
        {
            //double d;
            //if(Alpha < 0.5)
            //    d = -2.0637 * Math.Pow(Math.Log(1.0 / Alpha) - 0.16, 0.4274) + 1.5774;
            //else
            //    d = 2.0637 * Math.Pow(Math.Log(1.0 / (1 - Alpha)) - 0.16, 0.4274) - 1.5774;

            //double A = d * Math.Sqrt(2);
            //double B = 2 * (Math.Pow(d, 2) - 1) / 3;
            //double C = d * (Math.Pow(d, 2) - 7) / (9 * Math.Sqrt(2));
            //double D = -1 * (6 * Math.Pow(d, 4) + 14 * Math.Pow(d, 2) - 32) / 405;
            //double E = d * (9 * Math.Pow(d, 4) + 256 * Math.Pow(d, 2) - 433) / (4860 * Math.Sqrt(2));

            //double h = n + A * Math.Sqrt(n) + B + C / Math.Sqrt(n) + D / n + E / (n * Math.Sqrt(n));

            //return h;
            return v * Math.Pow(1 - 2d / (9 * v) + Quantile.Normal(Alpha) * Math.Sqrt(2d / (9 * v)), 3);
        }

        public static double Fisher(double Alpha, double v1, double v2)
        {
            double f = 0;
            double Sigma = 1d / v1 + 1d / v2;
            double Delta = 1d / v1 - 1d / v2;

            //double U = Quantile.Normal(Alpha);
            double U = Quantile.Normal((1 - Alpha));

            double z = 0;

            z = U * Math.Sqrt(Sigma / 2) - Delta * (Math.Pow(U, 2) + 2) / 6
                + Math.Sqrt(Sigma / 2) * ((Sigma / 24) * (Math.Pow(U, 2) + 3 * U)) + (Math.Pow(Delta, 2) / (72 * Sigma) * (Math.Pow(U, 3) + 11 * U))
                - ((Delta * Sigma) / 120) * (Math.Pow(U, 4) + 9 * Math.Pow(U, 2) + 8) 
                + (Math.Pow(Delta, 3) / (3240 * Sigma)) * (3 * Math.Pow(U, 4) + 7 * Math.Pow(U, 2) - 16)
                    + Math.Sqrt(Sigma / 2) * ((Math.Pow(Sigma, 2) / 1920) * (Math.Pow(U, 5) + 20 * Math.Pow(U, 3) + 15 * U)
                    + (Math.Pow(Delta, 4) / 2880) * (Math.Pow(U, 5) + 44 * Math.Pow(U, 3) + 183 * U) 
                    + (Math.Pow(Delta, 4) / (155520 * Math.Pow(Sigma, 2)) * (9 * Math.Pow(U, 5) - 284 * Math.Pow(U, 3) - 1513 * U)));

            f = Math.Exp(2 * z);

            return f;
        }

        public static double Student(double Alpha, int v)
        {
            //double u = Normal(Alpha);
            double u = Normal(1 - Alpha);
            return u + g1() / v + g2() / Math.Pow(v, 2) + g3() / Math.Pow(v, 3) + g4() / Math.Pow(v, 4);

            double g1() => (Math.Pow(u, 3) + u) / 4.0;

            double g2() => (5 * Math.Pow(u, 5) + 16 * Math.Pow(u, 3) + 3 * u) / 96.0;

            double g3() => (3 * Math.Pow(u, 7) + 19 * Math.Pow(u, 5) + 17 * Math.Pow(u, 3) - 15 * u) / 384.0;

            double g4() => (79 * Math.Pow(u, 9) + 779 * Math.Pow(u, 7) + 1482 * Math.Pow(u, 5) - 1920 * Math.Pow(u, 3) - 945 * u) / 92160.0;
        }
    }
}
