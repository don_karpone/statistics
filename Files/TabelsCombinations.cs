﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public class BinaryTabelsCombinations
    {
        protected int[,] Table;

        public int N0
        { get { return Table[0, 0] + Table[0, 1]; } }
        public int N1
        { get { return Table[1, 0] + Table[1, 1]; } }
        public int M0
        { get { return Table[0, 0] + Table[1, 0]; } }
        public int M1
        { get { return Table[0, 1] + Table[1, 1]; } }

        public double this[int i, int j]
        {
            get { return this.Table[i, j]; }
        }

        public BinaryTabelsCombinations()
        {
            this.Table = new int[2, 2];
        }
        public BinaryTabelsCombinations(Data[] Array, double MidX, double MidY)
        {
            Table = new int[2, 2];
            for (int i = 0; i < Array.Length; i++)
            {
                if (Array[i].XValue - MidX > 0 && Array[i].YValue - MidY > 0)
                    Table[1, 1]++;
                else if (Array[i].XValue - MidX < 0 && Array[i].YValue - MidY < 0)
                    Table[0, 0]++;
                else if (Array[i].XValue - MidX > 0 && Array[i].YValue - MidY < 0)
                    Table[1, 0]++;
                else if (Array[i].XValue - MidX < 0 && Array[i].YValue - MidY > 0)
                    Table[0, 1]++;
            }
            
        }

        public double Fehner() => (this[0, 0] + this[1, 1] - this[1, 0] - this[0, 1])
                / (this[0, 0] + this[1, 1] + this[1, 0] + this[0, 1]);

        public double Phi() => (this[0, 0] * this[1, 1] - this[1, 0] * this[0, 1]) / (Math.Sqrt(N0 * N1) * Math.Sqrt(M0 * M1));

        public double YollQ() => (this[0, 0] * this[1, 1] - this[0, 1] * this[1, 0]) / (this[0, 0] * this[1, 1] + this[0, 1] * this[1, 0]);

        public double YollY() => (Math.Sqrt(this[0, 0] * this[1, 1]) - Math.Sqrt(this[0, 1] * this[1, 0])) 
            / (Math.Sqrt(this[0, 0] * this[1, 1]) + Math.Sqrt(this[0, 1] * this[1, 0]));

        public bool IsYollQ(double alpha)
        {
            double Q = YollQ();

            double u = Q / S();

            return (Math.Abs(u) <= Quantile.Normal(1 - alpha / 2)) ? true : false;

            double S() => 1d / 4 * (1 - Q * Q) * Math.Sqrt(1d / this[0, 0] + 1d / this[0, 1] + 1d / this[1, 0]+ 1d / this[1, 1]);
        }
        public bool IsYollY(double alpha)
        {
            double Y = YollY();

            double u = Y / S();

            return (Math.Abs(u) <= Quantile.Normal(1 - alpha / 2)) ? true : false;

            double S() => 1d / 2 * (1 - Y * Y) * Math.Sqrt(1d / this[0, 0] + 1d / this[0, 1] + 1d / this[1, 0] + 1d / this[1, 1]);
        }

    }

    public class TablesCombinations : BinaryTabelsCombinations
    {
        public int[] N;
        public int[] M;
        public int Size;
        public double Hi;
        public TablesCombinations(VariationGrid V)
        {
            this.Size = V.Size;
            this.Table = new int[V.XClasses, V.YClasses];
            this.N = new int[V.XClasses];
            this.M = new int[V.YClasses];
            for (int i = 0; i < V.XClasses; i++)
                N[i] = 0;
            for (int i = 0; i < V.YClasses; i++)
                M[i] = 0;
            for (int i = 0, j = 0; i < V.XClasses; i++)
                for (j = 0; j < V.YClasses; j++)
                {
                    Table[i, j] = V.Grid[i, j].Frequency;
                    N[i] += V.Grid[i, j].Frequency;
                    M[j] += V.Grid[i, j].Frequency;
                }
            Hi = Hi2();
        }

        private double Hi2()
        {
            double Hi = 0;
            for (int i = 0, j; i < N.Length; i++)
                for (j = 0; j < M.Length; j++)
                    Hi += Math.Pow(this[i, j] - X(i, j), 2) / X(i, j);
            return Hi;
        }
        private double X(int i, int j) => (double)(N[i] * M[j]) / Size;

        public bool IsConnected(double alpha) => (Hi > Quantile.Hi2(1 - alpha, (N.Length - 1) * (M.Length - 1))) ? true : false;

        public bool IsSquare() => (N.Length == M.Length) ? true : false;

        public double Pirson(double alpha) => (IsConnected(alpha)) ? Math.Sqrt(Hi / (Hi + Size)) : 0;

        public double Stuard() => 2 * (P() - Q()) * Math.Min(M.Length, N.Length) / (Size * Size * Math.Min(M.Length, N.Length));

        public double Kendall()
        {
            return (P() - Q()) / Math.Sqrt((0.5 * Size * (Size - 1) - T(N)) * (0.5 * Size * (Size - 1) - T(M)));

            double T(int[] Values)
            {
                double Sum = 0;
                for (int i = 0; i < Values.Length; i++)
                    Sum += Values[i] * (Values[i] - 1);

                return 0.5 * Sum;
            }
        }

        double P()
        {
            double Sum1 = 0;
            double Sum2 = 0;
            for (int i = 0, j, k, l; i < N.Length; i++)
                for (j = 0; j < M.Length; j++)
                {
                    Sum2 = 0;
                    for (k = i + 1; k < N.Length; k++)
                        for (l = j + 1; l < M.Length; l++)
                            Sum2 += this[k, l];
                    Sum1 += this[i, j] * Sum2;
                }
            return Sum1;
        }
        double Q()
        {
            double Sum1 = 0;
            double Sum2 = 0;
            for (int i = 0, j, k, l; i < N.Length; i++)
                for (j = 0; j < M.Length; j++)
                {
                    Sum2 = 0;
                    for (k = i + 1; k < N.Length; k++)
                        for (l = 0; l < j - 1; l++)
                            Sum2 += this[k, l];
                    Sum1 += this[i, j] * Sum2;
                }
            return Sum1;
        }

    }
}
