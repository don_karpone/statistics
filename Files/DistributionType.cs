﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Statistics
{
    public enum DistributionType {None, Normal, Exponencial, Equaliste, Weibull, Extremal, Arcsinus, TwoDimNormal};

    public enum RegressionType { None, LinearSmallestSquares, LinearTeil, NonLinearPolynom, QuziLinear};
}
