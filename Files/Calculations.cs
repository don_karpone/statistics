﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using Algebra;

namespace Statistics
{
    public static class Calculations
    {
        public static double[] DataToDouble(string[] Numbers)
        {
            double[] num = new double[Numbers.Length];

            for (int i = 0; i < Numbers.Length; i++)
            {
                try
                {
                    num[i] = Convert.ToDouble(Numbers[i]);
                }
                catch
                {
                    continue;
                }
            }
            return num;
        }
        public static double[] DataToDouble(string[] Numbers, int index)
        {
            double[] num = new double[Numbers.Length];

            char[] Sep = { ' ', '\t', '\n' };
            string s;
            for (int i = 0; i < Numbers.Length; i++)
            {
                try
                {
                    s = Numbers[i].Split(Sep, StringSplitOptions.RemoveEmptyEntries)[index];

                    num[i] = Convert.ToDouble(s);
                }
                catch
                {
                    continue;
                }
            }
            return num;
        }

        public static Data[] StringToTwoDimData(string[] Numbers, int X, int Y)
        {
            Data[] Val = new Data[Numbers.Length];

            char[] Sep = { ' ', '\t', '\n', ';' };

            for (int i = 0; i < Numbers.Length; i++)
            {
                try
                {
                    string[] s = Numbers[i].Split(Sep, StringSplitOptions.RemoveEmptyEntries);
                    Val[i].XValue = Convert.ToDouble(s[X].Replace(',', '.'));
                    Val[i].YValue = Convert.ToDouble(s[Y].Replace(',', '.'));
                }
                catch
                {
                    continue;
                    //Console.Error.WriteLine("Error not a number {0}", Numbers[i]);
                }
            }
            return Val;
        }

        public static MultiDimensionalVector[] StringToMultiDimensionalData(string[] Numbers, int[] Indicies)
        {
            List<MultiDimensionalVector> Values = new List<MultiDimensionalVector>();
            MultiDimensionalVector V;
            char[] Sep = { ' ', '\t', '\n', ';' };

            for (int i = 0, j; i < Numbers.Length; i++)
            {
                try
                {
                    string[] s = Numbers[i].Split(Sep, StringSplitOptions.RemoveEmptyEntries);
                    V = new MultiDimensionalVector(Indicies.Length);
                    for(j = 0; j< Indicies.Length; j++)
                        V[j] = Convert.ToDouble(s[Indicies[j]].Replace(',', '.'));
                    Values.Add(V);
                }
                catch
                {
                    continue;
                }
            }
            return Values.ToArray();
        }
    }
}
