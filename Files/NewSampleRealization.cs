﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Algebra;

namespace Statistics
{
    class NewSampleRealization
    {
        public double Alpha;
        public readonly string Name;

        public int Dimensionality
        {
            get => DataValues.Width;
        }
        public int Length
        {
            get => DataValues.Height;
        }

        public Matrix Values
        {
            get => DataValues;
            set
            {
                DataValues = value.Copy();
                SetAllStatistics();
            }
        }
        public double[] GetSample(int Index) => DataValues.GetColumn(Index).ToArray();

        Matrix DataValues;
        Matrix OriginalValues;

        Matrix CorrelationMatrix;
        public Matrix GetCorrelationMatrix() => CorrelationMatrix.Copy();

        Matrix DispersionCovariationMatrix;
        public Matrix GetDispersionCovariationMatrix() => DispersionCovariationMatrix.Copy();

        public Dictionary<string, AdvancedEvaluation> Statistics;
        public Statistics[] Statistics1D;
        public TwoDimensionalStatistics[,] Statistics2D;

        public int RegressionDependentIndex;
        public Dictionary<string, AdvancedEvaluation> StatisticsRegression;

        public List<string> Jurnal;

        MultiDimensionalVector MathExpectation;
        public MultiDimensionalVector GetMathExpectation() => MathExpectation.Copy();

        MultiDimensionalVector Dispersion;
        public MultiDimensionalVector GetDispersion() => Dispersion.Copy();

        MultiDimensionalVector StandartDeviation;
        public MultiDimensionalVector GetStandartDeviation() => StandartDeviation.Copy();

        public MultiDimensionalVector RegressionArguments;

        MultiDimensionalVector Maximums;
        MultiDimensionalVector Minimums;

        Matrix Heatmap;
        public Matrix GetHeatmap() => Heatmap.Copy();

        Matrix ParallelCoordinates;
        public Matrix GetParallelCoordinates() => ParallelCoordinates.Copy();

        //ДОРОБИТИ МЕТОДИ
        Matrix ClassedValues;
        Matrix ClassedDensities;
        Matrix ClassedRelativeDensities;
        Matrix ClassedDistributions;

        public NewSampleRealization(string Name, Matrix NewValues)
        {
            DataValues = NewValues.Copy();
            OriginalValues = DataValues.Copy();
            this.Name = Name;
            Jurnal = new List<string>();

            SetJurnal("Name: " + Name + ";");
            SetJurnal("Dimensionality: " + Dimensionality.ToString() + ";");
            SetJurnal("Size: " + Length.ToString() + ";");

            SetAllStatistics();
        }

        void SetJurnal(string Text) => Jurnal.Add(Text);

        void SetAllStatistics()
        {
            SetJurnal("__CALCULATING STATISTICS__");

            Statistics = new Dictionary<string, AdvancedEvaluation>();
            GetBorders();
            SetMathExpectation();
            SetDispersion();
            SetDCMatrix();
            SetCorrelationMatrix();

            SetClassedValues();

            SetJurnal("__COMPLETE__");
        }

        void GetBorders()
        {
            Maximums = DataValues.GetRow(0);
            Minimums = DataValues.GetRow(0);

            for (int i = 1, j; i < DataValues.Height; i++)
                for (j = 0; j < DataValues.Width; j++)
                {
                    if (DataValues[i, j] > Maximums[j])
                        Maximums[j] = DataValues[i, j];
                    if (DataValues[i, j] < Minimums[j])
                        Minimums[j] = DataValues[i, j];
                }
        }

        void SetMathExpectation()
        {
            MathExpectation = new MultiDimensionalVector(Dimensionality);
            Parallel.For(0, Dimensionality, GetAverage);

            void GetAverage(int Index) => MathExpectation[Index] = DataValues.GetColumn(Index).ToArray().Average();
        }

        void SetDCMatrix()
        {
            if (MathExpectation == null)
                SetMathExpectation();
            Matrix CentredValues = DataValues - MathExpectation;
            DispersionCovariationMatrix = CentredValues.GetTransponed() * CentredValues / (Length - 1);
        }

        void SetDispersion()
        {
            if (DispersionCovariationMatrix == null)
                SetDCMatrix();

            Dispersion = new MultiDimensionalVector(Dimensionality);
            StandartDeviation = new MultiDimensionalVector(Dimensionality);
            for (int i = 0; i < Dimensionality; i++)
            {
                Dispersion[i] = DispersionCovariationMatrix[i, i];
                StandartDeviation[i] = Math.Sqrt(Dispersion[i]);
            }
        }

        void SetCorrelationMatrix()
        {
            if (DispersionCovariationMatrix == null)
                SetDCMatrix();

            double[,] TempMatrix = new double[Dimensionality, Dimensionality];
            for (int i = 0, j; i < Dimensionality; i++)
            {
                TempMatrix[i, i] = 1;
                for (j = i + 1; j < Dimensionality; j++)
                {
                    TempMatrix[i, j] = DispersionCovariationMatrix[i, j] / Math.Sqrt(Dispersion[i] * Dispersion[j]);
                    TempMatrix[j, i] = TempMatrix[i, j];
                }
            }
            CorrelationMatrix = new Matrix(TempMatrix);
        }


        public void SetRegression()
        {
            if (Dimensionality > 1)
            {
                if (Dimensionality == 2)
                {

                }
                else if (Dimensionality == 3)
                {

                }
                else
                {

                }   
            }
        }
        void SetDiagnostic()
        {
            
        }
        public double[][] GetBubble(int X, int Y, int Z)
        {
            if (X < Dimensionality && Y < Dimensionality && Z < Dimensionality)
                return new double[][] { GetSample(X), GetSample(Y), GetSample(Z) };
            else
            {
                SetJurnal("ERROR:");
                SetJurnal("---Bubble-diagram cannot be created with such indecies");
                throw new Exception("Error in building bubble. No such index");
            }
        }
        void SetParralelCoordinates()
        {
            ParallelCoordinates = Values.Copy();
            for (int i = 0; i < Dimensionality; i++)
                ParallelCoordinates.SetColumn(i, 
                    ((ParallelCoordinates.GetColumn(i) - Minimums[i]) / Maximums[i]).ToArray());
        }

        void SetHeatMap()
        {
            Heatmap = Values.Copy();
            for (int i = 0; i < Dimensionality; i++)
                Heatmap.SetColumn(i,
                    ((Heatmap.GetColumn(i) - Minimums[i]) / Maximums[i] * 255).ToArray());
        }

        public void Standartization()
        {
            DataValues -= MathExpectation;
            DataValues /= Dispersion;
            SetAllStatistics();
        }

        public void Move(int Index, double MovingValue)
        {
            for (int i = 0; i < Length; i++)
                DataValues.SetValue(i, Index, DataValues[i, Index] + MovingValue);

            SetAllStatistics();
        }

        public void Log(int Index)
        {
            for (int i = 0; i < Length; i++)
                DataValues.SetValue(i, Index, Math.Log(DataValues[i, Index]));

            SetAllStatistics();
        }

        void SetClassedValues()
        {
            int ClassesCount = (int)((Length > 500) ? Math.Pow(Length, 1d / 3) : Math.Sqrt(Length));
            ClassedValues = new Matrix(Dimensionality, ClassesCount);
            ClassedDensities = new Matrix(Dimensionality, ClassesCount);
            ClassedRelativeDensities = new Matrix(Dimensionality, ClassesCount);
            ClassedDistributions = new Matrix(Dimensionality, ClassesCount);

            double[] Steps = new double[Dimensionality];
            for (int i = 0; i < Dimensionality; i++)
                Steps[i] = (Maximums[i] - Minimums[i]) / ClassesCount;

            for(int i = 0, j; i < Dimensionality; i++)
                for(j = 0; j < ClassesCount; j++)
                    ClassedValues.SetValue(i, j, Minimums[i] + Steps[i] * (j + 1));

            for (int classCount = 0, dim, h; classCount < ClassesCount; classCount++)
            {
                for (dim = 0; dim < Dimensionality; dim++)
                {
                    for (h = 0; h < DataValues.Height; h++)
                    {
                        if (DataValues[h, dim] < ClassedValues[dim, classCount])
                        {
                            ClassedDistributions.SetValue(dim, classCount, ClassedDistributions[dim, classCount] + 1);
                            if (classCount > 0)
                            {
                                if (DataValues[h, dim] > ClassedValues[dim, classCount - 1])
                                    ClassedDensities.SetValue(dim, classCount, ClassedDistributions[dim, classCount] + 1);
                            }
                            else
                                ClassedDensities.SetValue(dim, classCount, ClassedDistributions[dim, classCount] + 1);
                        }
                    }
                }
            }

            ClassedDistributions /= DataValues.Height;
            ClassedRelativeDensities = ClassedDensities / DataValues.Height;
        }
    }
}