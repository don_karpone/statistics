﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Statistics
{
    public class Statistics
    {
        public double Alpha;
        double Value;
        double BottomValue;
        double TopValue;
        public Dictionary<string, Evaluation> Values;

        int N;
        double sum;

        public int Count
        {
            get => Values.Count;
        }
        public Evaluation this[string Key]
        {
            get => Values[Key];
        }
        double Q
        {
            get => Quantile.Student(1 - Alpha / 2, N - 1);
        }
        public Statistics()
        {
            this.Values = new Dictionary<string, Evaluation>();
        }
        public Statistics(double[] num)
            :this()
        {
            Set(num);
        }
        public Statistics(double[] num, double alpha)
            :this()
        {
            this.Alpha = alpha;
            Set(num);
            
        }
        void Set(double[] num)
        {
            N = num.Length;
            MathExpectation(num);
            MED(num);
            MAD(num);
            LoweredAverage(num);
            Dispersion(num);
            DispersionMoved(num);
            AssimetryCoefficient(num);
            Excess(num);
            ContrExcess(num);
            VariationCoefficient(num);
        }
        protected void MathExpectation(double[] num)
        {
            double M = SetMathExpectation(num);
            double S = SetSigma(num, M) / Math.Sqrt(N);
            BottomValue = M - S * Q;
            TopValue = M + S * Q;

            Values.Add("MathExpectation", new AdvancedEvaluation("Середнє", M, BottomValue, TopValue, S));
        }
        public static double SetMathExpectation(double[] InputArray)
        {
            double sum = 0;
            for (int i = 0; i < InputArray.Length; i++)
                sum += InputArray[i];
            return sum / InputArray.Length;
        }
        public static double SetMathExpectation(double[] InputArray1, double[] InputArray2)
        {
            double sum = 0;
            for (int i = 0; i < InputArray1.Length; i++)
                sum += InputArray1[i] * InputArray2[i];
            return sum / InputArray1.Length;
        }
        private void MED(double[] num)
        {
            if (N % 2 == 0)
                Values.Add("MED", new Evaluation("MED",(num[N / 2] + num[N / 2 - 1]) / 2));
            else
                Values.Add("MED", new Evaluation("MED", num[N / 2 - 1]));
        }

        private void LoweredAverage(double[] num)
        {
            sum = num[0];
            double alpha = 0.3;
            int k = (int)(alpha * N);
            sum = 0;
            for (int i = k + 1; i < (N - k); i++)
                sum += num[i - 1];
            Values.Add("UM", new Evaluation("Усічене середнє", (1d / (N - 2 * k)) * sum));
        }

        private void Dispersion(double[] num)
        {
            Values.Add("Dispersion", new Evaluation("Дисперсія", Dispersion(num, Values["MathExpectation"].Data)));

            double S = Math.Sqrt(Values["Dispersion"].Data);
            Value =  S / Math.Sqrt(2 * N);

            BottomValue = S - Value * Q;
            TopValue = S + Value * Q;

            Values.Add("Sigma", new AdvancedEvaluation("Середьноквадр.", S, BottomValue, TopValue, Value));
        }
        public static double Dispersion(double[] InputArray, double MiddleValue)
        {
            double Sum = 0;
            for (int i = 0; i < InputArray.Length; i++)
                Sum += Math.Pow(InputArray[i] - MiddleValue, 2);
            return Sum / (InputArray.Length - 1);
        }
        public static double SetSigma(double[] InputArray, double MiddleValue) => Math.Sqrt(Dispersion(InputArray, MiddleValue));

        private void DispersionMoved(double[] num)
        {
            sum = num[0];
            for (int i = 0; i < N; i++)
                sum += Math.Pow(num[i], 2);
            Values.Add("DispersionZ", new Evaluation("Дисперсія(зс.)", (1d / N * sum - Math.Pow(Values["MathExpectation"].Data, 2))));

            Values.Add("SigmaZ", new Evaluation("Середьноквадр.(зс.)", Math.Sqrt(Values["DispersionZ"].Data)));
        }

        private void MAD(double[] num)
        {
            double[] MeD = new double[N];
            for (int i = 0; i < N; i++)
                MeD[i] = Math.Abs(num[i] - Values["MED"].Data);
            if (N % 2 == 0)
                Values.Add("MAD", new Evaluation("MAD", 1.483 * (MeD[N / 2] + MeD[N / 2 - 1]) / 2));
            else
                Values.Add("MAD", new Evaluation("MAD", 1.483 * MeD[N / 2 - 1]));
        }

        private void AssimetryCoefficient(double[] num)
        {
            sum = 0;
            for (int i = 0; i < N; i++)
                sum += Math.Pow(num[i] - Values["MathExpectation"].Data, 3);
            Values.Add("ASYMz", new Evaluation("К. асиметрії(зс.)", (1d / (N * Math.Pow(Values["SigmaZ"].Data, 3))) * sum));

            double ASYM = Values["ASYMz"].Data * (Math.Sqrt(N * (N - 1)) / (N - 2));
            Value = Math.Sqrt(6 * (N - 2) / ((N + 1) * (N + 3)));
            BottomValue = Math.Round(ASYM - Value * Q, 4);
            TopValue = Math.Round(ASYM + Value * Q, 4);

            Values.Add("ASYM", new AdvancedEvaluation("К. асиметрії",ASYM, BottomValue, TopValue, Value));
        }

        private void Excess(double[] num)
        {
            sum = 0;
            for (int i = 0; i < N; i++)
            {
                sum += Math.Pow((num[i] - Values["MathExpectation"].Data), 4);
            }
            Values.Add("ECz",new Evaluation("К. ексцессу(зс.)", sum / (N * Math.Pow(Values["DispersionZ"].Data, 2))));

            double EC = (N * N - 1) * ((Values["ECz"].Data - 3) + 6d / (N + 1)) / ((N - 2) * (N - 3));
            Value = Math.Sqrt((24d / N) * (1 - 225d / (15 * N + 124)));
            BottomValue = EC - Value * Q;
            TopValue = EC + Value * Q;


            Values.Add("EC", new AdvancedEvaluation("К. ексцессу", EC, BottomValue, TopValue, Value));
        }

        private void ContrExcess(double[] num)
        {
            if (Math.Sqrt(Math.Abs(Values["MathExpectation"].Data)) != 0)
            {
                double CEC = 1d / Math.Sqrt(Math.Abs(Values["EC"].Data));

                Value = Math.Sqrt(Math.Abs(Values["ECz"].Data) / (29 * N)) *
                    Math.Pow(Math.Pow(Math.Abs(Math.Pow(Values["ECz"].Data, 2) - 1), 3), 1d / 4);
                BottomValue = CEC - Value * Q;
                TopValue = CEC + Value * Q;

                Values.Add("CEC", new AdvancedEvaluation("К. контрексцессу", CEC, BottomValue, TopValue, Value));
            }
        }

        private void VariationCoefficient(double[] num)
        {
            double VC = Values["Sigma"].Data / Values["MathExpectation"].Data;
            Value = VC * Math.Sqrt((1 + 2 * VC * VC) / (2 * N));
            BottomValue = VC - Value * Q;
            TopValue = VC + Value * Q;

            Values.Add("VC", new AdvancedEvaluation("К. варіації", VC, BottomValue, TopValue, Value));
        }
    }
}
