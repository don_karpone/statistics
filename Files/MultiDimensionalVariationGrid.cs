﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;

namespace Statistics
{
    class MultiDimensionalVariationGrid
    {
        public int Dimensionality
        {
            get => H.Length;
        }
        public double[] Minimums;
        public double[] Maximums;
        public double[][] ClassBorders;
        public double[] H;
        public int[] ClassesCount;
        public GridCell[,] Cells;

        public MultiDimensionalVariationGrid(MultiDimensionalVector[] InputArray)
        {
            SetDimensionality(InputArray[0].Dimensionality);
            FindMinimumsAndMaximums(InputArray);
            SetClasses(InputArray.Length);
            SetBorders(InputArray);
            SetGrid(InputArray);
        }
        void SetDimensionality(int Dimensionality)
        {
            Minimums = new double[Dimensionality];
            Maximums = new double[Dimensionality];
            H = new double[Dimensionality];
            ClassesCount = new int[Dimensionality];
        }
        void FindMinimumsAndMaximums(MultiDimensionalVector[] InputArray)
        {
            for (int i = 0; i < Dimensionality; i++)
            {
                Minimums[i] = Int32.MaxValue;
                Maximums[i] = Int32.MinValue;
            }
            for (int i = 0, j; i < InputArray.Length; i++)
                for (j = 0; j < Dimensionality; j++)
                {
                    if (Minimums[j] > InputArray[i][j])
                        Minimums[j] = InputArray[i][j];
                    if (Maximums[j] < InputArray[i][j])
                        Maximums[j] = InputArray[i][j];
                }
        }
        void SetClasses(int Size)
        {
            int M = VariationGrid.FindClasses(Size);
            for (int i = 0; i < Dimensionality; i++)
            {
                ClassesCount[i] = M;
                H[i] = (Maximums[i] - Minimums[i]) / ClassesCount[i];
            }
        }
        void SetBorders(MultiDimensionalVector[] InputArray)
        {
            ClassBorders = new double[Dimensionality][];
            for (int i = 0, j; i < Dimensionality; i++)
            {
                ClassBorders[i] = new double[ClassesCount[i]];
                for (j = 0; j < ClassesCount[i]; j++)
                {
                    ClassBorders[i][j] = Minimums[i] + i * H[i];
                    Cells[i, j] = new GridCell();
                }
            }
        }
        void SetGrid(MultiDimensionalVector[] InputArray)
        {
            for(int i = 0, j, k; i < InputArray.Length; i++)
                for (j = 0; j < Dimensionality; j++)
                    for (k = 1; k < ClassesCount[j]; k++)
                        if (InputArray[i][j] <= ClassBorders[j][k] && InputArray[i][j] > ClassBorders[j][k - 1])
                        {
                            Cells[j, k].Frequency++;
                            Cells[j, k].RelativeFrequency += 1d / InputArray.Length;
                        }
        }
    }
}