﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;


namespace Statistics
{
    public class SquareHistogramPart : Series
    {
        public SquareHistogramPart()
        {
            this.ChartArea = "MainArea";
            this.ChartType = SeriesChartType.RangeBar;
            this.Legend = "Legend1";
            this.Name = "SquareHistogram";
            this["PointWidth"] = "1";
            this.BorderColor = System.Drawing.Color.Black;
            this.BorderWidth = 1;
        }
    }
    public class EmptySquare : SquareHistogramPart
    {
        public EmptySquare()
        {
            this.Name = "Empty";
            this.Color = Color.FromArgb(255, 180, 170);
        }
    }
    public class LowSquare : SquareHistogramPart
    {
        public LowSquare()
        {
            this.Name = "Low";
            this.Color = Color.FromArgb(255, 105, 90);
        }
    }
    public class MediumSquare : SquareHistogramPart
    {
        public MediumSquare()
        {
            this.Name = "Medium";
            this.Color = Color.FromArgb(255, 50, 30);
        }
    }
    public class BigSquare : SquareHistogramPart
    {
        public BigSquare()
        {
            this.Name = "Big";
            this.Color = Color.FromArgb(220, 20, 0);
        }
    }
    public class ExtraBigSquare : SquareHistogramPart
    {
        public ExtraBigSquare()
        {
            this.Name = "ExtraBig";
            this.Color = Color.FromArgb(180, 15, 0);
        }
    }
    public class RegressionLine : Series
    {
        public RegressionLine()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            this.Legend = "Legend1";
            this.LegendText = "Лінія регресії";
            this.Name = "RegressionLine";
            this.Color = System.Drawing.Color.White;
            this.BorderWidth = 2;
        }
    }
    public class RegressionLineTrustedLine : Series
    {
        public RegressionLineTrustedLine()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            this.Legend = "Legend1";
            this.LegendText = "Довірчі інтервали ЛР";
            this.Name = "RegressionTrustedLine";
            this.Color = System.Drawing.Color.White;
            this.BorderWidth = 2;
        }
    }

    public class RegressionTrustedLine : Series
    {
        public RegressionTrustedLine()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            this.Legend = "Legend1";
            this.LegendText = "Довірчі інтервали НС";
            this.Name = "RegressionTrustedLine";
            this.Color = System.Drawing.Color.LightBlue;
            this.BorderWidth = 2;
        }
    }
    public class RegressionTolerantLine : Series
    {
        public RegressionTolerantLine()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            this.Legend = "Legend1";
            this.LegendText = "Толерантні межі";
            this.Name = "RegressionTolerantLine";
            this.Color = System.Drawing.Color.White;
            this.BorderWidth = 2;
        }
    }
    public class CorrelationSeries : Series
    {
        public CorrelationSeries()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            this.Legend = "Legend1";
            this.LegendText = "Коррелляційне поле";
            this.Name = "CorrelationField";
            this.BorderColor = System.Drawing.Color.Black;
            this.BorderWidth = 1;
            this.Color = Color.LightBlue;
        }
    }
    public class MainHistogramSeries : Series
    {
        public MainHistogramSeries()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            Color = Color.Red;
            this.Legend = "Legend1";
            this.LegendText = "Гістограма";
            this.Name = "Histogram";
            this["PointWidth"] = "1";
            this.BorderColor = System.Drawing.Color.Black;
            this.BorderWidth = 1;
        }
    }
    public class ClassedDistributionSeries : Series
    {
        public ClassedDistributionSeries()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            this.Legend = "Legend1";
            this.LegendText = "Розбита на класи";
            this.Name = "EmpiricalFunction";
            this["PointWidth"] = "1";
            this.Color = Color.Transparent;
            this.BackImage = Environment.CurrentDirectory + @"\line.png";
            this.BackImageWrapMode = System.Windows.Forms.DataVisualization.Charting.ChartImageWrapMode.Unscaled;
            this.BorderWidth = 1;
        }
    }
    public class DisclassedDistributionSeries : Series
    {
        public DisclassedDistributionSeries()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            Color = Color.LightBlue;
            BorderColor = Color.Black;
            this.Legend = "Legend1";
            this.LegendText = "Не розбита на класи";
            this.Name = "PointDistribution";
            //this["PointWidth"] = "1";
        }
    }
    public class ModelHistogramSeries : Series
    {
        public ModelHistogramSeries()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            this.Color = Color.White;
            this.BorderWidth = 2;
            this.Legend = "Legend1";
            this.LegendText = "Функція щільності";
            this.Name = "Density";
        }
    }
    public class ModelDistributionSeries : Series 
    {
        public ModelDistributionSeries()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            this.Color = Color.Red;
            this.BorderDashStyle = ChartDashStyle.Dash;
            this.BorderWidth = 2;
            this.Legend = "Legend1";
            this.LegendText = "Функція розподілу";
            this.Name = "DistributionFunction";
        }
    }
    public class TopDistributionSeries : Series
    {
        public TopDistributionSeries()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            this.Color = Color.White;
            this.BorderDashStyle = ChartDashStyle.Dash;
            this.BorderWidth = 1;
            this.Legend = "Legend1";
            this.LegendText = "Довірчі інтервали";
            this.Name = "TopTrustedLine";
        }
    }

    public class BottomDistributionSeries : Series
    {
        public BottomDistributionSeries()
        {
            this.ChartArea = "MainArea";
            this.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.Color = Color.White;
            this.BorderDashStyle = ChartDashStyle.Dash;
            this.BorderWidth = 1;
            this.Legend = "Legend1";
            this.IsVisibleInLegend = false;
            this.Name = "BottomTrustedLine";
        }
    }
}
