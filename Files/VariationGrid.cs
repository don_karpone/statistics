﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Statistics
{
    public struct Data
    {
        public double XValue;
        public double YValue;
        public Data(double X, double Y)
        {
            this.XValue = X;
            this.YValue = Y;
        }

        public Data(string X, string Y)
        {
            this.XValue = Convert.ToDouble(X);
            this.YValue = Convert.ToDouble(Y);
        }
    }

    public enum Massiveness {Empty, ExtraLow, Low, Medium, Big, ExtraBig }

    public class GridCell
    {
        public int Frequency;
        public double RelativeFrequency
        {
            set {
                RelFrequency = value;
                this.VisiblePart = value.ToString();
            }
            get => RelFrequency; 
        }
        private double RelFrequency;

        public Massiveness Massivenes
        {
            set
            {
                Mass = value;
            }
            get => Mass;
        }
        private Massiveness Mass;

        public List<Data> Points;

        public string VisiblePart;

        public GridCell()
        {
            this.Frequency = 0;
            this.RelativeFrequency = 0.0;
            this.Massivenes = Massiveness.Empty;
            this.Points = new List<Data>();
        }

        public GridCell(int Frequency, double RelativeFrequency)
            :this()
        {
            this.Frequency = Frequency;
            this.RelativeFrequency = RelativeFrequency;
        }

        public GridCell(int Frequency, double RelativeFrequency, Massiveness Mas, List<Data> Points)
            :this(Frequency, RelativeFrequency)
        {
            this.Massivenes = Mas;
            this.Points = Points;
        }

        public void SetMassiveness(double Var)
        {
            if(this.RelativeFrequency == 0)
                this.Massivenes = Massiveness.Empty;
            else if (this.RelativeFrequency <= Var * 0.5)
                this.Massivenes = Massiveness.ExtraLow;
            else if (this.RelativeFrequency <= Var * 1.5)
                this.Massivenes = Massiveness.Low;
            else if (this.RelativeFrequency <= Var * 2.5)
                this.Massivenes = Massiveness.Medium;
            else if (this.RelativeFrequency <= Var * 3.5)
                this.Massivenes = Massiveness.Big;
            else this.Massivenes = Massiveness.ExtraBig;
        }
    }
    public class VariationGrid
    {
        public GridCell[,] Grid;
        public double[] XClassBorders;
        public double[] YClassBorders;

        public int XClasses;
        public int YClasses;
        public int Size;

        public double hX;
        public double hY;

        public double MinX;
        public double MinY;
        public double MaxX;
        public double MaxY;

        public double MaxFrequency;
        private double ColorVariable;

        public VariationGrid(Data[] Array, double Phi)
        {
            Data[] DataArray = GetArray(Array, Phi);

            this.Size = Array.Length;
            this.XClasses = FindClasses(Size);
            this.YClasses = FindClasses(Size);

            SetGrid();

            FindMinMax(DataArray);

            this.hX = FindH(MinX, MaxX, XClasses);
            this.hY = FindH(MinY, MaxY, YClasses);

            SetBorders();

            CreateGrid(DataArray);
        }
        public VariationGrid(double[] X, double[] Y, double Phi)
        {
            Data[] DataArray = GetArray(X, Y, Phi);

            this.Size = X.Length;
            this.XClasses = FindClasses(Size);
            this.YClasses = FindClasses(Size);

            SetGrid();

            FindMinMax(DataArray);

            this.hX = FindH(MinX, MaxX, XClasses);
            this.hY = FindH(MinY, MaxY, YClasses);

            SetBorders();

            CreateGrid(DataArray);
        }

        public VariationGrid(Data[] Array)
        {
            this.Size = Array.Length;
            this.XClasses = FindClasses(Size);
            this.YClasses = FindClasses(Size);

            SetGrid();

            FindMinMax(Array);

            this.hX = FindH(MinX, MaxX, XClasses);
            this.hY = FindH(MinY, MaxY, YClasses);

            SetBorders();

            CreateGrid(Array);
        }

        public VariationGrid(Data[] Array, int X, int Y, double Phi)
        {
            Data[] DataArray = GetArray(Array, Phi);
            this.Size = Array.Length;
            this.XClasses = X;
            this.YClasses = Y;

            SetGrid();

            FindMinMax(DataArray);

            this.hX = FindH(MinX, MaxX, XClasses);
            this.hY = FindH(MinY, MaxY, YClasses);

            SetBorders();

            CreateGrid(DataArray);
        }
        public VariationGrid(Data[] Array, int X, int Y)
        {
            this.Size = Array.Length;
            this.XClasses = X;
            this.YClasses = Y;

            SetGrid();

            FindMinMax(Array);

            this.hX = FindH(MinX, MaxX, XClasses);
            this.hY = FindH(MinY, MaxY, YClasses);

            SetBorders();

            CreateGrid(Array);
        }

        Data[] GetArray(Data[] Array, double Phi)
        {
            double cos = Math.Cos(Phi);
            double sin = Math.Sin(Phi);

            Data[] DataArray = new Data[Array.Length];
            for (int i = 0; i < Array.Length; i++)
            {
                DataArray[i] = new Data();
                DataArray[i].XValue = Array[i].XValue * cos + Array[i].YValue * sin;
                DataArray[i].YValue = -Array[i].XValue * sin + Array[i].YValue * cos;
            }
            return DataArray;
        }
        Data[] GetArray(double[] X, double[] Y, double Phi)
        {
            double cos = Math.Cos(Phi);
            double sin = Math.Sin(Phi);

            Data[] DataArray = new Data[X.Length];
            for (int i = 0; i < X.Length; i++)
            {
                DataArray[i] = new Data();
                DataArray[i].XValue = X[i] * cos + Y[i] * sin;
                DataArray[i].YValue = -X[i] * sin + Y[i] * cos;
            }
            return DataArray;
        }
        private void SetBorders()
        {
            XClassBorders = new double[XClasses];
            YClassBorders = new double[YClasses];
            for (int i = 0; i < this.XClasses; i++)
                this.XClassBorders[i] = this.MinX + (i + 1) * hX;
            for (int i = 0; i < this.YClasses; i++)
                this.YClassBorders[i] = this.MinY + (i + 1) * hY;
        }

        private void SetGrid()
        {
            this.Grid = new GridCell[XClasses, YClasses];
            for (int i = 0; i < this.XClasses; i++)
                for (int j = 0; j < this.YClasses; j++)
                    this.Grid[i, j] = new GridCell();
        }

        private void CreateGrid(Data[] Array)
        {
            List<Data> List = new List<Data>();

            for (int k = 0; k < Array.Length; k++)
            {
                for (int i = 0; i < this.XClasses; i++)
                {
                    for (int j = 0; j < this.YClasses; j++)
                    {
                        if (i != 0 && j != 0)
                        {
                            if (Array[k].XValue > XClassBorders[i - 1] && Array[k].XValue <= XClassBorders[i])
                                if (Array[k].YValue > YClassBorders[j - 1] && Array[k].YValue <= YClassBorders[j])
                                {
                                    this.Grid[i, j].Points.Add(Array[k]);
                                    this.Grid[i, j].Frequency++;
                                    this.Grid[i, j].RelativeFrequency
                                        = Math.Round((double)this.Grid[i, j].Frequency / Array.Length, 4);
                                }
                        }
                        else if (i == 0 && j != 0)
                        {
                            if (Array[k].XValue < XClassBorders[i])
                                if (Array[k].YValue > YClassBorders[j - 1] && Array[k].YValue <= YClassBorders[j])
                                {
                                    this.Grid[i, j].Points.Add(Array[k]);
                                    this.Grid[i, j].Frequency++;
                                    this.Grid[i, j].RelativeFrequency
                                        = Math.Round((double)this.Grid[i, j].Frequency / Array.Length, 4);
                                }
                        }
                        else if (i != 0 && j == 0)
                        {
                            if (Array[k].XValue > XClassBorders[i - 1] && Array[k].XValue <= XClassBorders[i])
                                if (Array[k].YValue < YClassBorders[j])
                                {
                                    this.Grid[i, j].Points.Add(Array[k]);
                                    this.Grid[i, j].Frequency++;
                                    this.Grid[i, j].RelativeFrequency
                                        = Math.Round((double)this.Grid[i, j].Frequency / Array.Length, 4);
                                }
                        }
                        else if (i == 0 && j == 0)
                        {
                            if (Array[k].XValue <= XClassBorders[i])
                                if (Array[k].YValue <= YClassBorders[j])
                                {
                                    this.Grid[i, j].Points.Add(Array[k]);
                                    this.Grid[i, j].Frequency++;
                                    this.Grid[i, j].RelativeFrequency
                                        = Math.Round((double)this.Grid[i, j].Frequency / Array.Length, 4);
                                }
                        }
                    }
                }
            }
            FindMaxFrequency();

            SetMasses();
        }

        //ПОДСЧЕТ КОЛИЧЕСТВА КЛАССОВ=========================================
        public static int FindClasses(int N)
        {
            int M = 0;
            if (N <= 100)
            {
                double S = Math.Round(Math.Sqrt(N), 0);
                if (S % 2 == 0)
                    M = (int)(S - 1);
                else
                    M = (int)(S);
            }
            else if (N > 100)
            {
                double S = Math.Round(Math.Pow(N, 1.0 / 3), 0);
                if (S % 2 == 0)
                    M = (int)(S - 1);
                else
                    M = (int)(S);

            }
            return M;
        }

        public static double FindH(double Min, double Max, int M) => Math.Round((Max - Min) / M, 4);

        private void FindMaxFrequency()
        {
            this.MaxFrequency = this.Grid[0, 0].RelativeFrequency;
            for (int i = 0; i < this.XClasses; i++)
            {
                for (int j = 0; j < this.YClasses; j++)
                {
                    if (this.MaxFrequency < this.Grid[i, j].RelativeFrequency)
                        this.MaxFrequency = this.Grid[i, j].RelativeFrequency;
                }
            }
        }

        private void FindMinMax(Data[] Array)
        {
            this.MinX = Array[0].XValue;
            this.MaxX = Array[0].XValue;
            this.MinY = Array[0].YValue;
            this.MaxY = Array[0].YValue;

            for (int i = 1; i < Array.Length; i++)
            {
                if (this.MinX > Array[i].XValue)
                    this.MinX = Array[i].XValue;
                if (this.MaxX < Array[i].XValue)
                    this.MaxX = Array[i].XValue;

                if (this.MinY > Array[i].YValue)
                    this.MinY = Array[i].YValue;
                if (this.MaxY < Array[i].YValue)
                    this.MaxY = Array[i].YValue;
            }
        }

        private void SetMasses()
        {
            this.ColorVariable = this.MaxFrequency / 5;
            for (int i = 0, j; i < XClasses; i++)
                for (j = 0; j < YClasses; j++)
                    Grid[i, j].SetMassiveness(this.ColorVariable);
        }
    }
}
