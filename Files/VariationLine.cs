﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Statistics
{ 
    public class VariationLine
    {
        public List<VariationClass> VariationList;
        public int ClassNumbers;
        public int Size;
        public double h;
        public VariationLine(double[] values)
        {
            Array.Sort(values);
            this.Size = values.Length;
            this.ClassNumbers = CountClasses(Size);
            this.h = Math.Round((values.Max() - values.Min()) / this.ClassNumbers, 10);
            this.VariationList = CreateList(values);
        }
        public VariationLine(double[] values, int M)
        {
            Array.Sort(values);
            this.Size = values.Length;
            this.ClassNumbers = M;
            this.h = (values.Max() - values.Min()) / this.ClassNumbers;
            this.VariationList = CreateList(values);
        }

        private static int CountClasses(int N)
        {
            int M = 0;
            if (N <= 100)
            {
                double S = Math.Round(Math.Sqrt(N), 0);
                if (S % 2 == 0)
                    M = (int)(S + 1);
                else
                    M = (int)(S);
            }
            else if (N > 100)
            {
                double S = Math.Round(Math.Pow(N, 1d / 3), 0);
                if (S % 2 == 0)
                    M = (int)(S + 1);
                else
                    M = (int)(S);
            }
            return M;
        }

        private List<VariationClass> CreateList(double[] values)
        {
            int N = Size;
            List<VariationClass> L = new List<VariationClass>();
            double Prob = 0;
            for (int i = 0, j = 0; i < this.ClassNumbers; i++)
            {
                bool IsEmpty = true;
                List<VariationColumn> VarColumnList = new List<VariationColumn>();
                double hBot = Math.Round(values.Min() + i * this.h, 10);
                double Val = Math.Round(hBot + 0.5 * h, 4);
                double hTop = Math.Round(hBot + this.h, 10);
                if (i == (this.ClassNumbers - 1))
                    hTop = values.Max() + 0.0000001;
                int Fr = 0;
                double RelFr = 0;
                for (; j < N; j++)
                {
                    if (values[j] < hTop)
                    {
                        Fr++;
                        if (j < (N - 1))
                        {
                            int k = j;
                            while ( k + 1 < N && values[j] == values[k + 1])
                            {
                                Fr++;
                                k++;
                            }
                            RelFr = Math.Round((double)Fr / N, 8);
                            Prob += RelFr;
                            Prob = Math.Round(Prob, 8);
                            if(j != k)
                            {
                                while (j <= k)
                                {
                                    VarColumnList.Add(new VariationColumn(Math.Round(values[j], 8), Fr, RelFr, Prob));
                                    j++;
                                }
                                j--;
                            }
                            else
                                VarColumnList.Add(new VariationColumn(Math.Round(values[j], 8), Fr, RelFr, Prob));
                        }
                        else
                        {
                            RelFr = Math.Round((double)Fr / N, 8);
                            Prob += RelFr;
                            Prob = Math.Round(Prob, 8);
                            VarColumnList.Add(new VariationColumn(Math.Round(values[j], 8), Fr, RelFr, Prob));
                        }
                        Fr = 0;
                        IsEmpty = false;
                    }
                    else
                    {
                        break;
                    }
                }
                if(!IsEmpty)
                    L.Add(new VariationClass(VarColumnList, Math.Round(Val, 8), Math.Round((double)VarColumnList.Count / values.Length, 4)));
                else
                    L.Add(new VariationClass(Math.Round(Val, 8)));
            }
            return L;
        }
    }

    public struct VariationClass
    {
        public double ClassValue;
        public int Frequency;
        public double RelativeFrequency;
        public double DistributionProbability;
        public double Max;
        public double Min;
        public List<VariationColumn> ValuesInClass;
        public VariationClass(List<VariationColumn> L)
        {
            this.ValuesInClass = L;
            this.ClassValue = this.ValuesInClass[this.ValuesInClass.Count - 1].Value;
            this.Max = this.ValuesInClass[this.ValuesInClass.Count - 1].Value;
            this.Min = this.ValuesInClass[0].Value;
            this.Frequency = L.Count;
            this.DistributionProbability = this.ValuesInClass[ValuesInClass.Count - 1].DistributionProbability;
            RelativeFrequency = 0.0;
            for (int i = 0; i < L.Count; i++)
                this.RelativeFrequency += this.ValuesInClass[i].RelativeFrequency;
        }
        public VariationClass(List<VariationColumn> L, double var)
        {
            this.ValuesInClass = L;
            this.ClassValue = var;
            this.Max = this.ValuesInClass[this.ValuesInClass.Count - 1].Value;
            this.Min = this.ValuesInClass[0].Value;
            this.Frequency = L.Count;
            this.DistributionProbability = this.ValuesInClass[ValuesInClass.Count - 1].DistributionProbability;
            this.RelativeFrequency = 0.0;
            for (int i = 0; i < L.Count; i++)
                this.RelativeFrequency += this.ValuesInClass[i].RelativeFrequency;
            this.RelativeFrequency = Math.Round(this.RelativeFrequency, 4);
        }
        public VariationClass(double Val)
        {
            this.ValuesInClass = new List<VariationColumn>();
            this.ClassValue = Val;
            this.Max = 0;
            this.Min = 0;
            this.Frequency = 0;
            this.DistributionProbability = 0;
            RelativeFrequency = 0.0;
        }

        public VariationClass(List<VariationColumn> L, double var, double RelativeF)
        {
            this.ValuesInClass = L;
            this.ClassValue = var;
            this.Max = this.ValuesInClass[this.ValuesInClass.Count - 1].Value;
            this.Min = this.ValuesInClass[0].Value;
            this.Frequency = L.Count;
            this.DistributionProbability = this.ValuesInClass[ValuesInClass.Count - 1].DistributionProbability;
            this.RelativeFrequency = RelativeF;
        }
    }
    public struct VariationColumn
    {
        public double Value;
        public int Frequency;
        public double RelativeFrequency;
        public double DistributionProbability;
        public VariationColumn(double Class, int Frequency, 
            double RelativeFrequency, double DistributionProbability)
        {
            Value = Class;
            this.Frequency = Frequency;
            this.RelativeFrequency = RelativeFrequency;
            this.DistributionProbability = DistributionProbability;
        }
    }
}
