﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    static class Criterion
    {
        static public class Colmogorov
        {
            static public double Z(double[] Empiric, double[] Theoretic)
            {
                double N = Empiric.Length;
                double DPlus = Math.Abs(Empiric[0] - Theoretic[0]);
                double DMinus = Math.Abs(Empiric[0]);
                for (int i = 1; i < N; i++)
                {
                    if (DPlus < Math.Abs(Empiric[i] - Theoretic[i]))
                        DPlus = Math.Abs(Empiric[i] - Theoretic[i]);
                    if (DMinus < Math.Abs(Empiric[i] - Theoretic[i - 1]))
                        DMinus = Math.Abs(Empiric[i] - Theoretic[i - 1]);
                }
                double z;
                if (DPlus > DMinus)
                    z = Math.Sqrt(N) * DPlus;
                else
                    z = Math.Sqrt(N) * DMinus;

                return z;
            }

            static public double K(double Z, int N)
            {
                double Value = 0;
                double Sum = 0;
                for (int k = 1; k <= 1000000; k++)
                {
                    double f1 = Math.Pow(k, 2) - (1 - Math.Pow(-1, k)) / 2;
                    double f2 = 5 * Math.Pow(k, 2) + 22 - 7.5 * (1 - Math.Pow(-1, k));

                    Sum += Math.Pow(-1, k) * Math.Exp(-2 * Math.Pow(k, 2) * Math.Pow(Z, 2))
                        * (1 - 2 * (Math.Pow(k, 2) * Z) / (3 * Math.Sqrt(N))
                        - ((f1 - 4 * (f1 + 3)) * Math.Pow(k, 2) * Math.Pow(Z, 2)
                        + 8 * Math.Pow(k, 4) * Math.Pow(Z, 4)) / (18 * N)
                        + ((Math.Pow(k, 2) * Z) / (27 * Math.Sqrt(Math.Pow(N, 3))))
                        * ((Math.Pow(f2, 2) / 5)
                        - (4 * (f2 + 45) * Math.Pow(k, 2) * Math.Pow(Z, 2) / 15)
                        + 8 * Math.Pow(k, 4) * Math.Pow(Z, 4)));
                }
                Value = 1 + 2 * Sum;
                return Value;
            }
        }

        static public class Pirson
        {
            static public double Hi(double[] n, double[] n0)
            {
                double hi = 0;

                for (int i = 0; i < n.Length; i++)
                    hi += Math.Pow(n[i] - n0[i], 2) / n0[i];

                return hi;
            }
        }


        static public class Bartlett
        {
            //public static bool Solve()
            //{

            //}
            static public double B(double[] Si, int[] N, double S, int k)
            {
                double B = 0;

                double sum = 0;
                for (int i = 0; i < k; i++)
                {
                    sum += (N[i] - 1) * Math.Log(Si[i] / S);
                }
                B = -sum;
                return B;
            }
            static public double C(int[] N, int k)
            {
                double C = 0;

                double sum1 = 0;
                double sum2 = 0;
                for (int i = 0; i < k; i++)
                {
                    sum1 += 1d / (N[i] - 1);
                    sum2 += N[i] - 1;
                }
                C = 1 + (1d / (3 * (k - 1))) * (sum1 - 1d / sum2);
                return C;
            }
        }

        static public class Wilcoxon
        {
            static public double E(int N1, int N)
            {
                return (N1 * (N + 1)) / 2;
            }
            static public double D(int N1, int N2, int N)
            {
                return N1 * N2 * (N - 1) / 12;
            }
            static public double w(double W, double E, double D)
            {
                return (W - E) / Math.Sqrt(D);
            }
        }

        static public class Witney
        {
            static public double E(int N1, int N2)
            {
                return (N1 * N2) / 2;
            }
            static public double D(int N1, int N2, int N)
            {
                return N1 * N2 * (N + 1) / 12;
            }
            static public double u(double U, double E, double D)
            {
                return (U - E) / Math.Sqrt(D);
            }
        }

        static public class Symbol
        {
            static public double Alpha(int N, int S)
            {
                double Nf = Factorial(N);
                for (int i = N; i > 1; i--)
                    Nf *= (N - 1);
                double Sum = 0;
                for (int l = 0; l < N - S; l++)
                {
                    double lF = Factorial(l);
                    double NmL = Factorial(N - l);
                    Sum += lF / (Nf * NmL);
                }
                Sum *= Math.Pow(2, -N);
                return Sum;
            }
            static private double Factorial(int Value)
            {
                if (Value == 0)
                    return 1;
                for (int i = Value - 1; i > 0; i--)
                    Value *= i;
                return Value;
            }
        }

        static public class Kruskal
        {
            static public double H(double[] W, int[] Ni, int N)
            {
                double H = 0;
                for (int i = 0; i < W.Length; i++)
                {
                    H += (1 - (double)Ni[i] / N) * Math.Pow(W[i] - (N + 1) / 2, 2) / ((N + 1) * (double)(N - Ni[i]) / (12 * N));
                }
                return H;
            }
        }

        static public class MinusMed
        {
            static public double v(double rx, double ry, int N1, int N2)
            {
                int N = N1 + N2;
                double v = 0;

                v = (rx - ry) / (N * Math.Sqrt((N + 1) / (12 * N1 * N2)));

                return v;
            }
        }
    }
}
