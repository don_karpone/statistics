﻿using System;
using System.Collections.Generic;
using System.Linq;
using Algebra;

namespace Statistics
{
    public abstract class LinearRegression
    {
        public string Name;
        public string Label;
        public Dictionary<string, Evaluation> Values;
        public Func<double, double> Function;
        public static Func<double, double, double, double> Param = (x, A, B) => A + B * x;
        protected AdvancedEvaluation A;
        protected AdvancedEvaluation B;
        protected Evaluation ResidualDispersion;
        protected Evaluation SigmaA;
        protected Evaluation SigmaB;
        protected Evaluation DeterminationCoefficient;
        protected double[] HelpArray;
        protected double Quantilia;
        protected double xMiddle;

        public Evaluation this[string Key]
        {
            get => Values[Key];
        }
        public LinearRegression()
        {
            this.Name = "Лінійна";
            this.Label = "a + bx";
            this.Function = (x) => A.Data + B.Data * x;
            Values = new Dictionary<string, Evaluation>();
        }
        protected AdvancedEvaluation SetBorders(AdvancedEvaluation Arg, double Quantilia, double Sigma)
        {
            Arg.BottomBorder = Arg.Data - Quantilia * Sigma;
            Arg.TopBorder = Arg.Data + Quantilia * Sigma;
            return Arg;
        }
        public virtual double SigmaTolerant() => Math.Sqrt(ResidualDispersion.Data);
        public virtual double SigmaDivergence (double x, int Size) => Math.Sqrt(ResidualDispersion.Data* (1d / Size) + Math.Pow(SigmaB.Data, 2) * Math.Pow(x - xMiddle, 2));
        public virtual double SigmaNewObservation (double x, int Size) => Math.Sqrt(ResidualDispersion.Data* (1d / Size + 1) + Math.Pow(SigmaB.Data, 2) * Math.Pow(x - xMiddle, 2));
    }
    public class CubeLinearRegression : MultiLinearRegression
    {
        double YAverage;
        double YX1Average;
        double YX2Average;
        double X1Average;
        double X2Average;
        double X1SquareAverage;
        double X2SquareAverage;
        double X1X2Average;
        public CubeLinearRegression()
        {
            this.Name = "Лінійна (Тривимірна)";
            this.Label = "a0 + a1*x1+a2*x2";
            Values = new Dictionary<string, Evaluation>();
            this.Function = (X) => MultiDimensionalVector.ScalarMultiplying(AParams, X);
            Values = new Dictionary<string, Evaluation>();
        }
        public CubeLinearRegression(MultiDimensionalSample S) : this()
        {
            int X1 = S.Properties.RegressionVectors[0],
                X2 = S.Properties.RegressionVectors[1],
                Y = S.Properties.DependentVector;

            for (int i = 0; i < S.Values.Height; i++)
            {
                YAverage += S.Values[i, S.Properties.DependentVector];
                X1Average += S.Values[i, X1];
                X2Average += S.Values[i, X2];

                YX1Average += S.Values[i, Y] * S.Values[i, X1];
                YX2Average += S.Values[i, Y] * S.Values[i, X2];
                X1X2Average += S.Values[i, X1] * S.Values[i, X2];

                X1SquareAverage += S.Values[i, X1] * S.Values[i, X1];
                X2SquareAverage += S.Values[i, X2] * S.Values[i, X2];
            }
            YAverage /= S.Values.Height;
            X1Average /= S.Values.Height;
            X2Average /= S.Values.Height;

            YX1Average /= S.Values.Height;
            YX2Average /= S.Values.Height;
            X1X2Average /= S.Values.Height;

            X1SquareAverage /= S.Values.Height;
            X2SquareAverage /= S.Values.Height;

            AParams = SystemOfLinearEquations.SolveByGauss(new Matrix(
                new double[,] {
                    { 1, X1Average, X2Average },
                    { X1Average, X1SquareAverage, X1X2Average },
                    { X2Average, X1X2Average, X2SquareAverage }}
                ), new MultiDimensionalVector(new double[] { YAverage, YX1Average, YX2Average}));

            SetResidualDispersion(S.Values.ToVectorArray(), X1, X2, Y);
            SetA(S.DispersionCovariationMatrix, 1 - S.Alpha / 2, S.Values.Height - 3);
            SetDeterminationCoefficient(S.CorrelationMatrix, Y, X1, X2);
        }
        void SetA(Matrix DC, double Alpha, int V)
        {
            double Q = Quantile.Student(Alpha, V);
            A = new AdvancedEvaluation[3];
            for (int i = 0; i < AParams.Dimensionality; i++)
            {
                A[i] = new AdvancedEvaluation("A("+ i.ToString() + ")", AParams[i], AParams[i] - Q * Math.Sqrt(DC[i, i]), AParams[i] + Q * Math.Sqrt(DC[i, i]));
                Values.Add(A[i].Name, A[i]);
            }
        }
        void SetResidualDispersion(MultiDimensionalVector[] Values, int X1, int X2, int Y)
        {
            double S = 0;
            for (int i = 0; i < Values.Length; i++)
                S += Math.Pow(Values[i][Y] - Function(new MultiDimensionalVector(new double[] { 1, Values[i][X1], Values[i][X2] })), 2);

            ResidualDispersion = new Evaluation("S зал.", S / (Values.Length - Values[0].Dimensionality));
            this.Values.Add("ResidualDispersion", ResidualDispersion);
        }
        void SetDeterminationCoefficient(Matrix Corelation, int Y, int X1, int X2)
        {
            Matrix M = new Matrix
            ( new double[,]{
            { 1, Corelation[X1, X2], Corelation[X1, Y] },
            { Corelation[X1, X2], 1, Corelation[X2, Y]},
            { Corelation[X1, Y],Corelation[X2, Y], 1} } );
            double R = 1 - Math.Abs(M.GetDeterminant() / M.Minor(Y, Y).GetDeterminant());
            DeterminationCoefficient = new Evaluation("R^2", R);
            Values.Add("DeterminationCoefficient", DeterminationCoefficient);
        }
    }
    public class MultiLinearRegression : LinearRegression
    {
        int Dimensionality;
        protected Matrix X;
        protected MultiDimensionalVector Y;
        protected Matrix C;
        protected MultiDimensionalVector AParams;
        new public Evaluation[] A;
        public new Func<MultiDimensionalVector, double> Function;
        public MultiLinearRegression()
        {
            this.Name = "Лінійна (багатовимірна)";
            this.Label = "AX";
            this.Function = (X) => MultiDimensionalVector.ScalarMultiplying(AParams, X);
            Values = new Dictionary<string, Evaluation>();
        }
        public MultiLinearRegression(MultiDimensionalSample S) : this()
        {
            int DependentIndex = S.Properties.DependentVector;
            int[] VariableIndecies = S.Properties.RegressionVectors;

            Dimensionality = S.Values.Width;
            Y = new MultiDimensionalVector(S.Values.Height);
            X = new Matrix(S.Values.Height, VariableIndecies.Length);
            for (int i = 0, j; i < S.Values.Height; i++)
            {
                Y[i] = S.Values[i, DependentIndex];
                for (j = 0; j < VariableIndecies.Length; j++)
                    X.SetValue(i, j, S.Values[i, VariableIndecies[j]]);
            }
            C = Matrix.GetReverse((X.GetTransponed() * X));
            AParams = C * (X.GetTransponed() * Y);
            SetResidualDispersion();
            SetA(S.Alpha, S.Values.Height - VariableIndecies.Length + 1);
            SetDeterminationCoefficient(S);
        }
        protected virtual void SetA(double Alpha, int V)
        { 
            A = new AdvancedEvaluation[AParams.Dimensionality];
            double Q = Quantile.Student(Alpha, V);
            double c;
            for (int i = 0; i < AParams.Dimensionality; i++)
            {
                c = Math.Sqrt(ResidualDispersion.Data * C[i, i]);
                A[i] = new AdvancedEvaluation("A(" + i.ToString() + ")", AParams[i], AParams[i] + Q * c, AParams[i] - Q * c);
                Values.Add(A[i].Name, A[i]);
            }
        }
        protected void SetResidualDispersion()
        {
            double S = 0;
            for (int i = 0; i < X.Height; i++)
                S += Math.Pow(Y[i] - Function((MultiDimensionalVector)X.GetRow(i)), 2) / (X.Height - X.Width - 1);

            ResidualDispersion = new Evaluation("S зал.", S);
            this.Values.Add("ResidualDispersion", ResidualDispersion);
        }
        protected virtual void SetDeterminationCoefficient(MultiDimensionalSample S)
        {
            Matrix Correlation = S.CorrelationMatrix.Copy();
            for(int i = Correlation.Height - 1; i >= 0; i--)
                if (i != S.Properties.DependentVector && !S.Properties.RegressionVectors.Contains(i))
                {
                    Correlation = Matrix.RemoveColumn(Correlation, i);
                    Correlation = Matrix.RemoveRow(Correlation, i);
                }
            double R = 1 - Math.Abs(Correlation.GetDeterminant() / Correlation.Minor(Correlation.Height - 1, Correlation.Height - 1).GetDeterminant());

            DeterminationCoefficient = new Evaluation("R^2", R);
            Values.Add("DeterminationCoefficient", DeterminationCoefficient);
        }
    }
    public class MultiLinearRegressionE : MultiLinearRegression
    {
        double A0;
        int Dimensionality;
        MultiDimensionalVector XAverage;
        double YAverage;
        public MultiLinearRegressionE()
        {
            this.Name = "Лінійна (багатовимірна з вільним чл.)";
            this.Label = "AX + E";
            this.Function = (X) => MultiDimensionalVector.ScalarMultiplying(AParams, X) + A0;
            Values = new Dictionary<string, Evaluation>();
        }
        public MultiLinearRegressionE(MultiDimensionalSample S) : this()
        {
            int DependentIndex = S.Properties.DependentVector;
            int[] VariableIndecies = S.Properties.RegressionVectors;

            Dimensionality = S.Values.Width;
            Y = S.Values.GetColumn(DependentIndex);
            //new MultiDimensionalVector(S.Values.Height);
            X = new Matrix(S.Values.Height, VariableIndecies.Length);
            XAverage = new MultiDimensionalVector(S.Properties.RegressionVectors.Length);
            YAverage = 0;

            for (int i = 0, j; i < S.Values.Height; i++)
            {
                YAverage += Y[i] / S.Values.Height;
                for (j = 0; j < VariableIndecies.Length; j++)
                {
                    X.SetValue(i, j, S.Values[i, VariableIndecies[j]]);
                    XAverage[j] += S.Values[i, VariableIndecies[j]] / S.Values.Height;
                }
            }
            C = Matrix.GetReverse(X.GetTransponed() * X);
            AParams = Matrix.GetReverse((X - XAverage).GetTransponed() * (X - XAverage)) * ((X - XAverage).GetTransponed() * (Y - YAverage));

            A0 = 0;
            for (int i = 0; i < XAverage.Dimensionality; i++)
                A0 += AParams[i] * MultiDimensionalVector.GetAverage(S.Values.ToVectorArray(), VariableIndecies[i]);
            A0 = YAverage - A0;

            SetResidualDispersion();
            SetA(S.Alpha, S.Values.Height - VariableIndecies.Length + 1);
            SetDeterminationCoefficient(S);
        }

        protected override void SetA(double Alpha, int V)
        {
            A = new Evaluation[AParams.Dimensionality + 1];
            double Q = Quantile.Student(Alpha, V);
            double c;
            A[0] = new Evaluation("A(0)", A0);
            Values.Add(A[0].Name, A[0]);
            for (int i = 0; i < AParams.Dimensionality; i++)
            {
                c = Math.Sqrt(ResidualDispersion.Data * C[i, i]);
                A[i + 1] = new AdvancedEvaluation("A(" + (i + 1).ToString() + ")", AParams[i], AParams[i] + Q * c, AParams[i] - Q * c);
                Values.Add(A[i + 1].Name, A[i + 1]);
            }
        }
    }
    public class SmallestSquaresLinearRegression : LinearRegression
    {
        public SmallestSquaresLinearRegression() : base()
        {
            this.Name = "Лінійна (МНК)";
        }
        public SmallestSquaresLinearRegression(Data[] InputArray, TwoDimensionalStatistics Statistics)
        {
            SetRegression(InputArray, Statistics);
            SetAddition(Statistics, InputArray.Length);
            xMiddle = Statistics["MathExpectation(x)"].Data;
        }

        public virtual void SetRegression(Data[] InputArray, TwoDimensionalStatistics Statistics)
        {
            SetB(Statistics["Sigma(x)"].Data, Statistics["Sigma(y)"].Data, Statistics["CorrelationCoefficient"].Data);
            SetA(Statistics["MathExpectation(x)"].Data, Statistics["MathExpectation(y)"].Data, B.Data);
            SetResidualDispersion(InputArray);
        }

        protected virtual void SetAddition(TwoDimensionalStatistics Statistics, int Size)
        {
            SetSigmaB(Statistics, Size);
            SetSigmaA(Statistics, Size);
            SetDeterminationCoefficient(Statistics["Sigma(y)"].Data);
        }
        protected virtual void SetDeterminationCoefficient(double SigmaY)
        {
            DeterminationCoefficient = new Evaluation("R^2", (1d - ResidualDispersion.Data / (SigmaY * SigmaY)) * 100);
            Values.Add(DeterminationCoefficient.Name, DeterminationCoefficient);
        }
        protected virtual void SetB(double SigmaX, double SigmaY, double Correlation)
        {
            B = new AdvancedEvaluation("b", Correlation * SigmaY / SigmaX);
        }
        protected virtual void SetA(double MathExpectationX, double MathExpectationY, double B)
        {
            A = new AdvancedEvaluation("a", MathExpectationY - B * MathExpectationX);
        }
        protected virtual void SetResidualDispersion(Data[] InputArray)
        {
            double Sum = 0;
            for (int i = 0; i < InputArray.Length; i++)
                Sum += Math.Pow(InputArray[i].YValue - Function(InputArray[i].XValue), 2);
            ResidualDispersion = new Evaluation("ResidualDispersion", Sum / (InputArray.Length - 2));
            Values.Add(ResidualDispersion.Name, ResidualDispersion);
        }
        protected virtual void SetSigmaA(TwoDimensionalStatistics Statistic, int Size)
        {
            SigmaA = new Evaluation("Sigma(a)", Math.Sqrt(ResidualDispersion.Data) * Math.Sqrt(1d / Size + Math.Pow(Statistic["MathExpectation(x)"].Data, 2) / (Math.Pow(Statistic["Sigma(x)"].Data, 2) * (Size - 1))));
            Values.Add(SigmaA.Name, SigmaA);
            Quantilia = Quantile.Student(1 - Statistic.Alpha / 2, Size - 2);

            A = SetBorders(A, Quantilia, SigmaA.Data);

            Values.Add(A.Name, A);
        }
        protected virtual void SetSigmaB(TwoDimensionalStatistics Statistic, int Size)
        {
            SigmaB = new Evaluation("Sigma(b)", Math.Sqrt(ResidualDispersion.Data) / (Math.Pow(Statistic["Sigma(x)"].Data, 2) * (Size - 1)));
            Values.Add(SigmaB.Name, SigmaB);

            Quantilia = Quantile.Student(1 - Statistic.Alpha / 2, Size - 2);

            B = SetBorders(B, Quantilia, SigmaB.Data);

            Values.Add(B.Name, B);
        }
    }
    public class TeilLinearRegression : SmallestSquaresLinearRegression
    {
        public TeilLinearRegression(Data[] InputArray, TwoDimensionalStatistics Statistics):base()
        {
            this.Name = "Лінійна (метод Тейла)";

            SetRegression(InputArray);

            base.SetAddition(Statistics, InputArray.Length);
            xMiddle = Statistics["MathExpectation(x)"].Data;
        }
        public void SetRegression(Data[] InputArray)
        {
            SetB(InputArray);
            SetA(InputArray);
            SetResidualDispersion(InputArray);
        }
        void SetB(Data[] InputArray)
        {
            //HelpArray = new double[InputArray.Length * (InputArray.Length - 1)];
            List<double> BValues = new List<double>();
            for (int i = 1, j; i < InputArray.Length; i++)
                for (j = 0; j < i; j++)
                    BValues.Add((InputArray[i].YValue - InputArray[j].YValue) / (InputArray[i].XValue - InputArray[j].XValue));
            HelpArray = BValues.ToArray();
            Array.Sort(HelpArray);
            B = new AdvancedEvaluation("b", SetMED());
            HelpArray = null;
        }
        void SetA(Data[] InputArray)
        {
            HelpArray = new double[InputArray.Length];
            for (int i = 0; i < HelpArray.Length; i++)
                HelpArray[i] = InputArray[i].YValue - B.Data * InputArray[i].XValue;
            Array.Sort(HelpArray);
            A = new AdvancedEvaluation("a", SetMED());
            HelpArray = null;
        }
        double SetMED() => (HelpArray.Length % 2 == 0) ?
                (HelpArray[(HelpArray.Length - 1) / 2] + HelpArray[(HelpArray.Length - 1) / 2 - 1]) / 2 : HelpArray[(HelpArray.Length - 1) / 2];
    }

    public class PolynomialRegrression : SmallestSquaresLinearRegression
    {
        protected AdvancedEvaluation C;
        protected Evaluation SigmaC;

        int N
        { get => InputArray.Length; }
        Data[] InputArray;
        double MiddleX = 0;
        double MiddleXSquare = 0;
        double MiddleXCube = 0;
        double SigmaX = 0;
        double MiddleY = 0;
        double MiddlePhiSquare = 0;
        double Phi1(double X) => X - MiddleX;
        double Phi2(double X) => X * X - Phi1(X) * (MiddleXCube - MiddleXSquare * MiddleX) / (SigmaX * SigmaX) - MiddleXSquare;
        public static new Func<double, double, double, double, double> Param = (x, A, B, C) => A + B * x + C * x * x;

        private PolynomialRegrression()
        {
            Name = "Параболічна";
            Label = "a + bx + cx^2";
            Function = (x) => A.Data + B.Data * Phi1(x) + C.Data * Phi2(x);
        }
        public PolynomialRegrression(Data[] InputArray, TwoDimensionalStatistics Statistics): this()
        {
            this.InputArray = InputArray;
            SetParameters(Statistics);
            SetRegression();
            SetAddition(Statistics, InputArray.Length);
            xMiddle = Statistics["MathExpectation(x)"].Data;
        }

        public void SetParameters(TwoDimensionalStatistics Statistics)
        {
            HelpArray = new double[N];
            SigmaX = Statistics["Sigma(x)"].Data;
            MiddleX = Statistics["MathExpectation(x)"].Data;
            MiddleY = Statistics["MathExpectation(y)"].Data;
            for (int i = 0; i < N; i++)
            {
                HelpArray[i] = InputArray[i].XValue;
                MiddleXSquare += (InputArray[i].XValue * InputArray[i].XValue) / N;
                MiddleXCube += (InputArray[i].XValue * InputArray[i].XValue * InputArray[i].XValue) / N;
            }
        }
        void SetRegression()
        {
            SetA();
            SetB();
            SetC();
            SetResidualDispersion(InputArray);
        }
        void SetA()
        {
            A = new AdvancedEvaluation("a", MiddleY);
        }
        void SetB()
        {
            double Sum = 0;
            for (int i = 0; i < N; i++)
                Sum += (Phi1(InputArray[i].XValue) * InputArray[i].YValue); 
            B = new AdvancedEvaluation("b", Sum / (SigmaX * SigmaX * N));
        }
        void SetC()
        {
            double SumTop = 0;
            double SumBot = 0;
            MiddlePhiSquare = 0;
            double f;
            for (int i = 0; i < N; i++)
            {
                f = Phi2(InputArray[i].XValue);
                MiddlePhiSquare += (f * f) / N;
                SumTop += f * InputArray[i].YValue;
                SumBot += f * f;
            }
            C = new AdvancedEvaluation("c", SumTop / SumBot);
        }
        protected override void SetAddition(TwoDimensionalStatistics Statistics, int Size)
        {
            SetSigmaA(Statistics, InputArray.Length);
            SetSigmaB(Statistics, InputArray.Length);
            SetSigmaC(Statistics, InputArray.Length);
            SetDeterminationCoefficient(Statistics["Sigma(y)"].Data);

        }
        protected override void SetResidualDispersion(Data[] InputArray)
        {
            double Sum = 0;
            for (int i = 0; i < InputArray.Length; i++)
                Sum += Math.Pow(InputArray[i].YValue - Function(InputArray[i].XValue), 2);
            ResidualDispersion = new Evaluation("ResidualDispersion", Sum / (InputArray.Length - 3));
            Values.Add(ResidualDispersion.Name, ResidualDispersion);
        }
        protected override void SetSigmaA(TwoDimensionalStatistics Statistic, int Size)
        {
            SigmaA = new Evaluation("Sigma(a)", Math.Sqrt(ResidualDispersion.Data) / Math.Sqrt(Size));
            Values.Add(SigmaA.Name, SigmaA);
            Quantilia = Quantile.Student(1 - Statistic.Alpha / 2, Size - 3);

            A = SetBorders(A, Quantilia, SigmaA.Data);
            Values.Add(A.Name, A);
        }
        protected override void SetSigmaB(TwoDimensionalStatistics Statistic, int Size)
        {
            SigmaB = new Evaluation("Sigma(b)", Math.Sqrt(ResidualDispersion.Data) / (Math.Sqrt(Size) * Statistic["Sigma(x)"].Data));
            Values.Add(SigmaB.Name, SigmaB);

            Quantilia = Quantile.Student(1 - Statistic.Alpha / 2, Size - 3);

            B = SetBorders(B, Quantilia, SigmaB.Data);
            Values.Add(B.Name, B);
        }
        protected void SetSigmaC(TwoDimensionalStatistics Statistic, int Size)
        {
            SigmaC = new Evaluation("Sigma(c)", Math.Sqrt(ResidualDispersion.Data) / Math.Sqrt(Size * MiddlePhiSquare));
            Values.Add(SigmaC.Name, SigmaC);

            Quantilia = Quantile.Student(1 - Statistic.Alpha / 2, Size - 3);

            C = SetBorders(C, Quantilia, SigmaC.Data);
            Values.Add(C.Name, C);
        }
        public override double SigmaDivergence (double x, int Size) => Math.Sqrt(ResidualDispersion.Data* (1d / Size) + Math.Pow(SigmaB.Data, 2) * Math.Pow(Phi1(x), 2) + Math.Pow(SigmaC.Data, 2) * Math.Pow(Phi2(x), 2));
        public override double SigmaNewObservation (double x, int Size) => Math.Sqrt(ResidualDispersion.Data* (1d / Size + 1) + Math.Pow(SigmaB.Data, 2) * Math.Pow(Phi1(x), 2) + Math.Pow(SigmaC.Data, 2) * Math.Pow(Phi2(x), 2));
    }

    public struct QuziLinearRegressionWeigthFunction
    {
        public string Key;
        public Func<double, double> Phi;
        public Func<double, double> Psi;
        public Func<double, double, double> W;
        public Func<double, double> Reverse;
        public Func<double, double> a;
        public Func<double, double> b;
        public Func<double, double, double, double> Param;
    }
    public class QuziLinearRegression : SmallestSquaresLinearRegression
    {
        public static string[] FunctionKeys
        {
            get => FunctionList.Keys.ToArray();
        }
        new Data[] HelpArray;
        QuziLinearRegressionWeigthFunction WeigthFunctions;
        public Func<double, double> LinearFunction;
        public Func<double, double> Reverse;

        double Alpha;

        double[] T;
        double[] Z;
        double MiddleT;
        double MiddleZ;
        double SigmaT;
        double SigmaZ;
        double MiddleTZ;
        double Correlation;
        
        public QuziLinearRegression(Data[] InputArray, double Alpha, string Key)
        {
            Name = "Квазілінійна";
            Label = Key;
            this.Alpha = Alpha;
            WeigthFunctions = FunctionList[Key];
            HelpArray = new Data[InputArray.Length];
            InputArray.CopyTo(HelpArray, 0);
            Function = (x) => Reverse(LinearFunction(x));
            LinearFunction = (x) => A.Data + B.Data * WeigthFunctions.Phi(x);
            Reverse = WeigthFunctions.Reverse;
            Param = WeigthFunctions.Param;
            SetRegression();
            SetAddition(InputArray);
        }
        void SetRegression()
        {
            SetFunctionValues();
            SetB();
            SetA();
        }
        void SetAddition(Data[] InputArray)
        {
            SetResidualDispersion(InputArray);
            SetSigmaA();
            SetSigmaB();
            SetDeterminationCoefficient(SigmaZ);
        }
        void SetFunctionValues()
        {
            double w;
            double f;
            double p;
            int i;

            T = new double[HelpArray.Length];
            Z = new double[HelpArray.Length];
            for (i = 0; i < HelpArray.Length; i++)
            {
                f = WeigthFunctions.Phi(HelpArray[i].XValue);
                p = WeigthFunctions.Psi(HelpArray[i].YValue);
                w = WeigthFunctions.W(HelpArray[i].XValue, HelpArray[i].YValue);

                T[i] = f;
                Z[i] = p;
            }
            MiddleT = Statistics.SetMathExpectation(T);
            MiddleZ = Statistics.SetMathExpectation(Z);
            SigmaT = Statistics.SetSigma(T, MiddleT);
            SigmaZ = Statistics.SetSigma(Z, MiddleZ);
            MiddleTZ = Statistics.SetMathExpectation(T, Z);
            Correlation = TwoDimensionalStatistics.SetCorrelationCoefficient(MiddleT, SigmaT, MiddleZ, SigmaZ, MiddleTZ, T.Length);
        }
        void SetB() => B = new AdvancedEvaluation("b", Correlation * SigmaZ / SigmaT);
        void SetA() => A = new AdvancedEvaluation("a", MiddleZ - B.Data * MiddleT);

        protected override void SetResidualDispersion(Data[] InputArray)
        {
            double Sum = 0;
            for (int i = 0; i < InputArray.Length; i++)
                Sum += Math.Pow(WeigthFunctions.Psi(InputArray[i].YValue) - LinearFunction(InputArray[i].XValue), 2);
            ResidualDispersion = new Evaluation("ResidualDispersion", Sum / (InputArray.Length - 2));
            Values.Add(ResidualDispersion.Name, ResidualDispersion);
        }
        protected void SetSigmaA()
        {
            SigmaA = new Evaluation("Sigma(a)", Math.Sqrt(ResidualDispersion.Data) * Math.Sqrt(1d / HelpArray.Length + MiddleT * MiddleT / (SigmaT * SigmaT * (HelpArray.Length - 1))));
            Values.Add(SigmaA.Name, SigmaA);
            Quantilia = Quantile.Student(1 - Alpha / 2, HelpArray.Length - 2);

            A = SetBorders(A, Quantilia, SigmaA.Data);
            Values.Add(A.Name, A);
        }
        protected void SetSigmaB()
        {
            SigmaB = new Evaluation("Sigma(b)", Math.Sqrt(ResidualDispersion.Data) / (SigmaT * Math.Sqrt(HelpArray.Length - 1)));
            Values.Add(SigmaB.Name, SigmaB);

            Quantilia = Quantile.Student(1 - Alpha / 2, HelpArray.Length - 2);

            B = SetBorders(B, Quantilia, SigmaB.Data);
            Values.Add(B.Name, B);
        }

        public override double SigmaDivergence(double x, int Size) => Math.Sqrt(ResidualDispersion.Data * (1d / Size) + Math.Pow(SigmaB.Data, 2) * Math.Pow(WeigthFunctions.Phi(x) - MiddleT, 2));
        public override double SigmaNewObservation(double x, int Size) => Math.Sqrt(ResidualDispersion.Data * (1d / Size + 1) + Math.Pow(SigmaB.Data, 2) * Math.Pow(WeigthFunctions.Phi(x) - MiddleT, 2));

        public static Dictionary<string, QuziLinearRegressionWeigthFunction> FunctionList = new Dictionary<string, QuziLinearRegressionWeigthFunction>
        {
            { "a + bx", new QuziLinearRegressionWeigthFunction
                {
                    Key = "a + bx",
                    Phi = (x) => x,
                    Psi = (y) => y,
                    W = (x, y) => 1,
                    Reverse = (z) => z,
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => A + B * x
                }
            },
            { "Sqrt(a + bx)", new QuziLinearRegressionWeigthFunction
                {
                    Key = "Sqrt(a + bx)",
                    Phi = (x) => x,
                    Psi = (y) => y * y,
                    W = (x, y) => 1d / (4 * y * y),
                    Reverse = (z) => Math.Sqrt(z),
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => Math.Sqrt(A + B * x)
                }
            },
            { "a + b/x", new QuziLinearRegressionWeigthFunction
                {
                    Key = "a + b/x",
                    Phi = (x) => 1d / x,
                    Psi = (y) => y,
                    W = (x, y) => 1d /(x * x * x * x),
                    Reverse = (z) => z,
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => A + B / x
                }
            },
            { "1/(a + bx)", new QuziLinearRegressionWeigthFunction
                {
                    Key = "1/(a + bx)",
                    Phi = (x) => x,
                    Psi = (y) => 1d / y,
                    W = (x, y) => y * y * y * y,
                    Reverse = (z) => 1d / z,
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => 1d / (A + B * x)
                }
            },
            { "a + b*ln(x)", new QuziLinearRegressionWeigthFunction
                {
                    Key = "a + b*ln(x)",
                    Phi = (x) => Math.Log(x),
                    Psi = (y) => y,
                    W = (x, y) => 1d / (x * x),
                    Reverse = (z) => z,
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => A + B * Math.Log(x)
                }
            },
            { "a*x^b", new QuziLinearRegressionWeigthFunction
                {
                    Key = "a * x^b",
                    Phi = (x) => Math.Log(x),
                    Psi = (y) => Math.Log(y),
                    W = (x, y) => (y * y) / (x * x),
                    Reverse = (z) => Math.Exp(z),
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => A * Math.Pow(x, B)
                }
            },
            { "a*exp(bx)", new QuziLinearRegressionWeigthFunction
                {
                    Key = "a*exp(bx)",
                    Phi = (x) => x,
                    Psi = (y) => Math.Log(y),
                    W = (x, y) => y * y,
                    Reverse = (z) => Math.Exp(z),
                    Param = (x, A, B) => A * Math.Exp(B * x)
                }
            },
            { "a + bx^3", new QuziLinearRegressionWeigthFunction
                {
                    Key = "a + bx^3",
                    Phi = (x) => x * x * x,
                    Psi = (y) => y,
                    W = (x, y) => 9 * x * x * x * x,
                    Reverse = (y) => y,
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => A + B * x * x * x
                }
            },
            { "a*exp(b/x)", new QuziLinearRegressionWeigthFunction
                {
                    Key = "a*exp(b/x)",
                    Phi = (x) => 1d / x,
                    Psi = (y) => Math.Log(y),
                    W = (x, y) => (y * y) / (x * x * x * x),
                    Reverse = (z) => Math.Exp(z),
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => A * Math.Exp(B / x)
                }
            },
            { "b/(1/x + a)", new QuziLinearRegressionWeigthFunction
                {
                    Key = "b/(1/x + a)",
                    Phi = (x) => 1d / x,
                    Psi = (y) => 1d / y,
                    W = (x, y) => (y * y * y * y) / (x * x * x * x),
                    Reverse = (z) => 1d / z,
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => B / (1d / x + A)
                }
            },
            { "1/(a + b*exp(-x))", new QuziLinearRegressionWeigthFunction
                {
                    Key = "1/(a + b*exp(-x))",
                    Phi = (x) => Math.Exp(-x),
                    Psi = (y) => 1d / y,
                    W = (x, y) => y * y * y * y * Math.Exp(-2 * x),
                    Reverse = (z) => 1d / z,
                    a = (A) => A,
                    b = (B) => B,
                    Param = (x, A, B) => 1d / (A + B * Math.Exp(-x))
                }
            },

        };
    }
}
