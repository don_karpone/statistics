﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;

namespace Statistics
{
    public class PFA
    {
        public enum Approximations { AllTypes = 0, MaximumCorelation, Trinity, AverageValues, Centroids, Aleroids, ByPCA }

        public readonly int TotalCount;

        public Matrix EigenVectors;
        public MultiDimensionalVector EigenValues;
        public int FactorsCount;
        int CorrelationFactorsCount;

        int Dimensionality;
        double AverageEigenValue;

        Matrix Factors;

        MultiDimensionalVector Communality;

        public Func<Matrix, MultiDimensionalVector> ApproximateCommunalities;

        public PFA()
        {
            ApproximateCommunalities = this.AllTypes;
        }

        public Tuple<Matrix, MultiDimensionalVector> ComputePFA(Matrix CorrelationMatrix, int Accuracy, int MaximumIterations)
        {
            Matrix Reduction = CorrelationMatrix.Copy();

            int Iterations = 0;
            Dimensionality = CorrelationMatrix.Height;
            double Eps = Math.Pow(10, -Accuracy);
            double fOld;
            double fNew;

            Tuple<MultiDimensionalVector, Matrix> T;
            T = Matrix.FindEigenValues(Reduction);
            EigenValues = T.Item1;
            EigenVectors = T.Item2;

            Sort();

            CorrelationFactorsCount = 0;
            foreach (double l in EigenValues.ToArray())
                if (Math.Abs(l) >= 1)
                    CorrelationFactorsCount++;
                else
                    break;

            Communality = ApproximateCommunalities(Reduction);

            Factors = SetFactors(CorrelationFactorsCount);

            Matrix PrevFactors;
            MultiDimensionalVector PreCommunalities;

            fNew = Double.MaxValue;

            do
            { 
                PrevFactors = Factors.Copy();
                PreCommunalities = Communality.Copy();
                fOld = fNew;

                SetCommunalitiesToReduction(ref Reduction, Communality);

                T = Matrix.FindEigenValues(Reduction);
                EigenValues = T.Item1;
                EigenVectors = T.Item2;

                Sort();

                AverageEigenValue = EigenValues.ToArray().Average();

                FactorsCount = 0;
                foreach (double l in EigenValues.ToArray())
                    if (l < AverageEigenValue)
                        break;
                    else
                        FactorsCount++;

                if (FactorsCount < CorrelationFactorsCount)
                    FactorsCount = CorrelationFactorsCount;

                Factors = SetFactors(FactorsCount);

                for (int i = 0, j; i < Communality.Dimensionality; i++)
                {
                    Communality[i] = 0;
                    for (j = 0; j < FactorsCount; j++)
                        Communality[i] += Factors[i, j] * Factors[i, j];
                }

                fNew = CostFunction(Reduction, Factors);

                Iterations++;
            }
            while (CommunalityCheck(Communality) && CostCheck(fOld, fNew) && ACheck(Factors, PrevFactors, Eps) && Iterations < MaximumIterations);

            return new Tuple<Matrix, MultiDimensionalVector>(PrevFactors, PreCommunalities);
        }

        public Tuple<Matrix, MultiDimensionalVector> ComputePFA(Matrix CorrelationMatrix, int Accuracy, int MaximumIterations, int FactorsCount)
        {
            Matrix Reduction = CorrelationMatrix.Copy();

            int Iterations = 0;
            Dimensionality = CorrelationMatrix.Height;
            double Eps = Math.Pow(10, -Accuracy);
            double fOld;
            double fNew;

            Tuple<MultiDimensionalVector, Matrix> T;
            T = Matrix.FindEigenValues(Reduction);
            EigenValues = T.Item1;
            EigenVectors = T.Item2;

            Sort();

            Communality = ApproximateCommunalities(Reduction);

            this.FactorsCount = FactorsCount;

            Factors = SetFactors(FactorsCount);

            Matrix PrevFactors;
            MultiDimensionalVector PreCommunalities;

            fNew = Double.MaxValue;

            do
            {
                PrevFactors = Factors.Copy();
                PreCommunalities = Communality.Copy();
                fOld = fNew;

                SetCommunalitiesToReduction(ref Reduction, Communality);

                T = Matrix.FindEigenValues(Reduction);
                EigenValues = T.Item1;
                EigenVectors = T.Item2;

                Sort();

                AverageEigenValue = EigenValues.ToArray().Average();

                Factors = SetFactors(FactorsCount);

                for (int i = 0, j; i < Communality.Dimensionality; i++)
                {
                    Communality[i] = 0;
                    for (j = 0; j < FactorsCount; j++)
                        Communality[i] += Factors[i, j] * Factors[i, j];
                }

                fNew = CostFunction(Reduction, Factors);

                Iterations++;
            }
            while (CommunalityCheck(Communality) && CostCheck(fOld, fNew) && ACheck(Factors, PrevFactors, Eps) && Iterations < MaximumIterations);

            return new Tuple<Matrix, MultiDimensionalVector>(PrevFactors, PreCommunalities);
        }

        double CostFunction(Matrix Reduction, Matrix Factors)
        {
            Matrix R = Reduction - Factors * Factors.GetTransponed();

            double f = 0;
            for (int i = 0, j; i < R.Height; i++)
                for (j = 0; j < R.Width; j++)
                    if (i != j)
                        f += R[i, j] * R[i, j];
            return f;
        }

        bool CommunalityCheck(MultiDimensionalVector H)
        {
            foreach (double a in H.ToArray())
                if (a * a > 1)
                    return false;
            return true;
        }

        bool CostCheck(double fOld, double fNew) => true;// fOld > fNew;

        bool ACheck(Matrix A, Matrix B, double eps)
        {
            double Sum = 0;
            for (int i = 0, j; i < A.Height; i++)
                for (j = 0; j < A.Width; j++)
                    Sum += Math.Pow(A[i, j] - B[i, j], 2);
            return Sum > eps;
        }

        void Sort()
        {
            double V;
            MultiDimensionalVector Column;

            for (int i = 0; i < EigenValues.Dimensionality - 1; i++)
            {
                for (int j = EigenValues.Dimensionality - 1; j > i; j--)
                {
                    if (EigenValues[j] > EigenValues[j - 1])
                    {
                        V = EigenValues[j - 1];
                        EigenValues[j - 1] = EigenValues[j];
                        EigenValues[j] = V;

                        Column = EigenVectors.GetColumn(j - 1);
                        EigenVectors.SetColumn(j - 1, EigenVectors.GetColumn(j).ToArray());
                        EigenVectors.SetColumn(j, Column.ToArray());
                    }
                }
            }
        }

        public void SetApproximationType(Approximations Type)
        {
            switch (Type)
            {
                case Approximations.ByPCA:
                    ApproximateCommunalities = ByPCA;
                    break;
                case Approximations.MaximumCorelation:
                    ApproximateCommunalities = MaximumCorrelation;
                    break;
                case Approximations.AverageValues:
                    ApproximateCommunalities = AverageValues;
                    break;
                case Approximations.Trinity:
                    ApproximateCommunalities = Trinity;
                    break;
                case Approximations.Centroids:
                    ApproximateCommunalities = Centroids;
                    break;
                case Approximations.Aleroids:
                    ApproximateCommunalities = Aleroids;
                    break;
                default:
                    ApproximateCommunalities = AllTypes;
                    break;
            }
        }

        MultiDimensionalVector AllTypes(Matrix CorrelationMatrix)
        {
            Tuple<MultiDimensionalVector, Matrix> T;
            MultiDimensionalVector[] Communalities = new MultiDimensionalVector[6];
            Communalities[0] = MaximumCorrelation(CorrelationMatrix);
            Communalities[1] = Trinity(CorrelationMatrix);
            Communalities[2] = AverageValues(CorrelationMatrix);
            Communalities[3] = Centroids(CorrelationMatrix);
            Communalities[4] = Aleroids(CorrelationMatrix);
            Communalities[5] = ByPCA(CorrelationMatrix);

            double[] costs = new double[6];
            Matrix Factors;
            int MinimumIndex = 0;
            MultiDimensionalVector EigenValues;
            Matrix EigenVectors;

            for (int i = 0; i < costs.Length; i++)
            {
                SetCommunalitiesToReduction(ref CorrelationMatrix, Communalities[i]);

                T = Matrix.FindEigenValues(CorrelationMatrix);
                EigenValues = T.Item1;
                EigenVectors = T.Item2;

                Factors = SetFactors(FactorsCount);
                costs[i] = CostFunction(CorrelationMatrix, Factors);
                if (costs[i] < costs[MinimumIndex])
                    MinimumIndex = i;
            }

            return Communalities[MinimumIndex];
        }
        MultiDimensionalVector MaximumCorrelation(Matrix CorrelationMatrix)
        {
            MultiDimensionalVector Communality = new MultiDimensionalVector(CorrelationMatrix.Height);

            for (int i = 0, j; i < CorrelationMatrix.Height; i++)
            {
                Communality[i] = Double.MinValue;
                for (j = 0; j < CorrelationMatrix.Width; j++)
                    if (i != j)
                    {
                        if (Communality[i] < Math.Abs(CorrelationMatrix[i, j]))
                            Communality[i] = Math.Abs(CorrelationMatrix[i, j]);
                    }
            }
            return Communality;
        }
        MultiDimensionalVector Trinity(Matrix CorrelationMatrix)
        {
            MultiDimensionalVector Communality = new MultiDimensionalVector(CorrelationMatrix.Height);

            int a = 0, b = 0;
            double rA = 0, rB = 0;
            for (int i = 0, j; i < CorrelationMatrix.Height; i++)
            {
                Communality[i] = 0;
                for (j = 0; j < CorrelationMatrix.Width; j++)
                    if (i != j)
                    {
                        if (Communality[i] < Math.Abs(CorrelationMatrix[i, j]))
                        {
                            rA = Math.Abs(CorrelationMatrix[i, j]);
                            Communality[i] = rA;
                            a = j;
                        }
                    }
                Communality[i] = 0;
                for (j = 0; j < CorrelationMatrix.Width; j++)
                    if (i != j)
                    {
                        if (Communality[i] < Math.Abs(CorrelationMatrix[i, j]) 
                            && a != j)
                        {
                            rB = Math.Abs(CorrelationMatrix[i, j]);
                            Communality[i] = rB;
                            b = j;
                        }
                    }
                Communality[i] = Math.Abs(CorrelationMatrix[i, a] * CorrelationMatrix[i, b] / CorrelationMatrix[a, b]);
            }
            return Communality;
        }
        MultiDimensionalVector AverageValues(Matrix Correlation)
        {
            Matrix CorrelationMatrix = Correlation.Copy();
            MultiDimensionalVector Communality = new MultiDimensionalVector(CorrelationMatrix.Width);

            for (int i = 0, j; i < CorrelationMatrix.Height; i++)
                for (j = 0; j < CorrelationMatrix.Width; j++)
                    if (i != j)
                        Communality[i] += Math.Abs(CorrelationMatrix[i, j]) / (Correlation.Width - 1);

            return Communality;
        }
        MultiDimensionalVector Centroids(Matrix Correlation)
        {
            Matrix CorrelationMatrix = Correlation.Copy();
            MultiDimensionalVector Communality = new MultiDimensionalVector(CorrelationMatrix.Width);

            for (int i = 0, j; i < CorrelationMatrix.Height; i++)
            {
                CorrelationMatrix.SetValue(i, i, 0);
                for (j = 0; j < CorrelationMatrix.Width; j++)
                    if (i != j)
                    {
                        if (Math.Abs(CorrelationMatrix[i, j]) > CorrelationMatrix[i, i])
                            CorrelationMatrix.SetValue(i, i, Math.Abs(CorrelationMatrix[i, j]));
                    }
            }

            double Sum = 0;
            for (int i = 0, j; i < CorrelationMatrix.Height; i++)
            {
                for (j = 0; j < CorrelationMatrix.Width; j++)
                {
                    Sum += Math.Abs(CorrelationMatrix[i, j]);
                }
            }

            double Sum1;
            for (int i = 0, j; i < CorrelationMatrix.Height; i++)
            {
                Sum1 = 0;
                for (j = 0; j < CorrelationMatrix.Width; j++)
                {
                    Sum1 += Math.Abs(CorrelationMatrix[i, j]);
                }
                Communality[i] = Sum1 * Sum1 / Sum;
            }
            return Communality;
        }
        MultiDimensionalVector Aleroids(Matrix Correlation)
        {
            Matrix CorrelationMatrix = Correlation.Copy();
            MultiDimensionalVector Communality = new MultiDimensionalVector(CorrelationMatrix.Width);

            double Sum = 0;
            for (int i = 0, j; i < CorrelationMatrix.Height; i++)
            {
                for (j = 0; j < CorrelationMatrix.Width; j++)
                {
                    if(i != j)
                        Sum += Math.Abs(CorrelationMatrix[i, j]);
                }
            }

            double Sum1;
            for (int i = 0, j; i < CorrelationMatrix.Height; i++)
            {
                Sum1 = 0;
                for (j = 0; j < CorrelationMatrix.Width; j++)
                {
                    if(i != j)
                        Sum1 += Math.Abs(CorrelationMatrix[i, j]);
                }
                Communality[i] = Sum1 * Sum1 / Sum * Correlation.Width / (Correlation.Width - 1);
            }

            return Communality;
        }
        MultiDimensionalVector ByPCA(Matrix CorrelationMatrix)
        {
            MultiDimensionalVector Communality = new MultiDimensionalVector(CorrelationMatrix.Width);
            MultiDimensionalVector Values;
            Matrix Vectors;
            Tuple<MultiDimensionalVector, Matrix> T = Matrix.FindEigenValues(CorrelationMatrix);
            Values = T.Item1;
            Vectors = T.Item2;

            for (int i = 0, j; i < CorrelationMatrix.Height; i++)
            {
                for (j = 0; j < CorrelationMatrix.Width; j++)
                {
                    if (Math.Abs(Values[j]) >= 1)
                        Communality[i] += Vectors[j, i] * Vectors[j, i];
                }
            }

            return Communality;
        }

        void SetCommunalitiesToReduction(ref Matrix R, MultiDimensionalVector V)
        {
            for (int i = 0; i < R.Height; i++)
                R.SetValue(i, i, V[i]);
        }

        Matrix SetFactors(int Count)
        {
            Matrix Factors = new Matrix(Dimensionality, Count);
            for (int i = 0, j; i < Factors.Height; i++)
                for (j = 0; j < Factors.Width; j++)
                    Factors.SetValue(i, j, EigenVectors[i, j]);
            return Factors;
        }
    }
}
