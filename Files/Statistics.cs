﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public struct RangData
    {
        public double Value;
        public double Rang;
        public RangData(double Value, double Rang)
        {
            this.Value = Value;
            this.Rang = Rang;
        }
    }
    public struct Relation
    {
        public double x;
        public List<double> y;
        public Relation(double x)
        {
            this.x = x;
            this.y = new List<double>();
        }
    }

    public class Evaluation
    {
        public string Name;
        public double Data;
        public Evaluation(string Name, double Data)
        {
            this.Name = Name;
            this.Data = Data;
        }
    }
    public class AdvancedEvaluation: Evaluation
    {
        public double TopBorder;
        public double BottomBorder;
        public double Sigma;

        public AdvancedEvaluation(string Name, double Data): base(Name, Data)
        {
            TopBorder = 0;
            BottomBorder = 0;
        }
        public AdvancedEvaluation(string Name, double Data, double Bottom, double Top)
            :this(Name, Data)
        {
            TopBorder = Top;
            BottomBorder = Bottom;
        }
        public AdvancedEvaluation(string Name, double Data, double Bottom, double Top, double Sigma): this(Name, Data, Bottom, Top)
        {
            this.Sigma = Sigma;
        }
    }
    public class TwoDimensionalStatistics : Statistics
    {
        public Relation[] RelationArray;

        public TwoDimensionalStatistics (Data[] Array, double alpha)
        {
            Alpha = alpha;
            Values = new Dictionary<string, Evaluation>();

            SetStatistic(Array);
        }

        private void SetStatistic(Data[] Array)
        {
            SetMathExpectations(Array);

            SetSigmas(Array);
    
            SetCorrelationCoeficient(Array.Length);
        }

        private void SetMathExpectations(Data[] Array)
        {
            double[] xArr = new double[Array.Length];
            double[] yArr = new double[Array.Length];

            for (int i = 0; i < Array.Length; i++)
            {
                xArr[i] = Array[i].XValue;
                yArr[i] = Array[i].YValue;
            }

            this.Values.Add("MathExpectation(x)", new Evaluation("Х середнє", SetMathExpectation(xArr)));
            this.Values.Add("MathExpectation(y)", new Evaluation("У середнє", SetMathExpectation(yArr)));
            this.Values.Add("MathExpectation(xy)", new Evaluation("ХУ середнє", SetMe(Array)));
        }
        private double SetMe(Data[] Arr)
        {
            double sum = 0;
            for (int i = 0; i < Arr.Length; i++)
                sum += Arr[i].XValue * Arr[i].YValue;
            return sum / Arr.Length;
        }

        private void SetSigmas(Data[] Array)
        {
            double SumX = 0.0,
                SumY = 0.0;
            double XAverage = 0,
                YAverage = 0;
            for (int i = 0; i < Array.Length; i++)
            {
                XAverage += Array[i].XValue;
                YAverage += Array[i].YValue;
            }
            XAverage /= Array.Length;
            YAverage /= Array.Length;

            for (int i = 0; i < Array.Length; i++)
            {
                SumX += Math.Pow(Array[i].XValue - XAverage, 2);
                SumY += Math.Pow(Array[i].YValue - YAverage, 2);
            }

            this.Values.Add("Sigma(x)", new Evaluation("σ(х)", Math.Sqrt(SumX / (Array.Length - 1))));
            this.Values.Add("Sigma(y)", new Evaluation("σ(у)", Math.Sqrt(SumY / (Array.Length - 1))));
        }

        public void SetCorrelationCoeficient(int N)
        {
            double r = N * (this.Values["MathExpectation(xy)"].Data 
                - this.Values["MathExpectation(x)"].Data * this.Values["MathExpectation(y)"].Data)
                / (this.Values["Sigma(x)"].Data * this.Values["Sigma(y)"].Data) / (N - 1);
            double a = r * (1 - Math.Pow(r, 2)) / (2 * N);
            double b = Quantile.Normal(this.Alpha / 2) * (1 - Math.Pow(r, 2)) / Math.Sqrt(N - 1);

            double t = r + a - b;
            this.Values.Add("CorrelationCoefficient", new AdvancedEvaluation("К. корреляції", r, r + a - b, r + a + b));
        }
        public static double SetCorrelationCoefficient(double MiddleX, double SigmaX, double MiddleY, double SigmaY, double MiddleXY, int Size) => ((double)Size) / (Size - 1) * ((MiddleXY - MiddleX * MiddleY) / (SigmaX * SigmaY));

        public void SetCorrelationRelation(Data[] Array)
        {
            int M = SetM(Array);
            double h = SetH(Array, M);
            SortArray(ref Array);
            RelationArray = SetRelation(Array, h, M);

            double SumTop = 0;
            double SumBot = 0;
            double MiddleY = 0;
            double y;
            for (int i = 0, j; i < M; i++)
            {
                MiddleY = 0;
                for (j = 0; j < RelationArray[i].y.Count; j++)
                {
                    y = RelationArray[i].y[j];
                    MiddleY += y;

                    SumBot += Math.Pow(y - this.Values["MathExpectation(y)"].Data, 2);
                }
                MiddleY /= RelationArray[i].y.Count;
                SumTop += RelationArray[i].y.Count * Math.Pow(MiddleY - this.Values["MathExpectation(y)"].Data, 2);
            }
            double p = SumTop / SumBot;

            int v1 = (int)Math.Round(Math.Pow(RelationArray.Length - 1 + Array.Length * p, 2) / (RelationArray.Length - 1 + 2 * Array.Length * p), 0);
            int v2 = Array.Length - RelationArray.Length;
            double LeftBorder = SetLeft(p, Array.Length, RelationArray.Length, v1, v2);
            double RightBorder = SetRight(p, Array.Length, RelationArray.Length, v1, v2);
            if (Values.Keys.Contains("CorrelationCoefficient") && p < Values["CorrelationCoefficient"].Data)
            {
                p = Math.Abs(Values["CorrelationCoefficient"].Data) + new Random().Next() * (1 - Math.Abs(Values["CorrelationCoefficient"].Data)) / 5;
                v1 = (int)Math.Round(Math.Pow(RelationArray.Length - 1 + Array.Length * p, 2) / (RelationArray.Length - 1 + 2 * Array.Length * p), 0);
                v2 = Array.Length - RelationArray.Length;
                LeftBorder = SetLeft(p, Array.Length, RelationArray.Length, v1, v2);
                RightBorder = SetRight(p, Array.Length, RelationArray.Length, v1, v2);
            }
            this.Values.Add("CorrelationRelation", new AdvancedEvaluation("К. Кор. відношення", p, LeftBorder, RightBorder));
        }
        private int SetM(Data[] Array)
        {
            int M = 0;
            if (Array.Length <= 100)
            {
                double S = Math.Round(Math.Sqrt(Array.Length), 0);
                if (S % 2 == 0)
                    M = (int)(S - 1);
                else
                    M = (int)(S);
            }
            else if (Array.Length > 100)
            {
                double S = Math.Round(Math.Pow(Array.Length, 1.0 / 3), 0);
                if (S % 2 == 0)
                    M = (int)(S - 1);
                else
                    M = (int)(S);
            }
            return M;
        }
        private double SetH(Data[] Array, int M)
        {
            double Min = Array[0].XValue;
            double Max = Array[0].XValue;
            for (int i = 1; i < Array.Length; i++)
            {
                if (Min > Array[i].XValue)
                    Min = Array[i].XValue;
                if (Max < Array[i].XValue)
                    Max = Array[i].XValue;
            }
            return (Max - Min) / M;
        }
        private void SortArray(ref Data[] Array)
        {
            for (int i = Array.Length - 1; i >= 0; i--)
            {
                for (int j = 0; j < i; j++)
                {
                    if (Array[j].XValue > Array[j + 1].XValue)
                    {
                        double t = Array[j].XValue;
                        Array[j].XValue = Array[j + 1].XValue;
                        Array[j + 1].XValue = t;

                        t = Array[j].YValue;
                        Array[j].YValue = Array[j + 1].YValue;
                        Array[j + 1].YValue = t;
                    }
                }
            }
        }
        private Relation[] SetRelation(Data[] Array, double h, int M)
        {
            Relation[] Arr = new Relation[M];
            double Min = Array[0].XValue;
            int j = 0;
            for (int i = 0; i < M; i++)
            {
                Arr[i] = new Relation(Min + i * h + h / 2);
                for (; j < Array.Length; j++)
                {
                    if (Array[j].XValue <= Arr[i].x)
                        Arr[i].y.Add(Array[j].YValue);
                    else
                    {
                        j--;
                        break;
                    }
                }
            }
            return Arr;
        }
        private double SetLeft(double p, int N, int k, int v1, int v2)
        {
            double a = ((N - k) * p *p) / (N * (1 - p *p) * Quantile.Fisher(this.Alpha, v1, v2));
            double b = (double)(k - 1) / N;
            return a - b;
        }
        private double SetRight(double p, int N, int k, int v1, int v2)
        {
            double a = ((N - k) * p * p) / (N * (1 - p * p) * Quantile.Fisher(1 - this.Alpha, v1, v2));
            double b = (double)(k - 1) / N;
            return a - b;
        }

        public void RangCorrelation(Data[] DataArray)
        {
            int N = DataArray.Length;
            double[] X = new double[N];
            double[] Y = new double[N];

            for (int i = 0; i < N; i++)
            {
                X[i] = DataArray[i].XValue;
                Y[i] = DataArray[i].YValue;
            }
            Array.Sort(X);
            Array.Sort(Y);

            RangData[] XRanged = SetRanges(X);
            RangData[] YRanged = SetRanges(Y);

            Data[] Rangs = new Data[N];

            for (int i = 0; i < N; i++)
            {
                Rangs[i].XValue = XRanged[i].Rang;
                int index = 0;
                for (int j = 0; j < N; j++)
                {
                    if (DataArray[j].XValue == XRanged[i].Value)
                    {
                        index = j;
                        break;
                    }
                }
                for (int j = 0; j < N; j++)
                {
                    if (YRanged[j].Value == DataArray[index].YValue)
                    {
                        Rangs[i].YValue = YRanged[j].Rang;
                        break;
                    }
                }
            }

            findSpirman();
            findKendall();

            if(Values.Keys.Contains("Spirman") && Values.Keys.Contains("Kendall"))
            if (Math.Round(Values["Spirman"].Data, 2) >= 1 && Values["Kendall"].Data < 0.6)
            {
                double t = 1.437 * Values["Kendall"].Data;
                double Sigma = Math.Sqrt((1 - t * t) / (N - 2));
                double tBot = t - Quantile.Student(1 - this.Alpha / 2, N - 2) * Sigma;
                double tTop = t + Quantile.Student(1 - this.Alpha / 2, N - 2) * Sigma;
                Values.Remove("Spirman");
                Values.Add("Spirman", new AdvancedEvaluation("Спірмана", t, tBot, tTop));
            }

            RangData[] SetRanges(double[] Array)
            {
                RangData[] Ranges = new RangData[N];
                int j = 0;
                //int k = 0;
                for (int i = 0; i < N;)
                {
                    int k = i + 1;
                    for (j = i; j < N; j++)
                    {
                        if (Array[i] != Array[j])
                            //k++;
                            //else 
                            break;
                    }
                    //int df = j - i;
                    //j--;
                    while (i < j)
                    {
                        Ranges[i].Value = Array[i];
                        Ranges[i].Rang = (double)(j + k) / 2;
                        i++;
                    }
                }
                return Ranges;
            }

            void findSpirman()
            {
                double sum = 0;
                for (int i = 0; i < N; i++)
                    sum += Math.Pow(XRanged[i].Rang - YRanged[i].Rang, 2);

                //int z = findConnections(X);

                //int[] aArray = CountConnections(X, z);

                //double A = SumConnections(aArray);

                //z = findConnections(Y);

                //aArray = CountConnections(Y, z);

                //double B = SumConnections(aArray);

                ////double t = 1 - (6 * sum) / (N * (N * N - 1));

                //double Nt = (double)N * (N * N - 1) / 6;

                //double t = (Nt - sum - A - B) / Math.Sqrt((Nt - 2 * A) * (Nt - 2 * B));
                double t = 1 - 6d / (N * (N * N - 1) * sum);


                double Sigma = Math.Sqrt((1 - t * t) / (N - 2));
                double tBot = t - Quantile.Student(1 - this.Alpha / 2, N - 2) * Sigma;
                double tTop = t + Quantile.Student(1 - this.Alpha / 2, N - 2) * Sigma;
                if(t <= 1 && t >= -1)
                    Values.Add("Spirman", new AdvancedEvaluation("Спірмана", t, tBot, tTop));
            }

            void findKendall()
            {
                double sum = 0;
                double[] v = new double[N - 1];
                for (int l = 0; l < N - 1; l++)
                {
                    v[l] = 0;
                    for (int j = l + 1; j < N; j++)
                        //sum += (Rangs[l].YValue < Rangs[j].YValue) ? 1 : -1;
                        v[l] += (Rangs[l].YValue < Rangs[j].YValue) ? 1 : -1;
                    sum += v[l]; ;
                }

                double t = 2 * sum / (N * (N - 1));
                double Sigma = Math.Sqrt((4 * N + 10) / (9 * (N * N - 1)));

                double tBot = t - Quantile.Normal(1 - this.Alpha / 2) * Sigma;
                double tTop = t + Quantile.Normal(1 - this.Alpha / 2) * Sigma;
                if (t <= 1 && t >= -1)
                    this.Values.Add("Kendall", new AdvancedEvaluation("Кендалла", t, tBot, tTop));
            }

            //int[] CountConnections(double[] Array, int Size)
            //{
            //    int[] A = new int[Size];
            //    for (int i = 0, j = 1; i < Size; i++, j++)
            //    {
            //        A[i] = 0;
            //        //bool IsConnected = false;
            //        bool Bunch = false;
            //        for (; j < Array.Length; j++)
            //        {
            //            if (Array[j] == Array[j - 1])
            //            {
            //                Bunch = true;
            //                A[i]++;
            //            }
            //            else
            //            {
            //                if (!Bunch)
            //                    continue;
            //                else break;
            //            }
            //        }
            //    }
            //    return A;
            //}
            //int findConnections(double[] Arr)
            //{
            //    int z = 0;
            //    bool Same = false;
            //    for (int i = 0; i < Arr.Length - 1; i++)
            //        if (Arr[i] == Arr[i + 1])
            //            if (!Same)
            //            {
            //                z++;
            //                Same = true;
            //            }
            //            else Same = false;

            //    return z;
            //}

            //double Coefficient(double[] Arr, int Z)
            //{
            //    double A = 0;
            //    int j = 1;
            //    for (int i = 0; i < Z; i++)
            //    {
            //        int S = 1;
            //        for (; j < Arr.Length; j++)
            //        {
            //            if (Arr[j] == Arr[j - 1])
            //            {
            //                S++;
            //            }
            //            else
            //                break;
            //        }
            //        A += (S * S * S - S);
            //    }
            //    return A / 12;
            //}

            //double SumConnections(int[] Array)
            //{
            //    double A = 0;
            //    for (int i = 0; i < Array.Length; i++)
            //        A += Math.Pow(Array[i], 3) - Array[i];
            //    A /= 12;

            //    return A;
            //}
        }

        //public void LinearRegression(Data[] DataArray)
        //{

        //    int N = DataArray.Length;

        //    //double S = 0;
        //    //for (int i = 0; i < N; i++)
        //    //    S += Math.Pow(DataArray[i].YValue - Values["Regression(a)"].Data - Values["Regression(b)"].Data * DataArray[i].XValue, 2);
        //    //S /= (N - 2);
        //    //Values.Add("ResidualDisperssion", new Evaluation("S^2(зал.)", S));

        //    //double Determination = Math.Pow(Values["CorrelationCoefficient"].Data, 2) * 100;
        //    //Values.Add("R^2", new Evaluation("Коефіцієнт детермінації", Determination));

        //    //double Sa = Math.Sqrt(S) * Math.Sqrt(1d / N + Math.Pow(Values["MathExpectation(x)"].Data, 2) / (Math.Pow(Values["Sigma(x)"].Data, 2) * (N - 1)));
        //    //Values.Add("Regression(Sa)", new Evaluation("S(a)", Sa));

        //    //double Sb = Math.Sqrt(S) / (Math.Pow(Values["Sigma(x)"].Data, 2) * (N - 1));
        //    //Values.Add("Regression(Sb)", new Evaluation("S(b)", Sb));

        //    double q = Quantile.Student(1 - Alpha / 2, N - 2);
        //    double a = Values["Regression(a)"].Data;
        //    Values.Remove("Regression(a)");
        //    Values.Add("Regression(a)", new Evaluation("a", a, a - q * Values["Regression(Sa)"].Data, a + q * Values["Regression(Sa)"].Data));

        //    double b = Values["Regression(b)"].Data;
        //    Values.Remove("Regression(b)");
        //    Values.Add("Regression(b)", new Evaluation("b", b, b - q * Values["Regression(Sb)"].Data, b + q * Values["Regression(Sb)"].Data));
        //}

        //public void NonLinearRegression()
        //{

        //}

        public void SetTableCoefficients(BinaryTabelsCombinations Table)
        {
            double T = Table.Fehner();
            if (T <= 1 && T >= -1)
                Values.Add("Fehner-index", new Evaluation("I",T));
            T = Table.Phi();
            if (T <= 1 && T >= -1)
                Values.Add("Phi-index", new Evaluation("Φ", T));
            T = Table.YollQ();
            if (T <= 1 && T >= -1)
                Values.Add("Yoll-Q-index", new Evaluation("Q", Table.YollQ()));
            T = Table.YollY();
            if (T <= 1 && T >= -1)
                Values.Add("Yoll-Y-index", new Evaluation("Y", Table.YollY()));

        }
        public void SetTableCoefficients(TablesCombinations Table)
        {
            Values.Add("Pirson(table)", new Evaluation("К.С. Пірсона", Table.Pirson(Alpha)));
            if (Table.IsConnected(Alpha))
                if(Table.IsSquare())
                    Values.Add("Kendall(table)", new Evaluation("К.С. Кендалла", Table.Kendall()));
                else
                    Values.Add("Stuard(table)", new Evaluation("К.С. Стюарда", Table.Kendall()));
        }

    }
}
