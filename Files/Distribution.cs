﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;
using System.IO;
using Algebra;

namespace Statistics
{
    public static class Distribution
    {
        static private double Cov(double H1x1, double H1x2, double H2x1, double H2x2, double D1, double D2, double cov)
        {
            double C = 0;

            C = H1x1 * H2x1 * D1 + H1x2 * H2x2 * D2 + cov * (H1x1 * H2x2 + H1x2 * H2x1);
            return C;
        }

        static private double D(double d1, double dF1, double d2, double dF2, double cov)
        {
            return Math.Pow(dF1, 2) * d1 + Math.Pow(dF2, 2) * d2 + 2 * dF1 * dF2 * cov;
        }

        public static class MultiDimensionalNormalDistribution
        {
            public static double f(MultiDimensionalVector Value, MultiDimensionalVector MathExpectation, Matrix DC)
            {
                MultiDimensionalVector Difference = Value - MathExpectation;
                return (1d / (Math.Sqrt(DC.GetDeterminant()) * Math.Pow(2 * Math.PI, (double)Value.Dimensionality / 2))) * Math.Exp((double)(-0.5 * Difference * Matrix.GetReverse(DC) * Difference));
            }
            public static MultiDimensionalSample Model(MultiDimensionalVector MathExpectations, Matrix A, int Size, string Name)
            {
                Random Rand = new Random();
                MultiDimensionalVector U = new MultiDimensionalVector(MathExpectations.Dimensionality);
                MultiDimensionalVector[] Output = new MultiDimensionalVector[Size];
                for (int i = 0, j; i < Size; i++)
                {
                    for (j = 0; j < U.Dimensionality; j++)
                        U[j] = Normal.Reverse(ref Rand);
                    Output[i] = MathExpectations + (A * U);
                    for (j = 0; j < U.Dimensionality; j++)
                        if (Output[i][j] != Output[i][j])
                            throw new Exception("Value was NaN.");
                }
                return new MultiDimensionalSample(Output, Name);
            }
        }
        public static class TwoDimNormalDistribution//: Distributions
        {
            public static double f(double x, double y, TwoDimensionalStatistics S)
            {
                double R = 1 - Math.Pow(S["CorrelationCoefficient"].Data, 2);

                double a = 1.0 / (2 * Math.PI * S["Sigma(x)"].Data * S["Sigma(y)"].Data * Math.Sqrt(R));
                if (a < 0.000001)
                    a = 0;

                double b = -1.0 / (2 * R);

                double x1 = (x - S["MathExpectation(x)"].Data) / S["Sigma(x)"].Data;

                double y1 = (y - S["MathExpectation(y)"].Data) / S["Sigma(y)"].Data;

                double xy = 2 * S["CorrelationCoefficient"].Data * x1 * y1;

                return a * Math.Exp(a * (Math.Pow(x1, 2) - xy + Math.Pow(y1, 2)));
            }

            static public MultiDimensionalSample Model(string Name, int Size, double m1, double m2, double sX, double sY, double r)
            {
                Random Rand = new Random();

                double[] ValuesX = new double[Size];
                double[] ValuesY = new double[Size];
                double X;
                double Y;
                double n;
                for (int i = 0; i < Size; i++)
                {
                    X = Normal.Reverse(m1, sX, ref Rand);
                    n = Normal.Reverse(0, 1, ref Rand);
                    Y = m2 + sY * (z2() * Math.Sqrt(1 - r * r) + r * z1());
                    ValuesX[i] = X;
                    ValuesY[i] = Y;
                }
                return new MultiDimensionalSample(Name + ".txt", new Sample(ValuesX, Name + "_0"), new Sample(ValuesY, Name + "_1"));
                double z1() => (X - m1) / sX;
                double z2() => (n - r * z1());
            }
        }

        public static class Exponencial
            {
            static public Series BuildDensityFunction(double[] variables, Statistics St, double q)
            {
                Series s = new ModelHistogramSeries();

                int N = variables.Length;
                double[] Probability = new double[N];
                double L = 1 / St["MathExpectation"].Data;

                    double sum = 0;
                    for (int i = 0; i < N; i++)
                    {
                        if (variables[i] <= 0)
                            Probability[i] = 0;
                        else
                        {
                            Probability[i] = L * Math.Exp(-1 * L * variables[i]);
                            sum += Probability[i];
                        }
                    }
                    if(L > 1)
                        for (int i = 0; i < N; i++)
                            Probability[i] *= (q / sum);

                    if (Probability[0] > 1)
                    {
                        double k = Probability[0];
                        for (int i = 0; i < N; i++)
                        {
                            Probability[i] /= k;
                        }
                    }
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series BuildDistributionFunction(double[] variables, Statistics St)
                {
                    Series s = new ModelDistributionSeries();

                    double L = 1 / St["MathExpectation"].Data;

                    double[] Probability = Distribution(variables, L);
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series Top(double[] variables, Statistics St, double Alpha)
                {
                    Series s = new TopDistributionSeries();

                    int N = variables.Length;
                    double L = 1 / St["MathExpectation"].Data;

                    double[] Probability = Distribution(variables, L);
                    double a = 1 - Alpha / 2;
                    for (int i = 0; i < N; i++)
                    {
                        Probability[i] += Math.Sqrt(D(variables[i], L, N)) * Quantile.Normal(a);
                    }
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series Bottom(double[] variables, Statistics St, double Alpha)
                {
                    Series s = new BottomDistributionSeries();

                    int N = variables.Length;
                    double L = 1 / St["MathExpectation"].Data;

                    double[] Probability = Distribution(variables, L);
                    double a = 1 - Alpha / 2;
                    for (int i = 0; i < N; i++)
                    {
                        Probability[i] -= Math.Sqrt(D(variables[i], L, N)) * Quantile.Normal(a);
                    }
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }
            static public Sample Model(string Name, double L, int Size)
            {
                double[] x = new double[Size];

                Random rand = new Random();
                for (int i = 0; i < Size; i++)
                    x[i] = Math.Log((1 - rand.NextDouble())) / (-1 * L);

                return new Sample(x, Name);
            }
            static public double[] Generate(string Name, int Size, double L)
                {
                    double[] x = new double[Size];
                    StreamWriter St = new StreamWriter(Application.StartupPath + "\\Tests\\" + Name);
                    using (St)
                    {
                        Random rand = new Random();
                        for (int i = 0; i < Size; i++)
                        {
                            x[i] = Math.Log((1 - rand.NextDouble())) /(-1 * L);
                            St.WriteLine(Math.Round(x[i], 4).ToString());
                        }
                    }
                    return x;
                }

                static public double[] Distribution(double[] val, double L)
                {
                    int N = val.Length;
                    double[] Distribution = new double[N];
                    for (int i = 0; i < N; i++)
                    {
                        if (val[i] <= 0)
                            Distribution[i] = 0;
                        else
                        {
                            Distribution[i] = 1 - Math.Exp(-1 * L * val[i]);
                        }
                    }

                    if (Distribution[N - 1] > 1 || Distribution[N - 1] < 1)
                    {
                        double K = Distribution[N - 1];
                        for (int i = 0; i < N; i++)
                            Distribution[i] /= K;
                    }
                            return Distribution;
                }
                static public double Distribution(double val, double L)
                {
                    double D;
                    if (val <= 0)
                        D = 0;
                    else
                        D = 1 - Math.Exp(-1 * L * val);
                    return D;
                } 

                static private double D(double x, double L, double N)
                {
                    return Math.Pow(x, 2) * Math.Exp(-2 * L * x) * Math.Pow(L, 2) / N;
                }
            }

            public static class Normal
            {
                static public Series DensityFunction(double[] variables, Statistics St, double q)
                {
                    Series s = new ModelHistogramSeries();

                    int N = variables.Length;
                    double[] Probability = new double[N];
                    double m = St["MathExpectation"].Data;
                    double Sigma = St["Sigma"].Data;

                    double sum = 0;
                    for (int i = 0; i < N; i++)
                    {
                        Probability[i] = (1 / (Sigma * Math.Sqrt(Math.PI * 2)))
                            * Math.Exp(-1 * Math.Pow((variables[i] - m), 2) / (2 * Math.Pow(Sigma, 2)));
                        sum += Probability[i];
                    }

                    for (int i = 0; i < N; i++)
                        Probability[i] *= (q / sum);
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series DistributionFunction(double[] variables, Statistics St)
                {
                    Series s = new ModelDistributionSeries();

                    double m = St["MathExpectation"].Data;
                    double Sigma = St["Sigma"].Data;

                    double[] Probability = Distribution(variables, m, Sigma);
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series Top(double[] variables, Statistics St, double Alpha)
                {
                    Series s = new TopDistributionSeries();

                    double N = variables.Length;
                    double m = St["MathExpectation"].Data;
                    double Sigma = St["Sigma"].Data;
                    double SigmaS = 0;
                    for (int i = 0; i < N; i++)
                    {
                        SigmaS += Math.Pow(variables[i], 2);
                    }
                    SigmaS /= N;
                    SigmaS = N * Math.Sqrt(SigmaS - Math.Pow(m, 2)) / (N - 1);

                    double[] Probability = Distribution(variables, m, Sigma);
                    double a = 1 - Alpha / 2;
                    double Dm = Math.Pow(SigmaS, 2) / N;
                    double cov = 0;
                    double dSigma = Math.Pow(SigmaS, 2) / (2 * N); 
                    for (int i = 0; i < N; i++)
                    {
                        double dFm = -1 * (1 / (Sigma * Math.Sqrt(Math.PI * 2)))
                            * Math.Exp(-1 * Math.Pow((variables[i] - m), 2) / (2 * Math.Pow(Sigma, 2)));
                        double dFs = -1 * ((variables[i] - m) / (Math.Pow(Sigma, 2) * Math.Sqrt(Math.PI * 2)))
                            * Math.Exp(-1 * Math.Pow((variables[i] - m), 2) / (2 * Math.Pow(Sigma, 2)));

                        Probability[i] += Math.Sqrt(D(Dm, dFm, dSigma, dFs, cov)) * Quantile.Normal(a);

                        if (Probability[i] > 1)
                            Probability[i] = 1;
                        if (Probability[i] < 0)
                            Probability[i] = 0;
                    }
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series Bottom(double[] variables, Statistics St, double Alpha)
                {
                    Series s = new BottomDistributionSeries();

                    double N = variables.Length;
                    double m = St["MathExpectation"].Data;
                    double Sigma = St["Sigma"].Data;
                    double SigmaS = 0;
                    for (int i = 0; i < N; i++)
                    {
                        SigmaS += Math.Pow(variables[i], 2);
                    }
                    SigmaS /= N;
                    SigmaS = N * Math.Sqrt(SigmaS - Math.Pow(m, 2)) / (N - 1);

                    double[] Probability = Distribution(variables, m, Sigma);
                    double a = 1 - Alpha / 2;
                    double Dm = Math.Pow(SigmaS, 2) / N;
                    double cov = 0;
                    double dSigma = Math.Pow(SigmaS, 2) / (2 * N);
                    for (int i = 0; i < N; i++)
                    {
                        double dFm = -1 * (1 / (Sigma * Math.Sqrt(Math.PI * 2)))
                            * Math.Exp(-1 * Math.Pow((variables[i] - m), 2) / (2 * Math.Pow(Sigma, 2)));
                        double dFs = -1 * ((variables[i] - m) / (Math.Pow(Sigma, 2) * Math.Sqrt(Math.PI * 2)))
                            * Math.Exp(-1 * Math.Pow((variables[i] - m), 2) / (2 * Math.Pow(Sigma, 2)));

                        Probability[i] -= Math.Sqrt(D(Dm, dFm, dSigma, dFs, cov)) * Quantile.Normal(a);
                        
                        if (Probability[i] > 1)
                            Probability[i] = 1;
                        if (Probability[i] < 0)
                            Probability[i] = 0;
                    }
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }
            static public Sample Model(string Name, double m, double Sigma, int Size)
            {
                double[] x = new double[Size];

                Random rand = new Random();
                for (int i = 0; i < Size; i++)
                    x[i] = Reverse(m, Sigma, ref rand);

                return new Sample(x, Name);
            }
            static public double[] Generate(string Name, double m, double Sigma, int Size)
                {
                    double[] x = new double[Size];
                    StreamWriter St = new StreamWriter(Application.StartupPath + "\\Tests\\" + Name);
                    using (St)
                    {
                        Random rand = new Random();
                        for (int i = 0; i < Size; i++)
                        {
                        Reverse(m, Sigma, ref rand);
                            St.WriteLine(x[i].ToString());
                        }
                    }
                    return x;
                }
            static public double Reverse(double m, double S, ref Random R) => Reverse(ref R) * S + m;
            static public double Reverse(ref Random R)
            {
                double n = 6;
                double X = 0;
                for (int i = 0; i < n; i++)
                    X += R.NextDouble();
                return (X - n / 2) / Math.Sqrt(n / 12);
            }
                static public double[] Distribution(double[] val, double m, double Sigma)
                {
                    int N = val.Length;
                    double[] Distribution = new double[N];
                    for (int i = 0; i < N; i++)
                    {
                        double U = (val[i] - m) / Sigma;

                        if (U >= 0)
                        {
                            Distribution[i] = F(U);
                        }
                        else
                        {
                            Distribution[i] = 1 - F(Math.Abs(U));
                        }
                    }

                    if (Distribution[N - 1] > 1 || Distribution[N - 1] < 1)
                    {
                        double K = Distribution[N - 1];
                        for (int i = 0; i < N; i++)
                            Distribution[i] /= K;
                    }

                    return Distribution;
                }

                static public double Distribution(double val, double m, double Sigma)
                {
                    double D;
                    double U = (val - m) / Sigma;
                    if (U >= 0)
                    {
                        D = F(U);
                    }
                    else
                    {
                        D = 1 - F(Math.Abs(U));
                    }
                    return D;
                }

                static public double F(double u)
                {
                    double Ro = 0.2316419;

                    double b1 = 0.31938153;
                    double b2 = -0.356563782;
                    double b3 = 1.781477937;
                    double b4 = -1.821255978;
                    double b5 = 1.330274429;

                    double t = 1 / (1 + Ro * u);

                    double f = 1 - (1 / (Math.Sqrt(2 * Math.PI)))* 
                        Math.Exp(-1 * Math.Pow(u, 2) / 2) *
                        (b1 * t + b2 * Math.Pow(t, 2) + b3 * Math.Pow(t, 3) + b4 * Math.Pow(t, 4) + b5 * Math.Pow(t, 5)) + 0.000000078;
                    return f;
                }
            }

            public static class Equaliste
            {
                static public Series DensityFunction(double[] variables, double q)
                {
                    Series s = new ModelHistogramSeries();

                    int N = variables.Length;
                    double[] Probability = new double[N];

                    double sum = 0;
                    for (int i = 0; i < N; i++)
                    {
                        Probability[i] = 1 / (variables.Max() - variables.Min());
                        sum += Probability[i];
                    }

                    for (int i = 0; i < N; i++)
                        Probability[i] *= (q / sum);

                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series DistributionFunction(double[] variables)
                {
                    Series s = new ModelDistributionSeries();

                    int N = variables.Length;
                    double[] Probability = Distribution(variables);

                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series Top(double[] variables, Statistics St, double Alpha)
                {
                    Series s = new TopDistributionSeries();
                    double alpha = 1 - Alpha / 2;

                    double N = variables.Length;
                    double m = St["MathExpectation"].Data;
                    double mS = 0;
                    for (int i = 0; i < N; i++)
                    {
                        mS += Math.Pow(variables[i], 2);
                    }
                    mS /= N;

                    double[] Probability = Distribution(variables);

                    double a = m - Math.Sqrt(3 * (mS - Math.Pow(m, 2)));
                    double b = m + Math.Sqrt(3 * (mS - Math.Pow(m, 2)));
                    double covX = (a + b) * Math.Pow(b - a, 2) / (12 * N);
                    double H1m = 1 + 3 * (a + b) / (b - a);
                    double H2m = 1 - 3 * (a + b) / (b - a);
                    double H1mS = -3 / (b - a);
                    double H2mS = 3 / (b - a);
                    double Dm = Math.Pow(b - a, 2) / (12 * N);
                    double DmS = (Math.Pow(b - a, 4) + 15 * Math.Pow(a + b, 2) * Math.Pow(b - a, 2)) / (180 * N);
                    double dA = D(Dm, H1m, DmS, H1mS, covX);
                    double dB = D(Dm, H2m, DmS, H2mS, covX);
                    double cov = Cov(H1m, H1mS, H2m, H2mS, Dm, DmS, covX);
                    for (int i = 0; i < N; i++)
                    {
                        Probability[i] += Math.Sqrt(Dispersion(variables[i], a, b, dA, dB, cov)) * Quantile.Normal(alpha);

                        if (Probability[i] > 1)
                            Probability[i] = 1;
                        if (Probability[i] < 0)
                            Probability[i] = 0;
                    }
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series Bottom(double[] variables, Statistics St, double Alpha)
                {
                    Series s = new BottomDistributionSeries();
                    double alpha = 1 - Alpha / 2;

                    double N = variables.Length;
                    double m = St["MathExpectation"].Data;
                    double mS = 0;
                    for (int i = 0; i < N; i++)
                    {
                        mS += Math.Pow(variables[i], 2);
                    }
                    mS /= N;

                    double[] Probability = Distribution(variables);

                    double a = m - Math.Sqrt(3 * (mS - Math.Pow(m, 2)));
                    double b = m + Math.Sqrt(3 * (mS - Math.Pow(m, 2)));
                    double covX = (a + b) * Math.Pow(b - a, 2) / (12 * N);
                    double H1m = 1 + 3 * (a + b) / (b - a);
                    double H2m = 1 - 3 * (a + b) / (b - a);
                    double H1mS = -3 / (b - a);
                    double H2mS = 3 / (b - a);
                    double Dm = Math.Pow(b - a, 2) / (12 * N);
                    double DmS = (Math.Pow(b - a, 4) + 15 * Math.Pow(a + b, 2) * Math.Pow(b - a, 2)) / (180 * N);
                    double dA = D(Dm, H1m, DmS, H1mS, covX);
                    double dB = D(Dm, H2m, DmS, H2mS, covX);
                    double cov = Cov(H1m, H1mS, H2m, H2mS, Dm, DmS, covX);
                    for (int i = 0; i < N; i++)
                    {
                        Probability[i] -= Math.Sqrt(Dispersion(variables[i], a, b, dA, dB, cov)) * Quantile.Normal(alpha);

                        if (Probability[i] > 1)
                            Probability[i] = 1;
                        if (Probability[i] < 0)
                            Probability[i] = 0;
                    }
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

            static public Sample Model(string Name, double A, double B, int Size)
            {
                double[] x = new double[Size];

                Random rand = new Random();
                for (int i = 0; i < Size; i++)
                    x[i] = rand.NextDouble() * (B - A) + A;

                return new Sample(x, Name);
            }
            static public double[] Generate(string Name, double A, double B, int Size)
                {
                    double[] x = new double[Size];
                    StreamWriter St = new StreamWriter(Application.StartupPath + "\\Tests\\" + Name);
                    using (St)
                    {
                        Random rand = new Random();
                        for (int i = 0; i < Size; i++)
                        {
                            x[i] = rand.NextDouble() * (B - A) + A;
                            St.WriteLine(Math.Round(x[i], 4).ToString());
                        }
                    }
                    return x;
                }

                static public double[] Distribution(double[]val)
                {
                    int N = val.Length;
                    double[] Distribution = new double[N];
                    for (int i = 0; i < N; i++)
                    {
                        Distribution[i] = (val[i] - val.Min()) / (val.Max() - val.Min());
                    }

                    if (Distribution[N - 1] > 1 || Distribution[N - 1] < 1)
                    {
                        double K = Distribution[N - 1];
                        for (int i = 0; i < N; i++)
                            Distribution[i] /= K;
                    }

                    return Distribution;
                }

                static public double Distribution(double val, double Min, double Max)
                {
                    double D = (val - Min) / (Max - Min);
                    return D;
                }

                static double Dispersion(double x, double a, double b, double Da, double Db, double cov)
                {
                    double d1 = Da * Math.Pow(x - b, 2) / Math.Pow(b - a, 4);
                    double d2 = Db * Math.Pow(x - a, 2) / Math.Pow(b - a, 4);
                    double d3 = 2 * cov * (x - a) * (x - b) / Math.Pow(b - a, 4);
                    double d = d1 + d2 - d3;
                    return d;
                }

            }

            public static class Weibull
            {

                static public double[] SetAlphaAndBeta(double[] values, double[] Probability)
                {
                    double a11 = values.Length - 1;

                    double a12 = 0;

                    for (int i = 0; i < values.Length - 1; i++)
                        a12 += Math.Log(values[i]);

                    double a21 = a12;

                    double a22 = 0;

                    for (int i = 0; i < values.Length - 1; i++)
                        a22 += Math.Pow(Math.Log(values[i]), 2);

                    Func<double, double> LnPart = (F1NVAlue) =>
                    {
                        if (F1NVAlue == 1)
                            return 0;

                        double Ln = Math.Log(1d / (1 - F1NVAlue));

                        return Math.Log(Ln);
                    };

                    double b1 = 0,
                           b2 = 0;

                    for (int i = 0; i < values.Length - 1; i++)
                    {
                        double F1N = Probability[i];

                        b1 += LnPart(F1N);

                        b2 += Math.Log(values[i]) * LnPart(F1N);
                    }

                    double delta = a11 * a22 - a12 * a21;

                    double deltaA = b1 * a22 - a12 * b2;

                    double deltaBeta = a11 * b2 - b1 * a21;

                    double alpha = deltaA / delta;

                    alpha = Math.Exp(-alpha);

                    double beta = deltaBeta / delta;

                    return new double[] {alpha, beta};

                }
                static public Series DensityFunction(double[] variables, double alpha, double beta, double q)
                {
                    Series s = new ModelHistogramSeries();

                    int N = variables.Length;
                    double[] Probability = new double[N];

                    double sum = 0;

                    for (int i = 0; i < N; i++)
                    {
                        if (variables[i] <= 0)
                            Probability[i] = 0;
                        else
                        {
                            Probability[i] = (beta / alpha) * Math.Pow(variables[i], beta - 1) 
                                * Math.Exp(-Math.Pow(variables[i], beta) / alpha);
                            sum += Probability[i];
                        }
                    }

                    for (int i = 0; i < N; i++)
                        Probability[i] *= (q / sum);

                    if (Probability[0] > 1)
                    {
                        double k = Probability[0];
                        for (int i = 0; i < N; i++)
                        {
                            Probability[i] /= k;
                        }
                    }

                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series DistributionFunction(double[] variables, double alpha, double beta)
                {
                    Series s = new ModelDistributionSeries();

                    double[] Probability = Distribution(variables, alpha, beta);
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                //static public Series Top(double[] variables, double[] RelativeFr, double Alpha)
                //{
                //    Series s = new TopSeries();

                //    int N = variables.Length;

                //    double[] Probability = Distribution(variables, L);
                //    double a = 1 - Alpha / 2;
                //    for (int i = 0; i < N; i++)
                //    {
                //        Probability[i] += Math.Sqrt(D(variables[i], L, N)) * Quantile.Normal(a);
                //    }
                //    s.Points.DataBindXY(variables, Probability);
                //    return s;
                //}

                //static public Series Bottom(double[] variables, double[] RelativeFr, double Alpha)
                //{
                //    Series s = new BottomSeries();

                //    int N = variables.Length;

                //    double[] Probability = Distribution(variables, RelativeFr);
                //    double a = 1 - Alpha / 2;
                //    for (int i = 0; i < N; i++)
                //    {
                //        Probability[i] -= Math.Sqrt(D(variables[i], L, N)) * Quantile.Normal(a);
                //    }
                //    s.Points.DataBindXY(variables, Probability);
                //    return s;
                //}

                //static public double[] Generate(string Name, int Size, double L)
                //{
                //    double[] x = new double[Size];
                //    StreamWriter St = new StreamWriter(Application.StartupPath + "\\Tests\\" + Name);
                //    using (St)
                //    {
                //        Random rand = new Random();
                //        for (int i = 0; i < Size; i++)
                //        {
                //            x[i] = Math.Log((1 - rand.NextDouble())) / (-1 * L);
                //            St.WriteLine(Math.Round(x[i], 4).ToString());
                //        }
                //    }
                //    return x;
                //}

                static public double[] Distribution(double[] val, double alpha, double beta)
                {

                    int N = val.Length;
                    double[] Distribution = new double[N];
                    for (int i = 0; i < N; i++)
                    {
                        if (val[i] <= 0)
                            Distribution[i] = 0;
                        else
                        {
                            Distribution[i] = 1 - Math.Exp(-1 * Math.Pow(val[i], beta) / alpha);
                        }
                    }

                    if (Distribution[N - 1] > 1 || Distribution[N - 1] < 1)
                    {
                        double K = Distribution[N - 1];
                        for (int i = 0; i < N; i++)
                            Distribution[i] /= K;
                    }

                    return Distribution;
                }
                static public double Distribution(double val, double alpha, double beta)
                {
                    double D;
                    if (val <= 0)
                        D = 0;
                    else
                        D = 1 - Math.Exp(-1 * Math.Pow(val, beta) / alpha);
                    return D;
                }

                //static private double D(double x, double L, double N)
                //{
                //    double DispZal = 0;
                //    return D;
                //}
            }

            public static class Arcsinus
            {
                static public Series DensityFunction(double[] variables, Statistics St, double q)
                {
                    Series s = new ModelHistogramSeries();

                    int N = variables.Length;
                    double[] Probability = new double[N];
                    double a = Math.Sqrt(2 * St["Dispersion"].Data);

                    double sum = 0;

                    for (int i = 0; i < N; i++)
                    {
                        if (variables[i] < -a || variables[i] > a)
                            Probability[i] = 0;
                        else
                        {
                            Probability[i] = 1 / (Math.PI * Math.Sqrt(Math.Pow(a, 2) - Math.Pow(variables[i], 2)));
                            sum += Probability[i];
                        } 
                    }
                    for (int i = 0; i < N; i++)
                    {
                        Probability[i] *= (q / sum);
                        if (Probability[i] > 1)
                            Probability[i] = 1;
                    }

                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series DistributionFunction(double[] variables, Statistics St)
                {
                    Series s = new ModelDistributionSeries();

                    double a = Math.Sqrt(2 * St["Dispersion"].Data);

                    double[] Probability = Distribution(variables, a);
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series Top(double[] variables, Statistics St, double Alpha)
                {
                    Series s = new TopDistributionSeries();

                    int N = variables.Length;
                    double a = Math.Sqrt(2 * St["Dispersion"].Data);

                    double[] Probability = Distribution(variables, a);
                    double m = St["MathExpectation"].Data;
                    double mS = 0;
                    for (int i = 0; i < N; i++)
                    {
                        mS += Math.Pow(variables[i], 2);
                    }
                    mS /= N;

                    //double A = Math.Sqrt(2) * Math.Sqrt(mS - Math.Pow(m, 2));

                    //double dA = Math.Pow(A, 4) / (8 * N);
                    double dA = St["Dispersion"].Data;

                    Alpha = 1 - Alpha / 2;
                    for (int i = 0; i < N; i++)
                    {
                        //Probability[i] += Math.Sqrt(D(variables[i], A, dA)) * Quantile.Normal(Alpha);
                        Probability[i] += Math.Sqrt(D(variables[i], a, dA)) * Quantile.Normal(Alpha);

                        if (Probability[i] > 1)
                            Probability[i] = 1;
                        else if (Probability[i] < 0)
                            Probability[i] = 0;
                    }
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public Series Bottom(double[] variables, Statistics St, double Alpha)
                {
                    Series s = new BottomDistributionSeries();

                    int N = variables.Length;
                    double a = Math.Sqrt(2 * St["Dispersion"].Data);

                    double[] Probability = Distribution(variables, a);
                    double m = St["MathExpectation"].Data;

                    double mS = 0;
                    for (int i = 0; i < N; i++)
                    {
                        mS += Math.Pow(variables[i], 2);
                    }
                    mS /= N;

                    //double A = Math.Sqrt(2) * Math.Sqrt(mS - Math.Pow(m, 2));

                    //double dA =  Math.Pow(A, 4) / (8 * N);
                    double dA = St["Dispersion"].Data;

                    Alpha = 1 - Alpha / 2;
                    for (int i = 0; i < N; i++)
                    {
                        //Probability[i] -= Math.Sqrt(D(variables[i], A, dA)) * Quantile.Normal(Alpha);
                        Probability[i] -= Math.Sqrt(D(variables[i], a, dA)) * Quantile.Normal(Alpha);

                        if (Probability[i] > 1)
                            Probability[i] = 1;
                        else if (Probability[i] < 0)
                            Probability[i] = 0;
                    }
                    s.Points.DataBindXY(variables, Probability);
                    return s;
                }

                static public double[] Generate(string Name, int Size, double L)
                {
                    double[] x = new double[Size];
                    StreamWriter St = new StreamWriter(Application.StartupPath + "\\Tests\\" + Name);
                    using (St)
                    {
                        Random rand = new Random();
                        for (int i = 0; i < Size; i++)
                        {
                            x[i] = Math.Log((1 - rand.NextDouble())) / (-1 * L);
                            St.WriteLine(Math.Round(x[i], 4).ToString());
                        }
                    }
                    return x;
                }

                static public double[] Distribution(double[] val, double a)
                {
                    int N = val.Length;
                    double[] Distribution = new double[N];
                    for (int i = 0; i < N; i++)
                    {
                        if (val[i] < -a)
                            Distribution[i] = 0;
                        else if (val[i] > a)
                            Distribution[i] = 1;
                        else
                         Distribution[i] = 0.5 + Math.Asin(val[i] / a) / Math.PI;
                    }

                    if (Distribution[N - 1] > 1 || Distribution[N - 1] < 1)
                    {
                        double K = Distribution[N - 1];
                        for (int i = 0; i < N; i++)
                            Distribution[i] /= K;
                    }
                    return Distribution;
                }
                static public double Distribution(double val, double a)
                {
                    double D;
                    if (val < -a)
                        D = 0;
                    else if (val > a)
                        D = 1;
                    else
                        D = 0.5 + Math.Asin(val / a) / Math.PI;
                    return D;
                }

                static private double D(double val, double A, double dA)
                {  
                    double D = Math.Pow((-val / (Math.PI * A * Math.Sqrt(Math.Pow(A, 2) - Math.Pow(val, 2)))), 2) * dA;

                    return D;
                }
            }
        }
}
