﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;

namespace Statistics
{
    public class PCA
    {
        public readonly int TotalCount; 
        public int ComponentsCount
        {
            get => EigenValues.Dimensionality;
        }

        public int[] Indecies;
        public Matrix EigenVectors;
        public MultiDimensionalVector EigenValues;
        MultiDimensionalVector Average;

        Matrix Components;

        public PCA(Matrix Input)
        {
            TotalCount = Input.Width;
            Tuple<MultiDimensionalVector, Matrix> T = Matrix.FindEigenValues(Input);
            EigenValues = T.Item1;
            EigenVectors = T.Item2;

            Indecies = new int[EigenValues.Dimensionality];
            for (int i = 0; i < Indecies.Length; i++)
                Indecies[i] = i;

            Sort();

        }
        void Sort()
        {
            int ind;
            double V;
            MultiDimensionalVector Column;

            for (int i = 0; i < EigenValues.Dimensionality - 1; i++)
            {
                for (int j = EigenValues.Dimensionality - 1; j > i; j--)
                {
                    if (EigenValues[j] > EigenValues[j - 1])
                    {
                        V = EigenValues[j - 1];
                        EigenValues[j - 1] = EigenValues[j];
                        EigenValues[j] = V;

                        ind = Indecies[j - 1];
                        Indecies[j - 1] = Indecies[j];
                        Indecies[j] = ind;

                        Column = EigenVectors.GetColumn(j - 1);
                        EigenVectors.SetColumn(j - 1, EigenVectors.GetColumn(j).ToArray());
                        EigenVectors.SetColumn(j, Column.ToArray());
                    }
                }
            }
        }

        public Matrix Translate(Matrix Value, int NewComponentsCount)
        {
            Average = new MultiDimensionalVector(Value.Width);
            for (int i = 0; i < Average.Dimensionality; i++)
            {
                Average[i] = Value.GetColumn(i).ToArray().Average();
            }
            Components = new Matrix(EigenVectors.Height, NewComponentsCount);
            for (int i = 0, j; i < Components.Height; i++)
                for (j = 0; j < NewComponentsCount; j++)
                    Components.SetValue(i, j, EigenVectors[i, j]);

            return (Value - Average) * Components;

        }

        public Matrix TranslateBack(Matrix Value, int NewComponentsCount)
        {
            return Value * Components.GetTransponed() + Average;
        }
    }
}
