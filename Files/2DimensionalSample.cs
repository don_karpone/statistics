﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public class TwoDimensionalSample : Sample
    {
        public bool PCAHistogram;
        new public string Name {
            get => Properties.SampleName;
            set => Properties.SampleName = value;
        }
        new public double Alpha {
            get => Properties.Alpha;
            set => Properties.Alpha = value;
        }
        new public DistributionType Type {
            get => Properties.Distribution;
            set => Properties.Distribution = value;
        }
        new public AdvancedSettings Properties;
        public LinearRegression Regression;
        public Sample VectorX
        {
            get => OneDimensionalVectors[0];
        }
        public Sample VectorY
        {
            get => OneDimensionalVectors[1];
        }
        public VariationGrid VariationGrid;
        public CorrelationSeries CorrelationSeries;
        public RegressionLine RegressionLine;
        public RegressionTolerantLine TopTolerantLine;
        public RegressionTolerantLine BottomTolerantLine;
        public RegressionTrustedLine TopRegressionTrustedLine;
        public RegressionTrustedLine BottomRegressionTrustedLine;
        public RegressionLineTrustedLine TopRegressionLineTrustedLine;
        public RegressionLineTrustedLine BottomRegressionLineTrustedLine;

        public System.Drawing.Image HistogramImage;

        new public Data[] Values;
        public new TwoDimensionalStatistics Statistic;
        public BinaryTabelsCombinations BinaryTable;
        public TablesCombinations Table;

        public bool IsNormalSample;
        public bool IsLinear;
        public bool IsNonLinear;

        public TwoDimensionalSample(Data[] DataArray, string SampleName):base(SampleName)
        {
            Properties = new AdvancedSettings(SampleName, DataArray.Length);
            this.VariationGrid = new VariationGrid(DataArray, Properties.XClassesCount, Properties.YClassesCount);
            this.Values = DataArray;
            SetVectors();
            ChangeStatistic();
        }
        public TwoDimensionalSample(double[] XArray, double[] YArray, string SampleName) : base(SampleName)
        {
            if (IsSamplesDependent(XArray, YArray)) {
                Properties = new AdvancedSettings(SampleName, XArray.Length);
                SetVectors(XArray, YArray);
                ChangeStatistic();
            } else
                throw new Exception("Вибірки не залежні.");
        }
        public TwoDimensionalSample(Sample X, Sample Y, string SampleName) : base(SampleName)
        {
            if (IsSamplesDependent(X.UnSortedArray, Y.UnSortedArray)) {
                Properties = new AdvancedSettings(SampleName, X.Properties.XClassesCount, Y.Properties.XClassesCount);
                SetVectors(X, Y);
                ChangeStatistic();
            } else
                throw new Exception("Вибірки не залежні.");
        }
        public TwoDimensionalSample(string SampleName, Data[] Values, string RegressionModel): this(Values, SampleName)
        {
            Properties.RegressionModel = RegressionModel;
            SetRegression();
        }
        public TwoDimensionalSample(string SampleName, Data[] Values, string RegressionModel, string AdditionRegression) : this(Values, SampleName)
        {
            Properties.RegressionModel = RegressionModel;
            Properties.AdditionalRegression = AdditionRegression;
            SetRegression();
        }
        public void SetRegression()
        {
            switch (Properties.RegressionModel)
            {
                case "Лінійна (МНК)":
                    Regression = new SmallestSquaresLinearRegression(Values, Statistic);
                    break;
                case "Лінійна (метод Тейла)":
                    Regression = new TeilLinearRegression(Values, Statistic);
                    break;
                case "Параболічна":
                    Regression = new PolynomialRegrression(Values, Statistic);
                    break;
                case "Квазілінійна":
                    Regression = new QuziLinearRegression(Values, Alpha, Properties.AdditionalRegression);
                        break;
                default: throw new Exception("Unexpected regression's type.");
            }
            if (!RegressionQuality())
            {
                Journal.Add("Відтворення регресії не якісне" + Environment.NewLine);
            }
            SetRegressionSeries();
        }
        bool IsSamplesDependent(double[] FirstSample, double[] SecoundSample) => (FirstSample.Length == SecoundSample.Length) ? true : false;

        private void SetVectors(Sample X, Sample Y)
        {

            OneDimensionalVectors = new Sample[2];
            OneDimensionalVectors[0] = X;
            OneDimensionalVectors[1] = Y;

            this.Values = new Data[X.Values.Length];
            for (int i = 0; i < Values.Length; i++)
            {
                Values[i].XValue = X.UnSortedArray[i];
                Values[i].YValue = Y.UnSortedArray[i];
            }
        }
        private void SetVectors(double[] X, double[] Y) {

            OneDimensionalVectors = new Sample[2];
            OneDimensionalVectors[0] = new Sample(X, Name + "_X", Properties.XClassesCount);
            OneDimensionalVectors[1] = new Sample(Y, Name + "_Y", Properties.YClassesCount);

            this.Values = new Data[X.Length];
            for (int i = 0; i < Values.Length; i++) {
                Values[i].XValue = X[i];
                Values[i].YValue = Y[i];
            }
        }
        private void SetVectors() {
            double[] X = new double[Values.Length];
            double[] Y = new double[Values.Length];
            for (int i = 0; i < Values.Length; i++) {
                X[i] = Values[i].XValue;
                Y[i] = Values[i].YValue;
            }
            OneDimensionalVectors = new Sample[2];
            OneDimensionalVectors[0] = new Sample(X, Name + "_X", Properties.XClassesCount);
            OneDimensionalVectors[1] = new Sample(Y, Name + "_Y", Properties.YClassesCount);
        }

        private void SetSeries() {
            CorrelationSeries = Build.CorrelationField(Values);
            if (Regression != null) {
                SetRegressionSeries();
            }
        }
        void SetRegressionSeries()
        {
            RegressionLine = Build.RegressionLine(Regression, VariationGrid.MinX, VariationGrid.MaxX);
            TopTolerantLine = Build.TopRegressionTollerantLine(Alpha, Values.Length, Regression, VariationGrid.MinX, VariationGrid.MaxX);
            BottomTolerantLine = Build.BottomRegressionTollerantLine(Alpha, Values.Length, Regression, VariationGrid.MinX, VariationGrid.MaxX);
            TopRegressionTrustedLine = Build.TopRegressionTrustedLine(Alpha, Values.Length, Regression, VariationGrid.MinX, VariationGrid.MaxX, Statistic["MathExpectation(x)"].Data);
            BottomRegressionTrustedLine = Build.BottomRegressionTrustedLine(Alpha, Values.Length, Regression, VariationGrid.MinX, VariationGrid.MaxX, Statistic["MathExpectation(x)"].Data);
            TopRegressionLineTrustedLine = Build.TopRegressionLineTrustedLine(Alpha, Values.Length, Regression, VariationGrid.MinX, VariationGrid.MaxX, Statistic["MathExpectation(x)"].Data);
            BottomRegressionLineTrustedLine = Build.BottomRegressionLineTrustedLine(Alpha, Values.Length, Regression, VariationGrid.MinX, VariationGrid.MaxX, Statistic["MathExpectation(x)"].Data);
        }
        private void SetStatistic()
        {
            Statistic = new TwoDimensionalStatistics(Values, Alpha);
            if (IsLinearCorrelation())
            {
                if (IsNormal())
                {
                    IsNormalSample = true;
                    Type = DistributionType.TwoDimNormal;
                }
                IsLinear = true;
                if (Regression == null)
                {
                    Regression = new SmallestSquaresLinearRegression(Values, Statistic);
                    Properties.RegressionModel = "Лінійна (МНК)";
                }
            }
            else
            {
                Statistic.SetCorrelationRelation(Values);

                IsNonLinear = IsNonLinearCorrelation();
                if (IsNonLinear && Regression == null)
                {
                    Regression = new PolynomialRegrression(Values, Statistic);
                    Properties.RegressionModel = "Параболічна";
                }
            }
            Statistic.RangCorrelation(Values);

            BinaryTable = new BinaryTabelsCombinations(Values, Statistic["MathExpectation(x)"].Data, Statistic["MathExpectation(y)"].Data);
            Statistic.SetTableCoefficients(BinaryTable);
            //Table = new TablesCombinations(VariationGrid);
            //Statistic.SetTableCoefficients(Table);
            if (Regression != null && !RegressionQuality())
            {
                Journal.Add("Відтворення регресії не якісне." + Environment.NewLine);
            }
        }
        bool RegressionQuality()
        {
            double f = Math.Pow(Regression.SigmaTolerant(), 2) / (Statistic["Sigma(y)"].Data * Statistic["Sigma(y)"].Data);
            if (f <= Quantile.Fisher(1 - Alpha / 2, Properties.Size - 1, Properties.Size - 3))
                return true;
            return false;
        }
        bool IsNormal() {
            string ToJurnal = "";
            return true;
            if (Tests.hi2Test(VariationGrid, Statistic, 1 - Alpha)) {
                Type = DistributionType.TwoDimNormal;
                ToJurnal = "При рівні похибки " + Alpha.ToString() + " розподіл відповідає нормальному.";
            } else {
                Type = DistributionType.None;
                ToJurnal = "При рівні похибки " + Alpha.ToString() + " розподіл не відповідає нормальному.";
            }
            Journal.Add(ToJurnal);
            return (Type == DistributionType.TwoDimNormal)? true: false;
        }
        bool IsLinearCorrelation()
        {
            if (!Tests.tTest(Statistic["CorrelationCoefficient"].Data, Values.Length, 1 - Alpha / 2)) {
                Journal.Add("Лінійний коефіціент кореляції є значущим при рівні похибки "
                    + Alpha.ToString() + ". Наявний лінійний стохастичний зв'язок.");
                return true;
            } else {
                Journal.Add("Лінійний коефіціент кореляції не є значущим при рівні похибки "
                    + Alpha.ToString() + ". Лінійний стохастичний зв'язок відсутній.");
                return false;
            }
        }
        bool IsNonLinearCorrelation() {
            if (!Tests.tTest(Statistic["CorrelationRelation"].Data, Values.Length, 1 - Alpha / 2)) {
                Journal.Add("Коефіціент кореляційного відношення є значущим при рівні похибки "
                    + Alpha.ToString() + ". Наявний нелінійний стохастичний зв'язок.");
                return true;
            }
            else {
                Journal.Add("Коефіціент кореляційного відношення не є значущим при рівні похибки "
                    + Alpha.ToString() + ". Стохастичний зв'язок відсутній.");
                return false;
            }
        }

        public override void ChangeStatistic() {
            SetStatistic();
            VariationGrid = new VariationGrid(Values, Properties.XClassesCount, Properties.YClassesCount/*, Statistic["CorrelationCoefficient"].Data*/);
            //SetStatistic();
            SetSeries();
        }
        public override void ChangeStatistic(double Alpha)
        {
            this.Alpha = Alpha;
            VectorX.ChangeStatistic(Alpha);
            VectorY.ChangeStatistic(Alpha);
            SetStatistic();
            SetSeries();
        }
        public void ChangeStatistic(double Alpha, int XClasses, int YClasses) {
            this.Alpha = Alpha;
            Properties.XClassesCount = XClasses;
            Properties.YClassesCount = YClasses;
            SetStatistic();
            VariationGrid = new VariationGrid(Values, XClasses, YClasses/*, Statistic["CorrelationCoefficient"].Data*/);
            VectorX.ChangeStatistic(XClasses, Alpha);
            VectorY.ChangeStatistic(YClasses, Alpha);
            //SetStatistic();
            SetSeries();
        }

        public override void Log()
        {
            if (VariationGrid.MinX >= 0 && VariationGrid.MinY >= 0)
            {
                VectorX.Log();
                VectorY.Log();
                for (int i = 0; i < Values.Length; i++)
                {
                    Values[i].XValue = Math.Round(Math.Log(Values[i].XValue), 4);
                    Values[i].YValue = Math.Round(Math.Log(Values[i].YValue), 4);
                }
                ChangeStatistic();
            }
            else System.Windows.Forms.MessageBox.Show("У вибірці присутні від'ємні значення, логарифмування неможливе;");
        }
        public override void Standartization()
        {
                VectorX.Standartization();
                VectorY.Standartization();

                for (int i = 0; i < Values.Length; i++)
                {
                    Values[i].XValue = Math.Round((Values[i].XValue - Statistic["MathExpectation(x)"].Data) / Statistic["Sigma(x)"].Data, 4);
                    Values[i].YValue = Math.Round((Values[i].YValue - Statistic["MathExpectation(y)"].Data) / Statistic["Sigma(y)"].Data, 4);
                }
                ChangeStatistic();
        }
        public void Move(double XValue, double YValue)
        {
            for (int i = 0; i < Values.Length; i++)
            {
                Values[i].XValue += XValue;
                Values[i].YValue += YValue;
            }
            VectorX.Move(XValue);
            VectorY.Move(YValue);
            ChangeStatistic();
        }
        public override void DeleteAnomaly()
        {
            List<Data> Lst = new List<Data>(Values);
            for (int i = 0, j, k; i < VariationGrid.XClasses; i++)
                for (j = 0; j < VariationGrid.YClasses; j++)
                    if (VariationGrid.Grid[i, j].Massivenes == Massiveness.ExtraLow)
                        if (i == 0 && j == 0)
                        {
                            for (k = 0; k < Lst.Count; k++)
                                if (Lst[k].XValue <= VariationGrid.XClassBorders[i + 1])
                                    if (Lst[k].YValue <= VariationGrid.YClassBorders[j + 1])
                                    {
                                        Lst.RemoveAt(k);
                                        k--;
                                    }
                        }
                        else if (i == 0)
                        {
                            for (k = 0; k < Lst.Count; k++)
                                if (Lst[k].XValue <= VariationGrid.XClassBorders[i])
                                    if (Lst[k].YValue > VariationGrid.YClassBorders[j - 1] && Lst[k].YValue <= VariationGrid.YClassBorders[j])
                                    {
                                        Lst.RemoveAt(k);
                                        k--;
                                    }
                        }
                        else if (j == 0)
                        {
                            for (k = 0; k < Lst.Count; k++)
                                if (Lst[k].XValue > VariationGrid.XClassBorders[i - 1] && Lst[k].XValue <= VariationGrid.XClassBorders[i])
                                    if (Lst[k].YValue <= VariationGrid.YClassBorders[j])
                                    {
                                        Lst.RemoveAt(k);
                                        k--;
                                    }
                        }
                        else
                        {
                            for (k = 0; k < Lst.Count; k++)
                                if (Lst[k].XValue > VariationGrid.XClassBorders[i - 1] && Lst[k].XValue <= VariationGrid.XClassBorders[i])
                                    if (Lst[k].YValue > VariationGrid.YClassBorders[j - 1] && Lst[k].YValue <= VariationGrid.YClassBorders[j])
                                    {
                                        Lst.RemoveAt(k);
                                        k--;
                                    }
                        }

            Values = Lst.ToArray();
            SetVectors();
            ChangeStatistic();
        }
    }
}
