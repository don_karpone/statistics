﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statistics
{
    public static class Build
    {
        static int PointCount = 20;
        static double TempPoint;
        static int i;
        static double Difference;
        public static MainHistogramSeries Histogram(VariationLine L)
        {
            int N = L.ClassNumbers;
            double[] x = new double[N];
            double[] y = new double[N];
            for (i = 0; i < N; i++)
            {
                x[i] = Math.Round(L.VariationList[i].ClassValue, 4);
                y[i] = Math.Round(L.VariationList[i].RelativeFrequency, 4);
            }

            MainHistogramSeries s = new MainHistogramSeries();
            s.Points.DataBindXY(x, y);
            return s;
        }
        public static ClassedDistributionSeries EmpiricalFunction(VariationLine L)
        {
            int N = L.ClassNumbers;
            double[] x = new double[N];
            double[] y = new double[N];
            for (i = 0; i < N; i++)
            {
                x[i] = L.VariationList[i].ClassValue;
                x[i] = Math.Round(x[i], 4);
                y[i] = L.VariationList[i].DistributionProbability;
                if (y[i] == 0)
                    if (i != 0)
                        y[i] = y[i - 1];
            }
            ClassedDistributionSeries s = new ClassedDistributionSeries();

            s.Points.DataBindXY(x, y);
            return s;
        }

        public static DisclassedDistributionSeries DistribuionFunction(VariationLine L)
        {
            int N = L.Size;
            double[] x = new double[N];
            double[] y = new double[N];
            for (int i = 0, j = 0; i < L.ClassNumbers; i++)
            {
                try
                {
                    for (int k = 0; k < L.VariationList[i].ValuesInClass.Count; k++, j++)
                    {
                        x[j] = L.VariationList[i].ValuesInClass[k].Value;
                        x[j] = Math.Round(x[j], 4);
                        y[j] = L.VariationList[i].ValuesInClass[k].DistributionProbability;
                        y[j] = Math.Round(y[j], 4);
                    }
                }
                catch
                {
                    continue;
                }
            }
            DisclassedDistributionSeries s = new DisclassedDistributionSeries();

            s.Points.DataBindXY(x, y);
            return s;
        }
        //2 DIMENTIONAL DATA

        public static CorrelationSeries CorrelationField(Data[] Array)
        {
            CorrelationSeries CorrelationSeries = new CorrelationSeries();

            for (int i = 0; i < Array.Length; i++)
                CorrelationSeries.Points.AddXY(Math.Round(Array[i].XValue, 4), Math.Round(Array[i].YValue, 4));
            return CorrelationSeries;
        }

        public static RegressionLine RegressionLine(LinearRegression R, double Start, double End)
        {
            RegressionLine Regression = new RegressionLine();
            Difference = (End - Start) / (PointCount - 1);
            TempPoint = Start;
            for (i = 0; i < PointCount; i++)
            {
                Regression.Points.AddXY(Start + Difference * i, R.Function(Start + Difference * i));
                TempPoint += Difference;
            }
            return Regression;
        }

        public static RegressionTolerantLine TopRegressionTollerantLine(double alpha, int N, LinearRegression S, double Start, double End)
        {
            RegressionTolerantLine Top = new RegressionTolerantLine
            {
                Name = "TopTolerantLine"
            };
            Difference = (End - Start) / (PointCount - 1);
            TempPoint = Start;
            double Q = Quantile.Student(1d - alpha / 2, N - 2);
            for (int i = 0; i < PointCount; i++)
            {
                if(S is QuziLinearRegression)
                    Top.Points.AddXY(TempPoint, (S as QuziLinearRegression).Reverse(TopValue((S as QuziLinearRegression).LinearFunction(TempPoint), Q, S.SigmaTolerant())));
                else
                    Top.Points.AddXY(TempPoint, TopValue(S.Function(TempPoint), Q, S.SigmaTolerant()));
                TempPoint += Difference;
            }
            return Top;
        }

        public static RegressionTolerantLine BottomRegressionTollerantLine(double alpha, int N, LinearRegression S, double Start, double End)
        {
            RegressionTolerantLine Bottom = new RegressionTolerantLine
            {
                Name = "BottomTolerantLine",
                IsVisibleInLegend = false
            };
            Difference = (End - Start) / (PointCount - 1);
            TempPoint = Start;
            double Q = Quantile.Student(1d - alpha / 2, N - 2);
            for (int i = 0; i < PointCount; i++)
            {
                if (S is QuziLinearRegression)
                    Bottom.Points.AddXY(TempPoint, (S as QuziLinearRegression).Reverse(BottomValue((S as QuziLinearRegression).LinearFunction(TempPoint), Q, S.SigmaTolerant())));
                else
                    Bottom.Points.AddXY(TempPoint, BottomValue(S.Function(TempPoint), Q, S.SigmaTolerant()));
                TempPoint += Difference;
            }
            return Bottom;
        }

        public static RegressionLineTrustedLine TopRegressionLineTrustedLine(double alpha, int N, LinearRegression S, double Start, double End, double Middle)
        {
            RegressionLineTrustedLine Top = new RegressionLineTrustedLine
            {
                Name = "TopRegressionLineTrustedLine"
            };
            Difference = (End - Start) / (PointCount - 1);
            TempPoint = Start;
            double Q = Quantile.Student(1d - alpha / 2, N - 2);
            for (int i = 0; i < PointCount; i++)
            {
                if(S is QuziLinearRegression)
                    Top.Points.AddXY(TempPoint, (S as QuziLinearRegression).Reverse(TopValue((S as QuziLinearRegression).LinearFunction(TempPoint), Q, S.SigmaDivergence(TempPoint, N))));
                else
                    Top.Points.AddXY(TempPoint, TopValue(S.Function(TempPoint), Q, S.SigmaDivergence(TempPoint, N)));
                TempPoint += Difference;
            }
            return Top;
        }

        public static RegressionLineTrustedLine BottomRegressionLineTrustedLine(double alpha, int N, LinearRegression S, double Start, double End, double Middle)
        {
            RegressionLineTrustedLine Bottom = new RegressionLineTrustedLine
            {
                Name = "BottomRegressionLineTrustedLine",
                IsVisibleInLegend = false
            };
            Difference = (End - Start) / (PointCount - 1);
            TempPoint = Start;
            double Q = Quantile.Student(1d - alpha / 2, N - 2);
            for (int i = 0; i < PointCount; i++)
            {
                if (S is QuziLinearRegression)
                    Bottom.Points.AddXY(TempPoint, (S as QuziLinearRegression).Reverse(BottomValue((S as QuziLinearRegression).LinearFunction(TempPoint), Q, S.SigmaDivergence(TempPoint, N))));
                else
                    Bottom.Points.AddXY(TempPoint, BottomValue(S.Function(TempPoint), Q , S.SigmaDivergence(TempPoint, N)));
                TempPoint += Difference;
            }
            return Bottom;
        }
        public static RegressionTrustedLine TopRegressionTrustedLine(double alpha, int N, LinearRegression S, double Start, double End, double Middle)
        {
            RegressionTrustedLine Top = new RegressionTrustedLine
            { 
                Name = "TopTrustedLine"
            };
            Difference = (End - Start) / (PointCount - 1);
            TempPoint = Start;
            double Q = Quantile.Student(1d - alpha / 2, N - 2);
            for (int i = 0; i < PointCount; i++)
            {
                if (S is QuziLinearRegression)
                    Top.Points.AddXY(TempPoint, (S as QuziLinearRegression).Reverse(TopValue((S as QuziLinearRegression).LinearFunction(TempPoint), Q, S.SigmaNewObservation(TempPoint, N))));
                else
                    Top.Points.AddXY(TempPoint, TopValue(S.Function(TempPoint), Q, S.SigmaNewObservation(TempPoint, N)));
                TempPoint += Difference;
            }
            return Top;
        }

        public static RegressionTrustedLine BottomRegressionTrustedLine(double alpha, int N, LinearRegression S, double Start, double End, double Middle)
        {
            RegressionTrustedLine Bottom = new RegressionTrustedLine
            {
                Name = "BottomTrustedLine",
                IsVisibleInLegend = false
            };
            Difference = (End - Start) / (PointCount - 1);
            TempPoint = Start;
            double Q = Quantile.Student(1d - alpha / 2, N - 2);
            for (int i = 0; i < PointCount; i++)
            {
                if (S is QuziLinearRegression)
                    Bottom.Points.AddXY(TempPoint, (S as QuziLinearRegression).Reverse(BottomValue((S as QuziLinearRegression).LinearFunction(TempPoint), Q, S.SigmaNewObservation(TempPoint, N))));
                else
                    Bottom.Points.AddXY(TempPoint, BottomValue(S.Function(TempPoint), Q, S.SigmaNewObservation(TempPoint, N)));
                TempPoint += Difference;
            }
            return Bottom;
        }

        static double TopValue(double Y, double Quntilia, double S) => Y + Quntilia * S;
        static double BottomValue(double Y, double Quntilia, double S) => Y - Quntilia * S;
    }
}
