﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Algebra;

namespace Statistics
{
    public class MultiDimensionalSample : Sample
    {
        public PCA PrimaryComponents;


        public new SuperAdvancedSettings Properties;
        public Matrix CorrelationMatrix;
        public Matrix DispersionCovariationMatrix;
        new public MultiDimensionalStatistic Statistic;

        public MultiDimensionalVector MathExpectation;
        public new Matrix Values;
        public MultiDimensionalVector[] RegressionArguments;
        public TwoDimensionalSample[,] VectorCombinations;
        Bitmap HeatMapGraph;
        public Bitmap HeatMap
        {
            get
            {
                if (HeatMapGraph == null)
                    SetHeatMap();
                return HeatMapGraph;
            }
        }
        VisualizationChart[,] DispersionDiagramm;

        VisualizationChart ParallelCoordinatesGraph;
        public VisualizationChart ParallelCoordinates
        {
            get
            {
                if (ParallelCoordinatesGraph == null)
                    SetParralelCoordinates();
                return ParallelCoordinatesGraph;
            }
        }
        VisualizationChart BubbleGraph;
        public VisualizationChart Bubble
        {
            get
            {
                if (BubbleGraph == null)
                    SetBubble();
                return BubbleGraph;
            }
        }
        VisualizationChart DiagnosticDiagramGraph;
        public VisualizationChart DiagnosticDiagram
        {
            get
            {
                if (DiagnosticDiagramGraph == null)
                    SetDiagnostic();
                return DiagnosticDiagramGraph;
            }
        }
        public MultiDimensionalVector[] ClassedVectors;
        public MultiLinearRegression Regression;
        public Sample CurrentVectorX
        {
            get => CurrentTwoDimensionalVector.VectorX;
        }
        public Sample CurrentVectorY
        {
            get => CurrentTwoDimensionalVector.VectorY;
        }
        public TwoDimensionalSample CurrentTwoDimensionalVector;

        public TwoDimensionalSample this[int VectorX, int VectorY]
        {
            get
            {
                if (VectorCombinations[VectorX, VectorY] == null)
                    VectorCombinations[VectorX, VectorY] = new TwoDimensionalSample(OneDimensionalVectors[VectorX], OneDimensionalVectors[VectorY], Name + "_" + VectorX.ToString() + "_" + VectorY.ToString());
                return VectorCombinations[VectorX, VectorY];
            }
        }
        void SetSize(int Dimensionality)
        {
            OneDimensionalVectors = new Sample[Dimensionality];
            ClassedVectors = new MultiDimensionalVector[Dimensionality];
            VectorCombinations = new TwoDimensionalSample[Dimensionality, Dimensionality];
        }
        public MultiDimensionalSample(MultiDimensionalVector[] InputArray)
        {
            SetSample(new Matrix(InputArray).GetTransponed());
        }
        public MultiDimensionalSample(MultiDimensionalVector[] InputArray, string Name)
        {
            this.Name = Name;
            Properties = new SuperAdvancedSettings(Name, InputArray[0].Dimensionality, InputArray.Length);
            SetSample(new Matrix(InputArray).GetTransponed());
        }
        public MultiDimensionalSample(string Name, params Sample[] Vectors)
        {
            this.Name = Name;
            Properties = new SuperAdvancedSettings(Name, Vectors.Length, Vectors[0].Values.Length);
            SetSample(Vectors);
        }
        public VisualizationChart GetDispersionDiagram(int i, int j)
        {
            if (DispersionDiagramm[i, j] == null)
            {
                if(i != j)
                    DispersionDiagramm[i, j] = new VisualizationChart(Values.GetColumn(i).ToArray(), Values.GetColumn(j).ToArray());
                else
                    DispersionDiagramm[i, j] = new VisualizationChart(OneDimensionalVectors[i]);
            }
            return DispersionDiagramm[i, j];
        }
        public void SetSample(SuperAdvancedSettings NewProperties)
        {
            for (int i = 0, j; i < Dimensionality; i++)
                if (NewProperties.ClassesCount[i] != Properties.ClassesCount[i])
                {
                    Properties.ClassesCount[i] = NewProperties.ClassesCount[i];
                    OneDimensionalVectors[i].ChangeStatistic(Properties.ClassesCount[i]);
                    //SetHeatMap(i);
                    for (j = 0; j < Dimensionality; j++)
                        if (VectorCombinations[i, j] != null)
                            VectorCombinations[i, j] = new TwoDimensionalSample(OneDimensionalVectors[i], OneDimensionalVectors[j], VectorCombinations[i, j].Name);
                }
        }
        void SetSample(Sample[] Vectors)
        {
            double[,] Temp = new double[Vectors[0].Values.Length, Vectors.Length];

            OneDimensionalVectors = Vectors;
            for (int i = 0, j; i < Vectors[0].Values.Length; i++)
            {
                for (j = 0; j < Vectors.Length; j++)
                    Temp[i, j] = Vectors[j].UnSortedArray[i];
            }
            Values = new Matrix(Temp);

            SetMainStatistics();
            VectorCombinations = new TwoDimensionalSample[Dimensionality, Dimensionality];
            VectorCombinations[0, 1] = new TwoDimensionalSample(OneDimensionalVectors[0], OneDimensionalVectors[1], "Name_0_1");
            CurrentTwoDimensionalVector = VectorCombinations[0, 1];

            PrimaryComponents = new PCA(CorrelationMatrix/*DispersionCovariationMatrix*/);
            VectorCombinations = new TwoDimensionalSample[Dimensionality, Dimensionality];
        }
        void SetSample(Matrix InputArray)
        {
            Journal = new List<string>();
            Values = InputArray.Copy();

            SetMainStatistics();

            VectorCombinations[0, 1] = new TwoDimensionalSample(OneDimensionalVectors[0], OneDimensionalVectors[1], "Name_0_1");
            CurrentTwoDimensionalVector = VectorCombinations[0, 1];

            PrimaryComponents = new PCA(CorrelationMatrix/*DispersionCovariationMatrix*/);
        }
        //void SetSample(MultiDimensionalVector[] InputArray)
        //{
        //    Journal = new List<string>();
        //    Values = new MultiDimensionalVector[InputArray.Length];
        //    Array.Copy(InputArray, Values, InputArray.Length);
        //    SetMainStatistics();

        //    VectorCombinations[0, 1] = new TwoDimensionalSample(OneDimensionalVectors[0], OneDimensionalVectors[1], "Name_0_1");
        //    CurrentTwoDimensionalVector = VectorCombinations[0, 1];
        //}


        private void SetVectors()
        {
            OneDimensionalVectors = new Sample[Values.Width];
            SetSize(Values.Width);
            double[] TempDimensionValues = new double[Values.Height];
            for (int i = 0, j; i < Dimensionality; i++)
            {
                for (j = 0; j < Values.Height; j++)
                    TempDimensionValues[j] = Values[j, i];
                OneDimensionalVectors[i] = new Sample(TempDimensionValues, String.Format(Name + "_{0}", i));
            }
        }
        public void SetTwoDimensionalVector(int i, int j)
        {
            VectorCombinations[i, j] = new TwoDimensionalSample(OneDimensionalVectors[i], OneDimensionalVectors[j], String.Format("Name_{0}_{1}", i, j));
        }

        public override void SetMainStatistics()
        {
            SetVectors();
            Statistic = new MultiDimensionalStatistic(this);
            ChangeVisual();
            SetMathExpectation();
            CreateCorrelationMatrix();
            CreateDispersionMatrix();
            Statistic.SetMultipleCorrelation(CorrelationMatrix);
            SetRegression();
        }
        void RefreshStatistics()
        {
            Statistic = new MultiDimensionalStatistic(this);
            ChangeVisual();
            SetMathExpectation();
            CreateCorrelationMatrix();
            CreateDispersionMatrix();
            Statistic.SetMultipleCorrelation(CorrelationMatrix);
            SetRegression();
        }
        public void SetRegression()
        {
            DiagnosticDiagramGraph = null;
            switch (Properties.RegressionModel)
            {
                case "None":
                    break;
                case "Лінійна (3-мірна)":
                    Regression = new CubeLinearRegression(this);
                    break;
                case "Лінійна (багатовимірна)":
                    Regression = new MultiLinearRegression(this);
                    break;
                case "Лінійна (багатовимірна з вільним чл.)":
                    Regression = new MultiLinearRegressionE(this);
                    break;
            }
            RegressionArguments = new MultiDimensionalVector[Values.Height];
            List<double> TempVector = new List<double>();
            for (int i = 0, j; i < Values.Height; i++)
            {
                TempVector.Clear();
                for (j = 0; j < Values.Width; j++)
                    if (Properties.RegressionVectors.Contains(j))
                        TempVector.Add(Values[i, j]);
                RegressionArguments[i] = new MultiDimensionalVector(TempVector.ToArray());
            }
            if (Regression != null)
            {
                if (((Values.Height - Dimensionality - 1) / Dimensionality) * (1 / (1 - Regression.Values["DeterminationCoefficient"].Data) - 1)
                    < Quantile.Fisher(Alpha, Values.Height - Dimensionality - 1, Dimensionality))
                    Journal.Add("Модель регресії адекватна");
                else
                    Journal.Add("Модель регресії не адекватна");
            }
        }

        void CreateCorrelationMatrix()
        {
            double[,] TempMatrix = new double[Dimensionality, Dimensionality];
            for (int i = 0, j; i < Dimensionality; i++)
            {
                TempMatrix[i, i] = 1;
                for (j = i + 1; j < Dimensionality; j++)
                {
                    TempMatrix[i, j] = Statistic[String.Format("Correlation({0}, {1})", i, j)].Data;
                    TempMatrix[j, i] = Statistic[String.Format("Correlation({0}, {1})", j, i)].Data;
                }
            }
            CorrelationMatrix = new Matrix(TempMatrix);
        }


        void SetMathExpectation()
        {
            MathExpectation = new MultiDimensionalVector(Dimensionality);
            for (int i = 0; i < Dimensionality; i++)
                MathExpectation[i] = OneDimensionalVectors[i].Statistic["MathExpectation"].Data;
        }


        void CreateDispersionMatrix()
        {
            double[,] TempMatrix = new double[Dimensionality, Dimensionality];
            for (int i = 0, j; i < Dimensionality; i++)
            {
                TempMatrix[i, i] = Math.Pow(Statistic[String.Format("Sigma(x{0})", i)].Data, 2);
                for (j = i + 1; j < Dimensionality; j++)
                {
                    TempMatrix[i, j] = Statistic[String.Format("Correlation({0}, {1})", i, j)].Data 
                        * Statistic[String.Format("Sigma(x{0})", i)].Data * Statistic[String.Format("Sigma(x{0})", j)].Data;
                    TempMatrix[j, i] = TempMatrix[i, j];
                }
            }
            DispersionCovariationMatrix = new Matrix(TempMatrix);
        }

        public void ChangeVisual()
        {
            HeatMapGraph = null;
            DispersionDiagramm = new VisualizationChart[Dimensionality, Dimensionality];
            ParallelCoordinatesGraph = null;
            BubbleGraph = null;
            DiagnosticDiagramGraph = null;
        }
        void SetDispersionDiagramm()
        {
            DispersionDiagramm = new VisualizationChart[Dimensionality, Dimensionality];
            for (int i = 0, j; i < Dimensionality; i++)
            {
                DispersionDiagramm[i, i] = new VisualizationChart(OneDimensionalVectors[i]);
                for (j = i + 1; j < Dimensionality; j++)
                {
                    DispersionDiagramm[i, j] = new VisualizationChart(Values.GetColumn(i).ToArray(), Values.GetColumn(j).ToArray());
                    DispersionDiagramm[j, i] = new VisualizationChart(Values.GetColumn(j).ToArray(), Values.GetColumn(i).ToArray());
                }
            }
        }
        void SetDiagnostic()
        {
            DiagnosticDiagramGraph = new VisualizationChart(Regression, MultiDimensionalVector.GetArray(Values.ToVectorArray(), Properties.DependentVector), RegressionArguments);
        }
        void SetBubble()
        {
            BubbleGraph = new VisualizationChart(Values.ToVectorArray(), VisualizationType.Bubble);
        }
        void SetParralelCoordinates()
        {
            ParallelCoordinatesGraph = new VisualizationChart(Values.ToVectorArray(), VisualizationType.ParallelCoordinates);
        }
        //public void SetHeatMaps()
        //{
        //HeatMapGraph = GetHeatMap(Values);
        //for (int i = 0; i < Dimensionality; i++)
        //SetHeatMap(i);
        //}
        void SetHeatMap()
        {
            ClassedVectors = MultiDimensionalVector.Copy(Values.ToVectorArray());

            //int GridHeight = Properties.VisualizationCount;
            //ClassedVectors = GetClassedArray(out MultiDimensionalVector[] ArrayForColorParse);
            MultiDimensionalVector Minimums = new MultiDimensionalVector(Dimensionality);
            MultiDimensionalVector Maximums = new MultiDimensionalVector(Dimensionality);
            for (int i = 0; i < Dimensionality; i++)
            {
                Minimums[i] = OneDimensionalVectors[i].Values.Min();
                Maximums[i] = OneDimensionalVectors[i].Values.Max() - Minimums[i];
            }
            for (int i = 0; i < ClassedVectors.Length; i++)
                ClassedVectors[i] = (ClassedVectors[i] - Minimums) / Maximums;

            HeatMapGraph = GetHeatMap(ClassedVectors);
        }
        Bitmap GetHeatMap(MultiDimensionalVector[] ClassedVectors)
        {
            Bitmap OutputImage = new Bitmap(128 * Dimensionality, 32 * ClassedVectors.Length, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Bitmap Temp = new Bitmap(128, 32, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics G = Graphics.FromImage(OutputImage);
            Graphics TempG = Graphics.FromImage(Temp);
            Pen BlackPen = new Pen(Color.Black, 1);
            int TempRead = 0;
            for (int i = 0, j; i < Dimensionality; i++)
                for (j = 0; j < ClassedVectors.Length; j++)
                {
                    TempRead = (255 * ClassedVectors[j][i] > 255) ? 255 : (int)(Math.Round(255 * ClassedVectors[j][i]));
                    TempG.Clear(Color.FromArgb(TempRead, 0, 0));
                    TempG.DrawRectangle(BlackPen, 0, 0, Temp.Width, Temp.Height);

                    G.DrawImage(Temp, i * 128, j * 32);
                }
            return OutputImage;
        }
        public static explicit operator TwoDimensionalSample(MultiDimensionalSample S) => S.CurrentTwoDimensionalVector;

        public void Standartization(params int[] Indecies)
        {
            for (int i = 0; i < Indecies.Length; i++)
            {
                OneDimensionalVectors[Indecies[i]].Standartization();
                Values.SetColumn(i, OneDimensionalVectors[Indecies[i]].UnSortedArray);
                //MultiDimensionalVector.ChangeOneDimensionInArray(Values, Indecies[i], OneDimensionalVectors[Indecies[i]].UnSortedArray);
            }
            for(int i = 0, j; i < Indecies.Length; i++)
                for(j = 0; j < Indecies.Length; j++)
                    if(VectorCombinations[i, j] != null)
                        VectorCombinations[i, j] = new TwoDimensionalSample(OneDimensionalVectors[i], OneDimensionalVectors[j], VectorCombinations[i, j].Name);
            RefreshStatistics();

            PrimaryComponents = new PCA(DispersionCovariationMatrix);
            VectorCombinations = new TwoDimensionalSample[Dimensionality, Dimensionality];
        }

        public void Move(int X, int Y, double XMoving, double YMoving)
        {
            OneDimensionalVectors[X].Move(XMoving);
            Values.SetColumn(X, OneDimensionalVectors[X].UnSortedArray);
            //Values = MultiDimensionalVector.ChangeOneDimensionInArray(Values, X, OneDimensionalVectors[X].UnSortedArray);
            OneDimensionalVectors[Y].Move(YMoving);
            //Values = MultiDimensionalVector.ChangeOneDimensionInArray(Values, Y, OneDimensionalVectors[Y].UnSortedArray);
            Values.SetColumn(Y, OneDimensionalVectors[Y].UnSortedArray);
            VectorCombinations[X, Y] = new TwoDimensionalSample(OneDimensionalVectors[X], OneDimensionalVectors[Y], VectorCombinations[X, Y].Name);
            RefreshStatistics();
        }
        public void Move(int X, double XMoving)
        {
            OneDimensionalVectors[X].Move(XMoving);
            Values.SetColumn(X, OneDimensionalVectors[X].UnSortedArray);
            for (int i = 0; i < Dimensionality; i++)
                if (VectorCombinations[X, i] != null)
                    VectorCombinations[X, i] = new TwoDimensionalSample(OneDimensionalVectors[X], OneDimensionalVectors[i], VectorCombinations[X, i].Name);

            RefreshStatistics();
        }

        public void Log(params int[] Indecies)
        {
            for (int i = 0; i < Indecies.Length; i++)
            {
                OneDimensionalVectors[Indecies[i]].Log();
                Values.SetColumn(Indecies[i], OneDimensionalVectors[Indecies[i]].UnSortedArray);
                //Values = MultiDimensionalVector.ChangeOneDimensionInArray(Values, Indecies[i], OneDimensionalVectors[Indecies[i]].UnSortedArray);
            }
            for (int i = 0, j; i < Indecies.Length; i++)
                for (j = 0; j < Indecies.Length; j++)
                    if (VectorCombinations[i, j] != null)
                        VectorCombinations[i, j] = new TwoDimensionalSample(OneDimensionalVectors[i], OneDimensionalVectors[j], VectorCombinations[i, j].Name);
            RefreshStatistics();
        }
      
        public void DeleteAnomaly(int X, int Y)
        {
            X = X + Y;
        }

    }
}