﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.DataVisualization.Charting;

namespace Statistics
{
    public class Sample
    {
        public int Dimensionality
        {
            get => OneDimensionalVectors.Length;
        }
        public Settings Properties;
        public string UniqNumber;
        public string Name
        {
            get => Properties.SampleName;
            set => Properties.SampleName = value;
        }
        public double[] Values;
        public double[] UnSortedArray;
        public double[] _Values;
        public VariationLine VariationList;
        public Sample[] OneDimensionalVectors;
        public double Alpha
        {
            get => Properties.Alpha;
            set => Properties.Alpha = value;
        }
        public Statistics Statistic;
        public DistributionType Type
        {
            get => Properties.Distribution;
            set => Properties.Distribution = value;
        }

        public List<string> Journal;

        public Series HistogramView;
        public Series ClassedDistributionView;
        public Series DisclassedDistributionView;
        public Series ModelHistogramView;
        public Series ModelDistributionView;
        public Series ModelTopDistributionView;
        public Series ModelBottomDistributionView;

        public Sample()
        {
            Journal = new List<string>();
            Properties = new Settings("Sample");
            OneDimensionalVectors = new Sample[] {this};
        }
        public Sample(string SampleName)
        {
            Journal = new List<string>();
            Properties = new Settings(SampleName);
        }

        public Sample(double[] InputArray, string SampleName)
            :this()
        {
            Properties = new Settings(SampleName, InputArray.Length);

            SetArrays(InputArray);
            SetMainStatistics();
        }

        public Sample(double[] InputArray, string SampleName, int ClassCount)
            :this()
        {
            Properties = new Settings(SampleName, InputArray.Length);
            Properties.XClassesCount = ClassCount;

            SetArrays(InputArray);
            SetMainStatistics();
        }

        public Sample(double[] InputArray, string SampleName, DistributionType T, double Alpha)
            : this()
        {
            Properties = new Settings(SampleName, InputArray.Length);
            Properties.Distribution = T;
            Properties.Alpha = Alpha;

            SetArrays(InputArray);
            SetMainStatistics();
        }
        public virtual void SetMainStatistics()
        {
            this.VariationList = new VariationLine(this.Values, Properties.XClassesCount);
            this.Statistic = new Statistics(this.Values, Alpha);

            SetHistogram();
            SetDistribution();

            if (this.Type != DistributionType.None)
                SetModel();
        }
        void SetArrays(double[] InputArray)
        {
            this.Values = new double[InputArray.Length];
            this._Values = new double[InputArray.Length];
            this.UnSortedArray = new double[InputArray.Length];

            InputArray.CopyTo(this.UnSortedArray, 0);
            InputArray.CopyTo(this.Values, 0);
            Array.Sort(Values);
            Values.CopyTo(this._Values, 0);
        }
        public virtual void ChangeStatistic()
        {
            SetMainStatistics();
        }
        public virtual void ChangeStatistic(double Alpha)
        {
            Properties.Alpha = Alpha;
            SetMainStatistics();
        }
        public void ChangeStatistic(int M)
        {
            Properties.XClassesCount = M;
            SetMainStatistics();
        }
        public void ChangeStatistic(int M, double Alpha)
        {
            Properties.Alpha = Alpha;
            Properties.XClassesCount = M;
            SetMainStatistics();
        }
        public void ChangeStatistic(int M, double Alpha, DistributionType T)
        {
            Properties.Alpha = Alpha;
            Properties.Distribution = T;
            Properties.XClassesCount = M;
            SetMainStatistics();
        }

        private void SetHistogram()
        {
            this.HistogramView = Build.Histogram(this.VariationList);
        }

        private void SetDistribution()
        {
            this.ClassedDistributionView = Build.EmpiricalFunction(this.VariationList);
            this.DisclassedDistributionView = Build.DistribuionFunction(this.VariationList);
        }

        #region ІДЕНТИФІКАЦІЯ РОЗПОДІЛУ

        public void SetModel(double Alpha)
        {
            this.Alpha = Alpha;
            SetModel();
        }

        public void SetModel()
        {
            double q = 1000 / this.VariationList.ClassNumbers;
            double[] V = new double[1000];
            double r = (this.Values.Max() - this.Values.Min()) / 1000;
            double g = this.Values.Min();
            for (int i = 0; i < 1000; i++)
            {
                V[i] = g;
                g += r;
            }
            switch (this.Type)
            {
                case DistributionType.None:
                    #region None
                    #endregion
                    break;
                case DistributionType.Exponencial:
                    #region Exponencial

                    this.ModelHistogramView = Distribution.Exponencial.BuildDensityFunction(V, this.Statistic, q);

                    this.ModelDistributionView = Distribution.Exponencial.BuildDistributionFunction(V, this.Statistic);

                    this.ModelTopDistributionView = Distribution.Exponencial.Top(V, this.Statistic, this.Alpha);

                    this.ModelBottomDistributionView = Distribution.Exponencial.Bottom(V, this.Statistic, this.Alpha);

                    #endregion
                    break;
                case DistributionType.Normal:
                    #region Normal

                    this.ModelHistogramView = Distribution.Normal.DensityFunction(V, this.Statistic, q);

                    this.ModelDistributionView = Distribution.Normal.DistributionFunction(V, this.Statistic);

                    this.ModelTopDistributionView = Distribution.Normal.Top(V, this.Statistic, this.Alpha);

                    this.ModelBottomDistributionView = Distribution.Normal.Bottom(V, this.Statistic, this.Alpha);

                    #endregion
                    break;
                case DistributionType.Equaliste:
                    #region Equaliste

                    this.ModelHistogramView = Distribution.Equaliste.DensityFunction(V, q);

                    this.ModelDistributionView = Distribution.Equaliste.DistributionFunction(V);

                    this.ModelTopDistributionView = Distribution.Equaliste.Top(V, this.Statistic, this.Alpha);

                    this.ModelBottomDistributionView = Distribution.Equaliste.Bottom(V, this.Statistic, this.Alpha);

                    #endregion
                    break;
                case DistributionType.Weibull:
                    #region Weibull
                    double[] Probability = new double[this.Values.Length];
                    for (int i = 0, j = 0; i < this.VariationList.ClassNumbers; i++)
                    {
                        for (int k = 0; k < this.VariationList.VariationList[i].ValuesInClass.Count; k++, j++)
                        {
                            Probability[j] = this.VariationList.VariationList[i].ValuesInClass[k].DistributionProbability;
                        }
                    }
                    double a = Distribution.Weibull.SetAlphaAndBeta(this.Values, Probability)[0];
                    double b = Distribution.Weibull.SetAlphaAndBeta(this.Values, Probability)[1];

                    this.ModelHistogramView = Distribution.Weibull.DensityFunction(V, a, b, q);

                    this.ModelDistributionView = Distribution.Weibull.DistributionFunction(V, a, b);

                    #endregion
                    break;
                case DistributionType.Extremal:
                    #region Extremal
                    #endregion
                    break;
                case DistributionType.Arcsinus:
                    #region Arcsinus
                    this.ModelHistogramView = Distribution.Arcsinus.DensityFunction(V, this.Statistic, q);

                    this.ModelDistributionView = Distribution.Arcsinus.DistributionFunction(V, this.Statistic);

                    this.ModelTopDistributionView = Distribution.Arcsinus.Top(V, this.Statistic, this.Alpha);

                    this.ModelBottomDistributionView = Distribution.Arcsinus.Bottom(V, this.Statistic, this.Alpha);

                    #endregion
                    break;
                default:
                    break;
            }
        }
        #endregion

        public virtual void Standartization()
        {
            for (int i = 0; i < VariationList.Size; i++)
            {
                Values[i] = Math.Round((Values[i] - Statistic["MathExpectation"].Data) / Statistic["Sigma"].Data, 4);
                UnSortedArray[i] = Math.Round((UnSortedArray[i] - Statistic["MathExpectation"].Data) / Statistic["Sigma"].Data, 4);
            }

            SetMainStatistics();
        }
        public virtual void Log()
        {
            if (Values.Min() >= 0)
            {
                for (int i = 0; i < Values.Length; i++)
                {
                    Values[i] = Math.Round(Math.Log(Values[i]), 4);
                    UnSortedArray[i] = Math.Round(Math.Log(UnSortedArray[i]), 4);
                }
                SetMainStatistics();
            }
            else System.Windows.Forms.MessageBox.Show("У вибірці присутні від'ємні значення, логарифмування неможливе;");
        }
        public virtual void Move(double Value)
        {
            for (int i = 0; i < Values.Length; i++)
            {
                Values[i] += Value;
                UnSortedArray[i] += Value;
            }
            SetMainStatistics();
        }
        public virtual void DeleteAnomaly(/*double BottomLimit*/)
        {
            double v = (Statistic["CEC"].Data > 1) ? (Statistic["CEC"].Data - 1) : (1 - Statistic["CEC"].Data);
            double t = 1.2 + 3.6 * v * Math.Log10((double)Values.Length / 10);

            double a = Statistic["MathExpectation"].Data - t * Statistic["Sigma"].Data;
            double b = Statistic["MathExpectation"].Data + t * Statistic["Sigma"].Data;

            List<double> l = new List<double>(Values);
            for (int i = 0; i < l.Count; i++)
                if (l[i] <= a || l[i] >= b)
                {
                    l.Remove(l[i]);
                    i--;
                }
            Values = l.ToArray();
            SetMainStatistics();
        }
    }
}
