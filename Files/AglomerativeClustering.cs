﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;

namespace Statistics
{
    public static class AglomerativeClustering
    {
        static Func<double, double, double, double, double, double, double> ClusterUnion(Func<Matrix, Matrix, Func<MultiDimensionalVector, MultiDimensionalVector, double>, double> MetricType)
        {
            if (MetricType == ClusterDistances.NearestNeighbour)
                return (x, y, z, n1, n2, n3) => 0.5 * x + 0.5 * y - 0.5 * Math.Abs(x - y);
            else if(MetricType == ClusterDistances.FarestNeighbour)
                return (x, y, z, n1, n2, n3) => 0.5 * x + 0.5 * y + 0.5 * Math.Abs(x - y);
            else if(MetricType == ClusterDistances.Median)
                return (x, y, z, n1, n2, n3) => 0.5 * x + 0.5 * y - 0.25 * z;
            else if (MetricType == ClusterDistances.WeightedAverage)
                return (x, y, z, n1, n2, n3) => n1 / (n1 + n2) * x + n2 / (n1 + n2) * y;
            else if (MetricType == ClusterDistances.Centroids)
                return (x, y, z, n1, n2, n3) => n1 / (n1 + n2) * x + n2 / (n1 + n2) * y + n1 * n2 / ((n1 + n2)*(n1 + n2)) * z;
            else if (MetricType == ClusterDistances.Words)
                return (x, y, z, n1, n2, n3) => (n1 + n3) / (n1 + n2 + n3) * x + (n2 + n3) / (n1 + n2 + n3) * y + n3 / (n1 + n2 + n3) * z;
            else //Average
                return (x, y, z, n1, n2, n3) => 0.5 * x + 0.5 * y;
        }

        public static Matrix[] PerformClustering(Matrix Values, int StepCount,
            Func<MultiDimensionalVector, MultiDimensionalVector, double> DistanceMetric,
            Func<Matrix, Matrix, Func<MultiDimensionalVector, MultiDimensionalVector, double>, double> ClusterDistanceMetric)
        {

            Func<double, double, double, double, double, double, double> D = ClusterUnion(ClusterDistanceMetric);

            List<Matrix> Clusters = new List<Matrix>();
            for (int i = 0; i < Values.Height; i++)
            {
                Matrix M = new Matrix(1, Values.Width);
                M.SetRow(0, Values.GetRow(i).ToArray());
                Clusters.Add(M);
            }

            Matrix ClusterI, ClusterJ;
            int ClusterIndex1 = 0,
                ClusterIndex2 = 0;
            double Min;

            Matrix DistanceMatrix;

            GetDistances();

            while (Clusters.Count > StepCount && Clusters.Count > 1)
            {
                FindClosestClusters();

                UpdateDistanceMatrix();

                UniteClusters();
            }

            return Clusters.ToArray();

            void GetDistances()
            {
                DistanceMatrix = new Matrix(Clusters.Count, Clusters.Count);

                double d = 0;
                for (int i = 0, j; i < DistanceMatrix.Height; i++)
                    for (j = i + 1; j < DistanceMatrix.Width; j++)
                    {
                        d = ClusterDistanceMetric(Clusters[i], Clusters[j], DistanceMetric);
                        DistanceMatrix.SetValue(i, j, d);
                        DistanceMatrix.SetValue(j, i, d);
                    }
            }

            void FindClosestClusters()
            {
                Min = Double.MaxValue;
                for (int i = 0, j; i < DistanceMatrix.Height; i++)
                    for (j = i + 1; j < DistanceMatrix.Width; j++)
                    {
                        if (Min > DistanceMatrix[i, j])
                        {
                            Min = DistanceMatrix[i, j];
                            ClusterIndex1 = i;
                            ClusterIndex2 = j;
                        }
                    }
            }

            void UniteClusters()
            {
                ClusterI = Clusters[ClusterIndex1].Copy();
                ClusterJ = Clusters[ClusterIndex2].Copy();

                if (ClusterIndex1 > ClusterIndex2)
                    Clusters.RemoveAt(ClusterIndex1);
                else
                    Clusters.RemoveAt(ClusterIndex2);

                Matrix ClusterNew = new Matrix(ClusterI.Height + ClusterJ.Height, ClusterI.Width);

                for (int i = 0; i < ClusterI.Height; i++)
                    ClusterNew.SetRow(i, ClusterI.GetRow(i).ToArray());
                for (int i = 0; i < ClusterJ.Height; i++)
                    ClusterNew.SetRow(i + ClusterI.Height, ClusterJ.GetRow(i).ToArray());

                if (ClusterIndex1 > ClusterIndex2)
                    Clusters[ClusterIndex2] = ClusterNew;
                else
                    Clusters[ClusterIndex1] = ClusterNew;
            }

            void UpdateDistanceMatrix()
            {
                double d1, d2;
                double d3 = DistanceMatrix[ClusterIndex1, ClusterIndex2];
                double n1 = Clusters[ClusterIndex1].Height;
                double n2 = Clusters[ClusterIndex2].Height;
                double n3;
                double Dst;

                if (ClusterIndex1 > ClusterIndex2)
                {
                    for (int i = 0; i < DistanceMatrix.Width; i++)
                    {
                        if (i != ClusterIndex1 && i != ClusterIndex2)
                        {
                            d1 = DistanceMatrix[ClusterIndex1, i];
                            d2 = DistanceMatrix[ClusterIndex2, i];
                            n3 = Clusters[i].Height;

                            Dst = D(d1, d2, d3, n1, n2 ,n3);
                            DistanceMatrix.SetValue(ClusterIndex2, i, Dst);
                            DistanceMatrix.SetValue(i, ClusterIndex2, Dst);
                        }
                    }

                    DistanceMatrix = Matrix.RemoveColumn(DistanceMatrix, ClusterIndex1);
                    DistanceMatrix = Matrix.RemoveRow(DistanceMatrix, ClusterIndex1);
                }
                else
                {
                    for (int i = 0; i < DistanceMatrix.Width; i++)
                    {
                        if (i != ClusterIndex1 && i != ClusterIndex2)
                        {
                            d1 = DistanceMatrix[ClusterIndex1, i];
                            d2 = DistanceMatrix[ClusterIndex2, i];
                            n3 = Clusters[i].Height;

                            Dst = D(d1, d2, d3, n1, n2, n3);
                            DistanceMatrix.SetValue(ClusterIndex1, i, Dst);
                            DistanceMatrix.SetValue(i, ClusterIndex1, Dst);
                        }
                    }

                    DistanceMatrix = Matrix.RemoveColumn(DistanceMatrix, ClusterIndex2);
                    DistanceMatrix = Matrix.RemoveRow(DistanceMatrix, ClusterIndex2);
                }
            }
        }
    }
}
